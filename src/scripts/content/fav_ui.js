/*
    Copyright (C) 2020-present Aïdo WILHELM

    This file is part of favacarte.

    favacarte is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    favacarte is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with favacarte.  If not, see <https://www.gnu.org/licenses/>.
*/
/**
 **
 **
 **			Content script - un script exécuté avec un peu plus de privilèges que les scripts de la page
 **                          
 **
 **/

"use strict";

/*
 *
 * Fav
 *  contient le backend necessaire a :
 *     + synchronisation avec le stockage local
 *     + communication avec le daemon
 *     + synchronisation avec l'interface utilisateur
 *
 */
class Fav {
	/* Construire l'objet avec les différents context dont on a besoin */
	constructor(html_id="fav_render_context") 
	{
		// Structure
		this.api 												= new vAPI;					// redéfinition de méthode de l'API Javascript : browser.*
		this.backend											= {};              			// outils qui ne touche pas directement à l'interface
		this.backend.tools 										= new Tools("cs");				// outils léger qui simplifie les calculs
		this.backend.storage 									= {};						// outils de gestion du stockage interne
  		this.backend.storage.Groups 							= new Groups; 				// structure de Groups, niveau niveau juste avant l'interface visjs - sto.groups
  		this.backend.storage.Bookmarks 							= new Bookmarks;			// structure de Bookmarks  - sto.bookmarks
  		this.backend.storage.BookmarkTreeNode 					= new BookmarkTreeNode; 	// structure de BookmarkTreeNode - interne firefox
		this.backend.communication 								= {};						// outils de communication avec le script de background
		this.frontend                                           = new Frontend; 			// outils qui touchent à l'interface
//		this.frontend.api 										= {}; 						// frontend api
		this.frontend.kp_accu_decharge 							= false; 					// le kp accu est il en pleine décharge
		this.frontend.kp_accu 									= []; 						// accumulateur de touches utilisateur
//		this.frontend.handlers 									= {}; 						// events handlers du frontend (interaction utilisateur)
		this.frontend.handlers.contextmenu 						= {}; 						// handlers qui se déclenchent via le context menu
		this.frontend.handlers.contextmenu.accu_keypress		= []; 						// accumulateur général de la frappe
		this.frontend.datasets 									= {};						// visjs.Dataset
		this.frontend.datasets.nodes 							= undefined; 				// accès directe à l'ui (noeuds visjs)
		this.frontend.datasets.edges 							= undefined;				// accès directe à l'ui (noeuds visjs)
		this.frontend.vis 										= undefined;				// visj.Network
		this.html 												= {}; 						// outils html
		this.html.navtab 										= {}; 						// object contenant la navtab
		this.html.contextmenu 									= new Contextmenu; 			// API d'interaction avec le contextmenu
		this.html.context 										= {};
		this.html.context.opacity								= 0.8;						// Opacitée de l'interface HTML
		this.html.context.zIndex								= 99999999999;				// Valeur absolue de l'index de la div html
		this.html.context.CSS 									= []; 						// Style a enlever à la transition
		this.html.shadowRoot 									= undefined; 				// Stockage du pointeur d'accès à la shadow root

		// Stockage interne	(bookmarks,images,settings sont enregistrés) 				
		this.backend.storage.bookmarks 							= {};						// Favoris selon la structure interne
		this.backend.storage.images 							= [];						// Images à injecter dans le front end
		this.backend.storage.settings 							= {};
		//this.backend.storage.settings.backend.debug.frontend.set_overlay


		/**
		 **		API d'interation du client favaCarte
		 **     les api et fonction sont rangées par ordre alphabétique
		 **/
		// attributs
		this.h      = this.html;
		this.f   	= this.front   		= this.frontend;
		this.b   	= this.back    		= this.backend;
		this.sto    = this.storage      = this.backend.storage;
		this.noeud  = this.html.noeud;
		this.com    = this.backend.communication;
		this.tools  = this.backend.tools;
		this.handlers = this.frontend.handlers;
		this.contextmenu = this.frontend.handlers.contextmenu;
		this.Groups = this.backend.storage.Groups;
		this.Bookmarks = this.backend.storage.Bookmarks;
		this.BookmarkTreeNode = this.backend.storage.BookmarkTreeNode;


		// méthodes
		this.api.redirection 											= this._api_redirection;
		this.backend.communication.envoi 					   			= this._backend_communication_envoi;
		this.backend.storage.get   										= this._backend_storage_get;
		this.backend.storage.set 										= this._backend_storage_set;

		this.html.get_style_value 										= this._html_get_style_value;
		this.html.navtab.enter 											= this._html_navtab_enter;
		this.html.navtab.leave 											= this._html_navtab_leave;
		this.html.removeHoverEffects 									= this._html_removeHoverEffects;
		this.html.render_show 											= this._html_render_show;
		this.html.show_center 											= this._html_show_center;

		this.start 														= this._start;
		this.stop 														= this._stop;
   }
	async _api_redirection(url,location=undefined,depth_recurse=0,visited=[])
	{
		try{ if(this._this.backend.storage.settings.backend.debug.api.redirection)
		{
			this._this.tools.group("api.redirection");
			this._this.tools.console("url=","d",url);
			this._this.tools.console("location=","d",location);
			this._this.tools.console("depth_recurse=","d",depth_recurse);
			this._this.tools.console("visited=","d",visited);
			this._this.tools.groupEnd();
		}} catch(e) {}

		if( typeof(url) == "object")
		{
			visited.push(url.id);
			if(location==undefined)
				location="newtab";
			let ids = [],c,g;
			ids=ids.concat(url.children);
			if(this._this.backend.storage.settings.frontend.controller.openFolderFullGraphe)
				ids=ids.concat(url.parents);
			for(c of ids)
			{
				g = this._this.sto.groups[c];
				if(g.type == "bookmark" && depth_recurse > 0 )
				{
					this._this._api_redirection(g.url,location,0,visited);
				//	if(location=="newwindow")
				//		location="newtab";    // probleme: la fenetre ne s'est pas encore ouverte
				}
				else if(g.type == "folder" && depth_recurse > 0 && !visited.includes(c))
					this._this._api_redirection(g,location,depth_recurse-1,visited);
			}
			return ;
		}
		switch(location)
		{
			case "newtab":
			//	if(this._this.backend.storage.settings.frontend.controller.openLinksWithDaemon)
					await this._this.com.envoi({statut:"bookmark_open",value:{url: url,location:"newtab"}});
			//	else
					// TODO : mettre la prop javascript qui va bien
				break;
			case "newwindow":
				if(this._this.backend.storage.settings.frontend.controller.openLinksWithDaemon)
					await this._this.com.envoi({statut:"bookmark_open",value:{url: url,location:"newwindow"}});
				else
					window.open(url);
				break;
			case "newprivate":
			//	if(this._this.backend.storage.settings.frontend.controller.openLinksWithDaemon)
					await this._this.com.envoi({statut:"bookmark_open",value:{url: url,location:"newprivate"}});
				//else
					// TODO : mettre la prop qui va bien
				break;
			default:
				if(this._this.sto.settings.frontend.controller.openBookmarksInNewTabs)
					await this._this.com.envoi({statut:"bookmark_open",value:{url: url,location:"newtab"}});
				else
					if(this._this.sto.settings.frontend.controller.openLinksWithDaemon)
						await this._this.com.envoi({statut:"bookmark_open",value:{url: url,location:"current"}});
					else
						window.document.location.replace(url);
		}
	}
	async _backend_communication_envoi(message)
	{ 
		try{if(this._this.backend.storage.settings.backend.debug.backend.communication.envoi)
		{
			this._this.tools.group("backend.communication.envoi");
			this._this.tools.console("message=","d",message);
			this._this.tools.groupEnd();
		}} catch(e) {}
		if( typeof(message) == "string" )
			return await browser.runtime.sendMessage({statut: message,value: undefined});
		else
			return await browser.runtime.sendMessage(message);
	}
	/**
	 *
	 * Gestion du stockage interne
	 *
	 **/
	async _backend_storage_get()
	{
		let temp  					 			= await browser.storage.local.get();
		this._this.backend.storage.bookmarks 	= temp.bookmarks;
		this._this.backend.storage.images   	= temp.images;
		this._this.backend.storage.settings 	= temp.settings;
		this._this.backend.storage.groups 		= temp.groups;
		if(this._this.backend.storage.images == undefined)
			this._this.backend.storage.images = [];
		if(this._this.backend.storage.settings == undefined)
			this._this.backend.storage.settings = {};
		if(this._this.backend.storage.bookmarks == undefined)
			this._this.backend.storage.bookmarks = {};
		if(this._this.backend.storage.groups == undefined)
			this._this.backend.storage.groups = {};
		return true;
	} // vérifié
	async _backend_storage_set()
	{
		await browser.storage.local.set({bookmarks: this._this.backend.storage.bookmarks, images: this._this.backend.storage.images, settings: this._this.backend.storage.settings, groups: this._this.backend.storage.groups});
	} // vérifié

	_html_show_center(width=this._this.sto.settings.frontend.interface.cross.width,len=this._this.sto.settings.frontend.interface.cross.len,color=this._this.sto.settings.frontend.interface.cross.color)
	{
		let cross={
			width: 		width,
			len: 		len
		};
		let div = document.createElement("div");div.style.background=color;div.style.width=cross.len+"px";div.style.height=cross.width+"px";
		div.style.top=window.innerHeight/2-cross.width/2+"px";div.style.left=window.innerWidth/2-cross.len/2+"px";div.style.position="fixed";
		this._this.html.noeud.appendChild(div);
		div = document.createElement("div");div.style.background=color;div.style.width=cross.width+"px";div.style.height=cross.len+"px";
		div.style.top=window.innerHeight/2-cross.len/2+"px";div.style.left=window.innerWidth/2-cross.width/2+"px";div.style.position="fixed";
		this._this.html.noeud.appendChild(div);
	}
	_html_get_style_value(obj,style,jstyle=style)
	{
		let v,css;
		let p = obj.style[jstyle];
		if(p!=undefined && p!="")
		{
			v=p;
		}
		else
		{
			css = window.getComputedStyle(obj);
			v = css.getPropertyValue(style);
			if(v=="") v=undefined;
		}
		return v;
	}
	async _html_navtab_enter()
	{
		let d;
		if(this._this.sto.settings.temp.frontend.tabNav) return false;
		this._this.sto.settings.temp.frontend.tabNav=true;
		for(d of this._this.html.shadowRoot.children)
		{
			if(d.className == "vis-contextmenu")
			{		
				d.style.cursor="none";
			}
		}
		// On doit également supprimer toutes les divs de overlay dispo
		for(d of this._this.html.shadowRoot.children)
			if(d.className == this._this.html.noeud.id+"_link")
				d.parentNode.removeChild(d);
		this._this.html.noeud.visjs_noeud.children[0].children[0].style.cursor="none";
		this._this.html.removeHoverEffects();
		this._this.sto.set();
		return true;
	}
	async _html_navtab_leave()
	{
		let d;
		if(!this._this.sto.settings.temp.frontend.tabNav) return false;
		this._this.sto.settings.temp.frontend.tabNav=false;
		for(d of this._this.html.shadowRoot.children)
		{
			if(d.className == "vis-contextmenu")
			{			
				d.style.cursor="initial";
			}
		}
		this._this.html.noeud.visjs_noeud.children[0].children[0].style.cursor="grab";
		this._this.sto.set();
		return true;
	}
	/* But: enlever les popups et effets de subbrillance lié au dernier mousemove enregistré par le canvas */
	_html_removeHoverEffects()
	{
		let a,coo;
		for(a=0;a<10;a++) // on réessaye 10 fois avant de laisser tomber
		{
			coo={x:Math.random()*window.innerWidth,y:Math.random()*window.innerHeight};
			if(this._this.f.vis.getNodeAt(coo)==undefined)
			{
				this._this.f.api.click("mousemove",coo);
				break;
			}
		}
	}
	/* But: Afficher/Cacher favaCarte, sans détruire le context vis Network */
//	_html_toggle_render(active,immediate=false)
	_html_render_show(immediate=false)
	{
		let old = this._this.html.noeud.style.transition,i,v;
		if(immediate)
			this._this.html.noeud.style.transition="";
		
		if(!fav.backend.storage.settings.temp.backend.active)
		{
			this._this.html.contextmenu.delete();
			for(i of this._this.html.stylesheets.last().children)
				i.parentNode.removeChild(i);

			this._this.html.context.CSS=[];
			this._this.html.noeud.visjs_noeud.children[0].children[0].style.cursor 					= "initial";
			document.documentElement.style.scrollbarWidth 											= "initial";
			this._this.html.noeud.style.opacity 													= 0;
			if(immediate)
			{
				this._this.html.noeud.style.zIndex													= (-1)*this._this.html.context.zIndex;
			}
			else
			{
				Promise.resolve(this._this.html).then(function(this_scope) {
					setTimeout(function(scope) {
		  				scope.noeud.style.zIndex														= (-1)*scope.context.zIndex;
		  			}, 0.75*this_scope._this.sto.settings.frontend.interface.transition.opacity,this_scope);
				}, function(this_scope) {});
			}
			window.document.documentElement.focus();
		}
		else
		{
			v = this._this.sto.settings.frontend.interface.body_blur;
			this._this.html.stylesheets.last().appendChild(document.createTextNode("body { -webkit-filter: blur("+v+"); filter: blur("+v+");}"));
			if(this._this.sto.settings.temp.frontend.tabNav)
				this._this.html.noeud.visjs_noeud.children[0].children[0].style.cursor 				= "none";
			else
				this._this.html.noeud.visjs_noeud.children[0].children[0].style.cursor 				= "grab";
			document.documentElement.style.scrollbarWidth 											= "none";
			this._this.html.noeud.style.opacity 													= this._this.sto.settings.frontend.interface.opacity;
			this._this.html.noeud.style.zIndex														= this._this.html.context.zIndex;
			this._this.html.noeud.visjs_noeud.children[0].focus();
		}
		this._this.html.noeud.style.transition=old;
	} // vérifié
    /* But: initialiser l'interface html/vsjs */
    async _start(visible=false,html_id="fav_render_context") 
    {
    	let s,_this,document_root,div,ptr;
    	/* Fixation de l'API */
		this.backend.tools.lock_api();
    	this.backend.tools.javascript_patch(this);
    	await this._this.backend.storage.get();

		// Chargement de la structure html
		this._this.html.noeud 											= window.document.createElement("div");										// Noeud html
		this._this.html.noeud.id  										= html_id;																	// Propriété id="" du noeud HTML
		this._this.html.noeud.visjs_noeud 								= window.document.createElement("div");
		this._this.html.noeud.visjs_noeud.id 							= html_id+"_visjs";
		this._this.html.noeud.logo 										= window.document.createElement("div");
		this._this.html.noeud.logo.draggable                         	= false;                                                                    /* Eviter l'effet image glissé */
		this._this.html.noeud.logo.id 									= html_id+"_img";
		this._this.html.noeud.logo.image 								= window.document.createElement("img");
        this._this.html.noeud.logo.image.draggable                    	= false;
		this._this.html.noeud.logo.image.id 							= html_id+"_img_img";
		// Application des préférences sur les styles
		this._this.html.noeud.style.height 								= window.String(window.innerHeight)+"px"; 									// Hauteur en pixels de la fenetre
		this._this.html.noeud.style.width								= window.String(window.innerWidth)+"px";
		this._this.html.noeud.style.zIndex								= (-1)*this._this.html.context.zIndex;											// Mettre une valeur par défaut au z-index
		if(!visible)
			this._this.html.noeud.style.opacity="0";
		

		this._this.html.noeud.style.transition							= "opacity "+window.String(this._this.sto.settings.frontend.interface.transition.opacity*10**-3)+"s";// Mettre une valeur par défaut à la transition (pallier au manque de l'API javascript)
		this._this.html.noeud.style.color 					 			= this._this.sto.settings.frontend.interface.font.color;
		if(this._this.sto.settings.frontend.interface.font.bold)
			this._this.html.noeud.style.fontWeight						= "bold";
		this._this.html.noeud.visjs_noeud.style.background 				= this._this.sto.settings.frontend.interface.background;
		this._this.html.stylesheets 									= [document.createElement('link'),document.createElement('style')];
		this._this.html.stylesheets.first().rel="stylesheet";
		this._this.html.stylesheets.first().href=(await browser.runtime.getURL("css/fav_ui.css"));
		this._this.html.noeud.logo.image.src  							= await browser.runtime.getURL(this._this.sto.settings.frontend.interface.images.logorc);
		
		this._this.html.noeud.logo.appendChild(this._this.html.noeud.logo.image);
		this._this.html.noeud.appendChild(this._this.html.noeud.logo);
		this._this.html.noeud.appendChild(this._this.html.noeud.visjs_noeud);

		this._this.html.stylesheets.last().type="text/css";
		for(s of this._this.backend.storage.settings.frontend.interface.CSS)
			this._this.html.stylesheets.last().appendChild(document.createTextNode(s));
		for(s of this._this.html.stylesheets)
			this._this.html.noeud.appendChild(s);

		// intercation utilisateur
		this._this.html.noeud.visjs_noeud.addEventListener("contextmenu",_frontend_handle_contextmenu);
		_this=this._this;
		this._this.html.noeud.visjs_noeud.addEventListener("pointerdown",async function(e) {

			if(e.buttons != 1) // pour éviter d'intercepter les contextmenu events
				return;

			_this.html.contextmenu.delete();
			if(_this.sto.settings.temp.backend.active)
			{
				if(_this.sto.settings.temp.frontend.tabNav)
					e.target.style.cursor='none';
				else
					e.target.style.cursor='grabbing';
			}
		});

		this._this.html.noeud.visjs_noeud.addEventListener("pointerup",function(e) {
			if(_this.sto.settings.temp.backend.active)
			{
				if(_this.sto.settings.temp.frontend.tabNav)
					e.target.style.cursor='none';
				else
					e.target.style.cursor='grab';
			}
		}); 

    	// Insertion dans l'arbre
	   	document_root 	= window.document.documentElement;
	   	div = document_root.insertBefore(document.createElement("div"),document_root.firstChild);
	   	div.style.width="0px";
	   	div.style.height="0px";
	   	ptr = div.attachShadow({mode: "closed"});
	   	this._this.html.shadowRoot=ptr;

	   	ptr.appendChild(this._this.html.noeud);
		if(this._this.sto.settings.frontend.interface.hideLogo)
			this._this.html.noeud.logo.hidden=true;
		
		if(this._this.sto.settings.frontend.interface.hideRootLogo)
			this._this.sto.settings.frontend.interface.images.default.root.image=this._this.sto.settings.frontend.interface.images.default.folder.image;
	
		/*
			// NOK : essai implémentation avec la shadow root
			let div = document.createElement("div");
			document_root.insertBefore(div,document_root.firstChild);
			let ptr = div.attachShadow({mode: "closed"});
			ptr.appendChild(this._this.html.noeud);
		*/

		// Initialisation des images par défaut
		// Images initiales : [folder,bookmark,broken,root] //
		try{if(this._this.backend.storage.settings.backend.debug.start)
		{
			this._this.tools.group("start");
			this._this.tools.console("all images loaded: this._this.sto.settings.frontend.images.default=","d",this._this.sto.settings.frontend.images.default);
			this._this.tools.groupEnd();
		}} catch(e) {}				
		// Lecture du stockage interne
		this._this.backend.tools.javascript_patch(this);
		await this._this.frontend.put_to_runtime();
		_html_attach_events();
		if(this._this.sto.settings.frontend.interface.cross!=undefined&&this._this.sto.settings.frontend.interface.cross.enabled)
		this._this.html.show_center();
		await this._this.sto.set(); // pour sauver les images en particulier
	} // vérifié
	_stop(html_id="fav_render_context")
	{
		this._this.html.render_show(true);
		delete this._this.f.vis;
		this._this.html.noeud.parentNode.removeChild(this._this.html.noeud);
	}
   async regression()
   {
   		await this._this.frontend.regression();
   }
}

//let _frontend_handle_click=_frontend_handle_doubleclick;
/*
 * But : gérer le menu context (le clic)
 */
function _frontend_handle_contextmenu(e)
{
	e.preventDefault();
	e.stopImmediatePropagation();
	e.target.parentNode.parentNode._this.html.contextmenu.show(e);
}
/* But: Intercepter la drag Edge et réagir en fonction */
async function _frontend_handle_addEdge(event, properties, senderId)
{
	try{if(fav.backend.storage.settings.backend.debug.frontend.handle_addEdge) {
		fav.tools.group("frontend.handle_addEdge");
		fav.tools.console("event","d",event);
		fav.tools.console("properties","d",properties);
		fav.tools.console("senderId","d",senderId);	}} catch(e) {}

	// On pose le cadre de travail
	let edge = fav.frontend.datasets.edges.get(properties.items[0]);
	if(edge==null)
		return false;

	await fav.f.appendEdge(edge);

	// désactivation du handler
	fav.frontend.datasets.edges.off("add",_frontend_handle_addEdge);

	try{if(fav.backend.storage.settings.backend.debug.frontend.handle_addEdge)
		fav.tools.groupEnd();} catch(e) {}
}















/*
 *
 * Gestion des évènements du frontend
 *
 */
window.addEventListener("load",async function()
{
	document.documentElement.style.padding="0";
	window.fav = new Fav();

	await window.fav.start();				// démarrage asynchrone (tout l'arbre DOM n'est pas encore créer)

	let endloop = false;
	while(!endloop)
	{
		try{
			endloop=true;
			if(fav.backend.storage.settings.temp.backend.active)
			{
				fav.h.render_show(true);		 	// le toggle par définition va changer la valeur, ce qu'on ne veut pas ici
			}
			else
				 await fav.tools.sleep(100);
		} catch(e) { await fav.tools.sleep(100); }
	}
});

function _html_attach_events(prod=false)
{
	fav.html.noeud.addEventListener("mousemove",function(e){
		this._this.html.navtab.leave();
	});
	// Deplacer la vue avec les touches du clavier
	fav.html.noeud.addEventListener("keydown",async function (event)
	{
//		console.warn("START");
		if (event.defaultPrevented||fav.html.context.active)
			return;
		
		// Gestion des touches
		switch(event.key) {
			case fav.sto.settings.frontend.controller.deplacement.d.b:
			case fav.sto.settings.frontend.controller.deplacement.d.h:
			case fav.sto.settings.frontend.controller.deplacement.d.g:
			case fav.sto.settings.frontend.controller.deplacement.d.d:
			case fav.sto.settings.frontend.controller.deplacement.escape:
			case fav.sto.settings.frontend.controller.deplacement.submit:
				this._this.html.navtab.enter();
				event.preventDefault();
				event.stopImmediatePropagation();
				break;
			default:
				return;
		}
		if(fav.html.contextmenu.is_active())
		{
			let e = new KeyboardEvent("keydown", event);
			fav.html.contextmenu.dispatchEvent(e);
			return;
		}
		if(event.key==fav.sto.settings.frontend.controller.deplacement.submit)
		{
			////console.warn("A PARTIR DE LA : TODO");
			// Implémenter la double entrée et déplacement de noeud
			//
			//
		  	fav.f.open_nearest_bookmark();
		}
		  	

		// Gestion du clavier
		if(fav.f.kp_accu_decharge)
			return;
		fav.f.kp_accu_decharge=true;
		let x=0,y=0;
		switch(event.key)
		{
			case fav.sto.settings.frontend.controller.deplacement.d.b:
				x=0;y=-fav.sto.settings.frontend.controller.deplacement.vitesse;
				break;
			case fav.sto.settings.frontend.controller.deplacement.d.h:
				x=0;y=+fav.sto.settings.frontend.controller.deplacement.vitesse;
		  		break;
			case fav.sto.settings.frontend.controller.deplacement.d.g:
				x=+fav.sto.settings.frontend.controller.deplacement.vitesse;y=0;
		  		break;
			case fav.sto.settings.frontend.controller.deplacement.d.d:
				x=-fav.sto.settings.frontend.controller.deplacement.vitesse;y=0;
		  		break;
		}
		fav.f.vis.moveTo(
			{offset: {x:x,y:y},
			animation:{
				duration: fav.backend.storage.settings.frontend.controller.deplacement.anim.total,
				easingFunction: fav.backend.storage.settings.frontend.controller.deplacement.anim.fname
		}});

		await fav.tools.sleep(fav.backend.storage.settings.frontend.controller.deplacement.anim.total);

		fav.frontend.highlight_nearest();
	//	fav.frontend.to_groups();
	//	await fav.sto.set();
		fav.f.kp_accu_decharge=false;
	//	console.warn("END");
	}, true);

	window.addEventListener("resize",function() {
		if(fav==undefined) return;
		fav.html.noeud.style.height 						= String(window.innerHeight)+"px";
		fav.html.noeud.style.width							= String(window.innerWidth)+"px";	
	});
	/*
	 *
	 * Gestion des events avec le backend
	 * Centre de communication:
	 * 		communication avec bakground.js
	 *
	 */
	//browser.runtime.onMessage.addListener(function(obj, sender, sendResponse)
	browser.runtime.onMessage.addListener(async function(obj, sender, sendResponse)
	{
		if(fav==undefined) return;
		let return_statut = {statut: "ndone"};
		try{if(fav.backend.storage.settings.backend.debug.backend.communication.recv)
			{
				fav.tools.group("backend.communication.recv");
				fav.tools.console("obj=","d",obj);
				fav.tools.console("request=","d",obj.request);
				fav.tools.console("sender=","d",sender);
				fav.tools.console("sendResponse=","d",sendResponse);
				fav.tools.groupEnd();
			}
		}
		catch(e) { }
		if(obj.statut=="debug_regression")
		{
	//		fav.tools.console("Pour que le test de l'interface marche, Maj+Maj+d puis F5","warn");
		/*	if(obj.value.verb) {
				let dconf="backend.debug.frontend.append=boolean=true;;frontend.interface.physics.enabled=boolean=false;;";
				dconf+="backend.debug.frontend.get_nearest_from_position=boolean=true;;backend.debug.frontend.delete=boolean=true;;";
				dconf+="backend.debug.backend.storage.Groups.delete=boolean=true;;backend.debug.backend.storage.Bookmarks.delete=boolean=true;;";
				fav.tools.parsePref(dconf);
			} else{
				let dconf="backend.debug.frontend.append=boolean=false;;frontend.interface.physics.enabled=boolean=false;;";
				dconf+="backend.debug.frontend.get_nearest_from_position=boolean=false;;backend.debug.frontend.delete=boolean=false;;";
				dconf+="backend.debug.backend.storage.Groups.delete=boolean=false;;backend.debug.backend.storage.Bookmarks.delete=boolean=false;;";
				fav.tools.parsePref(dconf);
			}*/
			obj = fav.api.Object.copy({value: {controle:0xFFFFFF,more_tests:false,debug:false,verb:false}},obj);
			await fav.frontend.regression(obj.value.controle,obj.value.more_tests,obj.value.debug,obj.value.verb);
			fav.stop();
			delete window.fav;
		}
		if(obj.statut=="ui_render_show")
		{
			if(obj.value!=undefined)
				fav.h.render_show(obj.value);
			else
				fav.h.render_show();
		}
		if(obj.statut=="sto_read")
		{
			await fav.sto.get();
			await fav.frontend.put_to_runtime();
		}
		if(obj.statut=="sto_read_exceptUI")
		{
			let a,b,c=(Object.keys(fav.sto.groups)).length;
			await fav.sto.get();
	        fav.f.to_groups(); // probleme ici : le dataset contient une edge qui fait référence à l'ancien ID et pas au nouveau
			a=(Object.keys(fav.sto.groups)).length;
			await fav.frontend.put_to_runtime();
			await fav.sto.set();
			b=(Object.keys(fav.sto.groups)).length;
		//	//console.warn("fav.sto.groups=",fav.sto.groups,",a=",a,",b=",b,",c=",c);
		}
		if(obj.statut == "sto_write")
		{
			fav.f.to_groups();
			await fav.sto.set();
		}
		return Promise.resolve(return_statut);
	});










	// bloquer les touches en Maj+Maj+<KEY> dans le version de production
	if(prod) return;



	/***
		Interface de débuggage
		Maj+Maj+e.key
	***/
	let kp_accu = [];
	let last_pref=undefined;
	let last_css=undefined;
	window.addEventListener("keydown",async function(e)
	{
		if(kp_accu.length < 2)
		{
			if(e.key == "Shift" )
				kp_accu.push(true);
			else
				kp_accu=[];
			return;
		}
		let txt,dconf;
		

		switch(e.key)
		{	
			case "a":
				let d = new vis.DataSet({id: 1,title: "ok"});
				await browser.storage.local.set({["TEST"]: d});
				let d1=await browser.storage.local.get("TEST");
				console.warn("d=",d,",d1=",d1);
			break;
			case "b":
			break;
			case "c":
				fav.f.vis.moveTo({position:{x:0,y:0},scale:1});
				fav.html.show_center(1,50);
			break;
			case "d":
				fav.tools.console("charger la config de debug","i");
				fav.tools.console("You need to reload to see all changes","w");
				dconf="frontend.controller.deplacement.anim.total=number=1;;frontend.controller.deplacement.anim.fname=string=linear;;frontend.controller.deplacement.vitesse=number=10;;";
				dconf+="frontend.interface.font.color=string=green;;frontend.interface.ctx.font.color=string=green;;frontend.interface.ctx.font.bold=boolean=true;;frontend.interface.hideLogo=boolean=true;;frontend.interface.hideRootLogo=boolean=true;;";
				dconf+="frontend.interface.background=string=black;;frontend.interface.opacity=number=1;;";
				dconf+="frontend.interface.ctx.background=string=red;;frontend.interface.ctx.font.color=string=blue;;";
				dconf+="frontend.interface.ctx.textfield.font.bold=boolean=true;;frontend.interface.ctx.textfield.font.color=string=blue;;frontend.interface.ctx.textfield.background=string=yellow;;";
				dconf+="frontend.interface.CSS=CSS=div.vis-tooltip > * { background-color: red;color:blue;};;";				
				dconf+="frontend.interface.ctx.hover.background=string=#36FF04;;frontend.interface.transition.opacity=number=0;;";
				dconf+="frontend.interface.physics.enabled=boolean=false;;";
				dconf+="frontend.controller.deplacement.get_nearest_circle=number=100;;";
				fav.backend.tools.parsePref(dconf);
				await fav.sto.set();
				await fav.frontend.put_to_runtime();
			break;
			case "e":
			break;
			case "Shift":
			case "h":
				console.log("\
	Favacarte client debugging tools\n\
	  f : display fav object\n\
	  v : display vis object\n\
	  s : await fav.sto.set()\n\
	  t : bookmark_change to ds\n\
	  g : display runtime storage\n\
	  k : kill ui\n\
	  c : reset view 0,0 scale 1\n\
	  u : start ui\n\
	  p : preferences\n\
	  d : charger la config de debug\n\
	  z : reset la config de debug\n\
	  r : make some tests\n\
	");
				break;
			case "r":
				await fav.frontend.regression();
				break;
			case "w":
				await fav.f.regression(0x10);
				//fav.f.api.click("pointerdown",0,0);
			//	fav.html.noeud.visjs_noeud.children[0].children[0].dispatchEvent(new PointerEvent("pointerdown",{x: 0,y:0,clientX:0,clientY:0}));
			//	//console.warn("dispatched");
				break;
			case "y":
			//	fav.f.vis.addEdgeMode();
				fav.f.vis.moveTo({position: {x:0,y:0}});	
				break;
			case "x":
				let copy=ev_saved.slice(),positionsVISJS;
				ev_saved=[];
			//	//console.warn("Sequence of replay: ev_saved=",copy);
				(async function()
				{
					for(let ev of copy)
					{
						switch(ev.type)
						{
							case "mousemove":
								positionsVISJS=fav.f.coord_DOM2visjs(ev.clientX,ev.clientY);
			//					//console.warn("ev.clientX=",ev.clientX,",ev.clientY=",ev.clientY);
			//					//console.warn("positionsVISJS.x=",positionsVISJS.x,",positionsVISJS.y=",positionsVISJS.y);
						//		fav.f.vis.moveTo({position:  positionsVISJS});								
						//		fav.html.noeud.dispatchEvent(ev);
//								fav.html.noeud.visjs_noeud.children[0].children[0].dispatchEvent(ev);
								break;

							case "pointerdown":
								fav.html.noeud.visjs_noeud.children[0].children[0].dispatchEvent(ev);
								break;
						}	
						await fav.tools.sleep(200);
					}
				})();
				
				break;
			case "z":
			//	dconf ="backend.debug.backend.storage.Bookmarks.delete=boolean=true;;";
			//	dconf+="backend.debug.backend.storage.Groups.delete=boolean=true;;";
			//	dconf+="backend.debug.backend.storage.Groups.unlink=boolean=true;;";
			//	dconf+="backend.debug.frontend.delete=boolean=true;;";
				dconf ="backend.debug.backend.storage.BookmarkTreeNode.push=boolean=true;;";
				dconf+="backend.debug.frontend.delete=boolean=true;;";
				fav.backend.tools.parsePref(dconf);
				await fav.f.regression(0x2);
				break;
			case "f":
				fav.tools.console("display fav object","i");
				fav.tools.console("fav=","i",fav);
				break;
			case "v":
				fav.tools.console("display vis object","i");
				fav.tools.console("this._this.frontend.vis=","i",fav.frontend.vis);
				break;
			case "s":
				fav.html.noeud.visjs_noeud.children[0].focus();
				break;
			case "l":
				//let ddd = fav.html.shadowRoot.firstChild.getElementById("fav_render_context_visjs");
				//console.warn("ddd=",ddd,",>",ddd.children[0].children[0]);
				let ddd = fav.html.shadowRoot.firstChild.getElementsByClassName("vis-network")[0].children[0];
				ddd.focus();
//				ddd.addEventListener("mousemove",function(e){
//					console.warn("e=",e);
//				});
				let dddn=new MouseEvent("mousemove",{clientX: 100,clientY:100,layerX:100,layerY:100});
				ddd.dispatchEvent(dddn);
				console.warn(">",fav.html.shadowRoot.firstChild);
				fav.html.shadowRoot.firstChild.dispatchEvent(dddn);
				let n = new PointerEvent("pointerup",{});
				fav.html.shadowRoot.firstChild.children[1].dispatchEvent(n);
				fav.html.shadowRoot.firstChild.children[1].focus();
			//	document.getElementById("fav_render_context").dispatchEvent(dddn);
			//	console.warn("ddd=",ddd);
				console.warn("fav.html.shadowRoot.firstChild",);
				break;
			case "m":
				fav.frontend.highlight_nearest();
				break;
			case "g":
				fav.tools.group("display runtime storage");
				fav.tools.console("images=","info",fav.sto.images);
				fav.tools.console("settings=","info",fav.sto.settings);
				fav.tools.console("groups=","info",fav.sto.groups);
				fav.tools.console("bookmarks=","info",fav.sto.bookmarks);
				fav.tools.groupEnd();
				break;
			case "t":
			 	fav.tools.console("bookmark_change to ds","i");
				await fav.com.envoi("bookmark_change");
				break;
			case "k":
				fav.tools.console("kill ui","i");
				fav.stop();
				delete window.fav;
				break;
			case "u":
				fav.tools.console("start ui","i");
				window.fav = new Fav();
				window.fav.start();	
				break;
			case "p":
				fav.tools.group("preferences");
				fav.tools.console("You need to reload to see all changes","w");
				fav.tools.groupEnd();
				if(last_pref==undefined)
					last_pref="debug.backend.storage.set";
				txt = prompt("pref:",last_pref);
				if (!(txt == null || txt == "") ) {
					fav.backend.tools.parsePref(txt);
					await fav.sto.set();
					last_pref=txt;
					await fav.frontend.put_to_runtime();

					await fav.com.envoi({statut: "options_settings",value: "change"});
				}
				break;
			default:
				break;
		}
		if( e.key != "Shift" )
			kp_accu=[];
	});




}

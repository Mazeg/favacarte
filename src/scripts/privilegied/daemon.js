/*
    Copyright (C) 2020-present Aïdo WILHELM

    This file is part of favacarte.

    favacarte is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    favacarte is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with favacarte.  If not, see <https://www.gnu.org/licenses/>.
*/
/**
 **
 **			Script Daemon - un script avec beaucoup de privilèges, pouvant réaliser de profondes transformations sur le navigateur
 **
 **/
"use strict";
var FAV_ROOT 			= "toolbar_____";
var FAV_VISJS   		= "/scripts/content/vis.patch.js";
/*
 * Une version réduite de la classe Fav
 * But à long terme : placer un maximum du code de content-script/backend dans daemon/backend (pour des raisons de performances)
 */
class FavDaemon {
	/* Construire l'objet avec les différents context dont on a besoin */
	constructor(storage_name="FavaCarte_storage")
	{
		this.html 												= {};
		this.html.context 										= {};
		this.frontend 											= {}; 					// outils qui touchent à l'interface
		this.frontend.config 									= new ConfigDefaut; 	// Config par défaut "hardcoded"
		this.backend											= {};					// outils qui ne touche pas directement à l'interface														// aller à droite
		this.backend.storage={ 															// outils de gestion du stockage interne
			bookmarks: 											{}, 					// Favoris selon la structure interne
			images: 				 							[], 					// Images à injecter dans le front end
			groups: 				 							{},
			settings: 											undefined
		};
 		this.backend.storage.Groups 							= new Groups;			// structure de Groups, niveau niveau juste avant l'interface visjs - sto.groups
  		this.backend.storage.Bookmarks 							= new Bookmarks;		// structure de Bookmarks  - sto.bookmarks
  		this.backend.storage.BookmarkTreeNode 					= new BookmarkTreeNode;	// structure de BookmarkTreeNode - interne firefox
		this.backend.tools 										= new Tools("ds");			// outils léger qui simplifie les calculs
		this.backend.communication 								= {};					// outils de communication avec le script de background
		this.api  											    = new vAPI; 			// rédéfinition de l'API permettant de faire de la compat entre nav, ainsi que des tests.

		this.backend.storage.name 							 	= storage_name; 		 				// Nom de l'espace de stockage local
		this.backend.storage.newwindow_accu 					= []; 									// Accumulateur permettant de simuler l'ouverture tout dans une fenetre
		this.backend.storage.cbc_accu 							= []; 									// Accumulateur d'event de type bookmark : permet d'éviter la redondance d'ajout
		this.backend.storage.event_accu							= [];									// Un Accumulateur d'évènements : la FEATURES d'endiga	tion des events utilisateurs
		this.backend.storage.verrou_anti_grele 					= false;								// Permet de vérouiller lexecution de script lors de
		this.backend.unactivated_tabs                           = [];									// Stock les IDs des onglets sur lesquels l'extension est desactivee
		this.backend.workers 									= [this.backend.storage.Bookmarks.sync_icons_worker];			// Une liste de fonction asynchrones à exécuter (pas de params please)
		this.html.context.active								= false;								// Etat actuel de l'interface													// L'interface HTML est-elle visible



		/**
		 **		API d'interaction du daemon favacarte favaCarte
		 **/
		// attributs
		this.f      = this.frontend;
		this.b   	= this.back    				= this.backend;
		this.sto    = this.storage      		= this.backend.storage;
		this.com    = this.communication 		= this.backend.communication;
		this.tools  = this.backend.tools;
		this.Bookmarks = this.backend.storage.Bookmarks;
		this.BookmarkTreeNode = this.backend.storage.BookmarkTreeNode;
		this.Groups = this.backend.storage.Groups;


		this.api.sendMessage 									= this._api_sendMessage;
		this.api.reset 											= this._api_reset;
		this.backend.cs_allowed 								= this._backend_cs_allowed;
		this.backend.reset_cache 								= this._backend_reset_cache;
		this.backend.sync_usettings 							= this._backend_sync_usettings;
		this.backend.communication.envoi 					    = this._backend_communication_envoi;
		this.backend.storage.get   								= this._backend_storage_get;
		this.backend.storage.set 								= this._backend_storage_set;
		this.show_help  										= this.show_help;

		this.frontend.toggle_iface 								= this._frontend_toggle_iface;
		this.start = this._start;
		this.regression 										= _regression;
    }
	/* But: redéfinir l'api avec laquelle on intéragit, en fournissant ces choses, quand je programme je n'ai pas à me soucier de certains détails */
	async _api_sendMessage(tabId,message,options)
	{
		if(this._this.backend.unactivated_tabs.indexOf(tabId)==-1)
		{
			try { await browser.tabs.sendMessage(tabId,message,options); } catch(e) {return false;}
			return true;
		}
		return false;
	}
	/* But: faire un reset du stockage interne (perdre tous les réglages utilisateurs) */
	async _api_reset(soft_rst=false)
	{
		if(soft_rst)
		{
			let k;
			await this._this.sto.get();
			await browser.storage.local.remove("settings");
			this._this.backend.reset_cache();
			this._this.sto.settings.frontend=this._this.api.Object.copy(this._this.frontend.config.frontend);
			this._this.sto.settings.backend=this._this.api.Object.copy(this._this.frontend.config.backend);			
			for(k of Object.keys(this._this.sto.settings.frontend.interface.images.default))
				this._this.sto.settings.frontend.interface.images.default[k].image = await browser.runtime.getURL(this._this.sto.settings.frontend.interface.images.default[k].image);

		}
		else
		{
			await browser.storage.local.remove(["settings","groups","bookmarks","images"]);
			this._this.start();
			this._this.backend.reset_cache();
		}
		await this._this.sto.set();
		await this._this.com.envoi("sto_read",{broadcast:true});
	}
	/* But: Mémoriser les onglets dont on a l'accès en injection content-script  */
	_backend_cs_allowed(x,y=undefined)
	{
		let t = typeof(x),index;
		if(t == "number")
		{
			if(this._this.b.unactivated_tabs.indexOf(x)!=-1)
			{
				return false;
			}
			// todo : gérer le cas ou on a pas l'url : on doit donc le requetter puis faire un test sur sa forme
		}
		if(t == "string")
		{
			if( ! this._this.tools.host_permission(x))
			{
				if(y!=undefined && typeof(y)=="number")
				{
					if(this._this.backend.unactivated_tabs.indexOf(y)==-1)
					{
						this._this.backend.unactivated_tabs.push(y);
						// todo : gérer le cas ou y n'est pas fournit (query un(s) tab(s) qui a bien cet url de chargé)
					}
				}
				return false;
			}
			else
			{
				index = this._this.backend.unactivated_tabs.indexOf(y);
				if(index!=-1)
				{
					this._this.backend.unactivated_tabs.splice(index,1);
				}
			}
		}
		return true;
	}
	/* But: décharger les stockages temporaires qui ont besoin de l'être */
	_backend_reset_cache()
	{
		this._this.backend.storage.cbc_accu 							= [];
		this._this.backend.storage.event_accu							= [];
		this._this.backend.storage.verrou_anti_grele 					= false;
	}
	/* But: chopper les pref utilisateur et les mettre dans le stockage interne */
	async _backend_sync_usettings()
	{
		await this._this.com.envoi("sto_write");
		await this._this.sto.get();
		let openBookmarksInNewTabs =  await browser.browserSettings["openBookmarksInNewTabs"].get({});

		this._this.backend.storage.settings.frontend.controller.openBookmarksInNewTabs = openBookmarksInNewTabs.value;

				//TODO: refresh storage in all tabs
		await this._this.sto.set();
		await this._this.com.envoi("sto_read",{broadcast:true});
	}
	/* But: un message sur des onglets */
	async _backend_communication_envoi(message,options=undefined)
	{
		try{if(this._this.backend.storage.settings.backend.debug.backend.communication.envoi)
		{
			this._this.tools.group("backend.communication.envoi");
			this._this.tools.console("message=","d",message);
			this._this.tools.groupEnd();
		}} catch(e) {}
		if(typeof(message)==="string")
			message={statut: message};

		let tabs,tab;
		if(options == undefined)
		{
			tabs = await browser.tabs.query({currentWindow: true,active: true});
			console.assert(tabs.length<=1);
			tab = tabs[0]; // On ne récupère que l'onglet actif normalement
			await this._this.api.sendMessage(tab.id,{statut: message.statut, value: message.value});
		}
		else if(options.broadcast) for(tab of (await browser.tabs.query({})))
		{
			await this._this.api.sendMessage(tab.id,{statut: message.statut, value: message.value});
		}
	}
	/**
	 *
	 * Gestion du stockage interne
	 *
	 **/
	async _backend_storage_get()
	{
		let temp  					 			= await browser.storage.local.get();
		this._this.backend.storage.bookmarks 	= temp.bookmarks;
		this._this.backend.storage.images   	= temp.images;
		this._this.backend.storage.settings 	= temp.settings;
		this._this.backend.storage.groups 		= temp.groups;

		if(this._this.backend.storage.images == undefined)
			this._this.backend.storage.images = [];
		// Si les settings ne sont pas chargés, on utilise ceux par défaut
		if(this._this.backend.storage.settings == undefined)
			this._this.backend.storage.settings = this._this.api.Object.copy(this._this.frontend.config);
		if(this._this.backend.storage.bookmarks == undefined)
			this._this.backend.storage.bookmarks = {};
		if(this._this.backend.storage.groups == undefined)
			this._this.backend.storage.groups = {};
	
		return true;
	} // vérifié
	async _backend_storage_set()
	{
		await browser.storage.local.set({bookmarks: this._this.backend.storage.bookmarks, images: this._this.backend.storage.images, settings: this._this.backend.storage.settings, groups: this._this.backend.storage.groups});
	} // vérifié
	async _frontend_toggle_iface()
	{
	  	this._this.backend.storage.settings.temp.backend.active=!this._this.backend.storage.settings.temp.backend.active;  	
	  	await this._this.sto.set();
	  	let t = await browser.tabs.query({currentWindow: true,active: true});
	  	console.assert(t.length==1);
	  	t=t[0];
	  	let tabs = await browser.tabs.query({}),tab;
	  	for(tab of tabs)
	  	{
	  		if(tab.id == t.id)
	  		{
	  	  		await this._this.api.sendMessage(tab.id,{statut: "sto_read_exceptUI"});
	  			await this._this.api.sendMessage(tab.id,{statut: "ui_render_show"});
	  		}
	  		else
	  		{
	  			await this._this.api.sendMessage(tab.id,{statut: "sto_read"});
	  			await this._this.api.sendMessage(tab.id,{statut: "ui_render_show",value: true});
	  		}
	  	}
	}
    /* But: démarer l'interface */
    async _start()
    {
    	/* Fixation de l'API */
		this.backend.tools.javascript_patch(this);
		this.backend.tools.lock_api();
		let hardcoded_conf="frontend.interface.background=string=#ededf0;;frontend.interface.opacity=number=1;;frontend.interface.font.color=string=#ffffff;;frontend.controller.deplacement.vitesse=number=50;;frontend.controller.deplacement.anim.fname=string=linear;;frontend.controller.deplacement.anim.total=number=0;;frontend.interface.font.color=#4a4a4f;;frontend.interface.nodes.chosen.shadow.color=array=[0,0,0,0.4];;frontend.interface.nodes.chosen.shadow.offset.x=number=0;;frontend.interface.nodes.chosen.shadow.offset.y=number=0;;frontend.interface.hideLogo=boolean=true;;frontend.interface.edges.color=array=[12, 12, 13, 0.3];;frontend.interface.nodes.chosen.shadow.size=number=2;;frontend.interface.ctx.background=string=#ffffff;;frontend.interface.CSS=CSS=div.vis-tooltip{border: 2px solid rgba(12, 12, 13, 0.2);background-color:white;color:black;};;frontend.interface.CSS=CSS=div.vis-contextmenu{color:black};;frontend.interface.ctx.textfield.font.color=string=black;;frontend.interface.ctx.hover.background=string=rgba(12, 12, 13, 0.3);;frontend.interface.ctx.textfield.background=string=white;;frontend.interface.ctx.font.color=string=black;;frontend.controller.deplacement.get_nearest_circle_link=number=100;;frontend.controller.deplacement.get_nearest_circle_ctxmenu=number=50;;frontend.interface.cross.enabled=boolean=true;;frontend.interface.cross.width=number=2;;frontend.interface.cross.len=number=40;;frontend.interface.cross.color=string=rgba(12, 12, 13, 0.3);;frontend.interface.min_opacity=number=1;;frontend.interface.physics.enabled=boolean=false;;frontend.interface.edges.smooth=boolean=false;;frontend.interface.hideRootLogo=boolean=true;;";
		this._this.backend.tools.parsePref(hardcoded_conf,this._this.frontend.config);
		this._this.backend.storage.settings=this._this.api.Object.copy(this._this.frontend.config);
		// Essaye de chopper des propriétés supplémentaires
		try{
			let url = Favicons.defaultFavicon.asciiSpec; 		// probleme : viens de la vielle interface XPCOM
			this._this.backend.storage.settings.frontend.interface.images.default.bookmark.image=url;
		}catch(e){}
		// démarrage des démons
    	for(let a = 0;a<this._this.backend.workers.length;a++)
    		this._this.backend.workers[a]();
    	// ajout des events listeners
    	browser.runtime.onMessage.addListener(_backend_communication_recv);
   		await this._this.sto.BookmarkTreeNode.init_build();
    }
   
    show_help()
	{
			console.log("\
daemon.js CONTROLS\n\
Shift+Ctrl+n\n\
 1 : SHOW HELP\n\
 2 : STORAGE SHOW\n\
 3 : STORAGE RESET\n\
 4 : STORAGE RESET(soft)\n\
 5 : RUNTIME SHOW(favd)\n\
 6 : TEST\n\
 7 : INTERFACE TOGGLE");
	}
}








 async function _regression(controle=0xFFFF,more_tests=false,debug=false,verb=false)
{
	let dconf,tab,tabs;
	let type = 0x1;
	if(type==0x1) {
		dconf= "frontend.controller.deplacement.anim.total=number=1;;frontend.controller.deplacement.anim.fname=string=linear;;frontend.controller.deplacement.vitesse=number=10;;";
		dconf+="frontend.interface.font.color=string=green;;frontend.interface.ctx.font.color=string=green;;frontend.interface.ctx.font.bold=boolean=true;;frontend.interface.hideLogo=boolean=true;;frontend.interface.hideRootLogo=boolean=true;;";
		dconf+="frontend.interface.background=string=black;;frontend.interface.opacity=number=1;;";
		dconf+="frontend.interface.ctx.background=string=red;;frontend.interface.ctx.font.color=string=blue;;";
		dconf+="frontend.interface.ctx.textfield.font.bold=boolean=true;;frontend.interface.ctx.textfield.font.color=string=blue;;frontend.interface.ctx.textfield.background=string=yellow;;";
		dconf+="frontend.interface.CSS=CSS=div.vis-tooltip > * { background-color: red;color:blue;};;";				
		dconf+="frontend.interface.ctx.hover.background=string=#36FF04;;frontend.interface.transition.opacity=number=0;;";
		dconf+="frontend.interface.physics.enabled=boolean=false;;";
		dconf+="frontend.controller.deplacement.get_nearest_circle=number=100;;";
		this._this.backend.tools.parsePref(dconf);
		this._this.sto.settings.temp.frontend.position={x:0,y:0};
		this._this.sto.settings.temp.frontend.scale=1;

		await this._this.sto.set();
		tabs = await browser.tabs.query({currentWindow: true,active: true});
		tab = tabs[0];
		await browser.tabs.reload(tab.id);
		await this._this.tools.sleep(2000);
		await this._this.com.envoi({statut: "debug_regression",value: {}});
		this._this.tools.group("backend");
	    	this._this.tools.regression();
		    this._this.tools.group("storage");
				await this._this.sto.Groups.regression();
			 	await this._this.sto.Bookmarks.regression();
		 	 	await this._this.sto.BookmarkTreeNode.regression();
	    	this._this.tools.groupEnd();
    	this._this.tools.groupEnd();
    }
    else if(type==0x2) {
		dconf= "frontend.controller.deplacement.anim.total=number=1;;frontend.controller.deplacement.anim.fname=string=linear;;frontend.controller.deplacement.vitesse=number=10;;";
		dconf+="frontend.interface.font.color=string=green;;frontend.interface.ctx.font.color=string=green;;frontend.interface.ctx.font.bold=boolean=true;;frontend.interface.hideLogo=boolean=true;;frontend.interface.hideRootLogo=boolean=true;;";
		dconf+="frontend.interface.background=string=black;;frontend.interface.opacity=number=1;;";
		dconf+="frontend.interface.ctx.background=string=red;;frontend.interface.ctx.font.color=string=blue;;";
		dconf+="frontend.interface.ctx.textfield.font.bold=boolean=true;;frontend.interface.ctx.textfield.font.color=string=blue;;frontend.interface.ctx.textfield.background=string=yellow;;";
		dconf+="frontend.interface.CSS=CSS=div.vis-tooltip > * { background-color: red;color:blue;};;";				
		dconf+="frontend.interface.ctx.hover.background=string=#36FF04;;frontend.interface.transition.opacity=number=0;;";
		dconf+="frontend.interface.physics.enabled=boolean=false;;";
		dconf+="frontend.controller.deplacement.get_nearest_circle=number=100;;";


		dconf+="backend.debug.frontend.appendEdge=boolean=true;;";
		dconf+="backend.debug.frontend.to_groups=boolean=true;;";
		dconf+="backend.debug.backend.storage.Groups.to_bookmarks=boolean=true;;";
		dconf+="backend.debug.backend.storage.Bookmarks.process_heritage_multiple=boolean=true;;";
		dconf+="backend.debug.backend.storage.Bookmarks.copy_branche_at=boolean=true;;";

//		dconf+="backend.debug.backend.storage.Bookmarks.correct=boolean=true;;";
//		dconf+="backend.debug.backend.storage.BookmarkTreeNode.push=boolean=true;;";
		this._this.backend.tools.parsePref(dconf);
		await this._this.sto.set();
		tabs = await browser.tabs.query({currentWindow: true,active: true});
		tab = tabs[0];
		await browser.tabs.reload(tab.id);
		await this._this.tools.sleep(2000);
		await this._this.com.envoi({statut: "debug_regression",value: {controle: 0x10}});
	}
	else if(type==0x3)
	{
		dconf= "backend.debug.backend.storage.Bookmarks.copy_branche_at=boolean=true;;";
		this._this.backend.tools.parsePref(dconf);
		await this._this.sto.Bookmarks.regression(0x20);
	}
	else if(type==0x4)
	{
		dconf= "backend.debug.backend.storage.Groups.unlink=boolean=true;;";
		dconf+="backend.debug.backend.storage.Bookmarks.delete=boolean=true;;";
		dconf+="backend.debug.backend.storage.BookmarkTreeNode.push=boolean=true;;";
		this._this.backend.tools.parsePref(dconf);
		await this._this.sto.Groups.regression(0x80);
	}
	else if(type==0x8)
	{
		dconf ="backend.debug.backend.storage.BookmarkTreeNode.push=boolean=true;;";
		this._this.backend.tools.parsePref(dconf);
		await this._this.sto.Groups.regression(0x8000);			
	}
	else if(type==0x10)
	{
		await this._this.sto.Bookmarks.regression();
	}

}







/* + Une centrale de connexion : recoit(create/Removed/move) => reviens sur son role de mise à jour des favoris */
// cette méthode ne peut pas être mise en locale scope
async function _backend_communication_recv(request, sender, sendResponse)
{
	try{if(favd.backend.storage.settings.backend.debug.backend.communication.recv)
		{
			favd.group("backend.communication.recv");
			favd.console("request=","d",request);
			favd.console("sender=","d",sender);
			favd.console("sendResponse=","d",sendResponse);
			favd.groupEnd();
		}
	}
	catch(e) { }
	if(request.statut=="options_settings")
	{
		switch(request.value)
		{
			case "reset":
			default:
				await favd.api.reset(true);
				break;
			case "change":
				await favd.sto.get();
				break;
		}
		for(let tab of await browser.tabs.query({}))
		    if(tab.id != sender.tab.id && favd.backend.cs_allowed(tab.url,tab.id))
		    	await browser.tabs.reload(tab.id);
	}
	if(request.statut=="bookmark_change")
	{
		await favd.sto.get();
		await favd.sto.BookmarkTreeNode.push();
		await favd.sto.set();
		await favd.com.envoi("sto_read",{broadcast:true});
	}
	if(request.statut=="bookmark_open")
	{
		if(!favd.sto.settings.frontend.controller.openLinkAndKeepInterface && favd.sto.settings.temp.backend.active)
			await favd.f.toggle_iface();
		let created,duplicating,updating,incognito=false;
		switch(request.value.location)
		{
			case "current":
				await browser.tabs.update(sender.tab.id,{url: request.value.url});
				break;
			case "newprivate":
				incognito=true;
			case "newwindow":
				if(request.value.url.match(/^file:\/\//)!=undefined)
				{
					favd.console(request.value.url+" can't be openned using this method","w");
					break;
				}
				favd.backend.storage.newwindow_accu.push(request.value.url);
				if(favd.backend.storage.newwindow_accu.length>1)
					break;
				await favd.tools.sleep(favd.backend.storage.settings.backend.api.bookmarks.newwindow_accu_sleep);
				created = 	await browser.windows.create({
					url:  		favd.backend.storage.newwindow_accu,
					incognito:  incognito
				});
				favd.backend.storage.newwindow_accu=[];
				break;
			case "newtab":
			default:
				duplicating = await browser.tabs.duplicate(sender.tab.id);
				updating = await browser.tabs.update(duplicating.id,{url: request.value.url});
		}
	}
	/* terminé */
}




















/*
 * Gestion des commandes control
 */
browser.commands.onCommand.addListener(async function(command)
{
	let stock,A,B,C,D,E,F,G,H;
	switch(command)
	{
  		case "fava_2":
			stock = await browser.storage.local.get();
			stock=stock.FavaCarte_storage;
			favd.tools.group("STORAGE SHOW");
			favd.tools.console("STORAGE SHOW(stock.images)","info",stock.images);
			favd.tools.console("STORAGE SHOW(stock.settings)","info",stock.settings);
			favd.tools.console("STORAGE SHOW(stock.bookmarks)","info",stock.bookmarks);
			favd.tools.console("STORAGE SHOW(stock.groups)","info",stock.groups);
			favd.tools.groupEnd();  
 		break;
		case "fava_3":
			await favd.api.reset();
  		break;
		case "fava_4":
		  	await favd.api.reset(true);
  		break;
  		case "fava_5":
			favd.tools.group("RUNTIME SHOW(favd)");
			favd.tools.console("RUNTIME SHOW(favd.sto.images)","info",favd.sto.images);
			favd.tools.console("RUNTIME SHOW(favd.sto.settings)","info",favd.sto.settings);
			favd.tools.console("RUNTIME SHOW(favd.sto.bookmarks)","info",favd.sto.bookmarks);
			favd.tools.console("RUNTIME SHOW(favd.sto.groups)","info",favd.sto.groups);
			favd.tools.groupEnd();
		break;
		case "fava_6":
		  	favd.tools.console("REGRESSION : testing components");
			await favd.regression();
			favd.tools.console("END REGRESSION");
  		break;
  		case "fava_7":
  		 	favd.frontend.toggle_iface();
  		break;
  		case "fava_8":
  			A = favd.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
			B = favd.sto.Groups.new({parents: [A.id],title: "B"});
			C = favd.sto.Groups.new({parents: [A.id],title: "C"});
			D = favd.sto.Groups.new({parents: [B.id,C.id],title: "D"});
			E = favd.sto.Groups.new({parents: ["toolbar_____"],title: "E"});
			await favd.sto.BookmarkTreeNode.push();
			await favd.tools.sleep(1000);
			await favd.sto.set();
			await favd.com.envoi("sto_read");
  		break;
  		case "fava_9":
  		/*	A = favd.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
			B = favd.sto.Groups.new({parents: [A.id],title: "B"});
			C = favd.sto.Groups.new({parents: [A.id],title: "C"});
			D = favd.sto.Groups.new({parents: [B.id,C.id],title: "D"});
			E = favd.sto.Groups.new({parents: [D.id],title: "E"});
			F = favd.sto.Groups.new({parents: [E.id],title: "F"});
			G = favd.sto.Groups.new({parents: [E.id],title: "G"});
			H = favd.sto.Groups.new({parents: [G.id,F.id],title: "H"});
			await favd.sto.BookmarkTreeNode.push();
			await favd.tools.sleep(1000);
			await favd.sto.set();
			await favd.com.envoi("sto_read");
		*/
			console.warn(">",await browser.storage.local.get());

  		break;
  		default:
		case "fava_1":
			favd.show_help();
  		break;
  	}
});

/*
 * On voudrait que ce genre de script soit chargé une fois, pour toutes les pages
 * visiblement ce n'est plus possible depuis le passage à la restriction web extension
 * creation d'onglet 
 */
browser.tabs.onCreated.addListener(async function(tabObj) {
	try{
		if(favd.backend.storage.settings.backend.debug.browser.tabs.onCreated)
		{
			favd.group("browser.tabs.onCreated");
			favd.console("tabObj=","d",tabObj);
			favd.groupEnd();
		}} catch(e) {}
	if(favd.b.cs_allowed(tabObj.url,tabObj.id))
		browser.tabs.executeScript(tabObj.id,{file: FAV_VISJS});
});
/*
 * Déclenché quand on recharge l'onglet : attention, c'est un série d'evenement qui est déclenchée pour une rechage
 *
 */
browser.tabs.onUpdated.addListener(async function(tabId, changeInfo, tabInfo) {
	try{
		if(favd.backend.storage.settings.backend.debug.browser.tabs.onUpdated)
		{
			favd.group("browser.tabs.onUpdated");
			favd.console("tabId=","d",tabId);
			favd.console("changeInfo=","d",changeInfo);
			favd.console("tabInfo=","d",tabInfo);
			favd.groupEnd();
		}} catch(e) {}
	if(favd.sto.verrou_anti_grele==false)
	{
		// Problème avec ces events : ne doivent pas être exécutés avec le chargement de la page
		// (sinon cause une suppression incoditionnelle des coordonnées)
		if( tabInfo.url != null)
		{
			await favd.sto.get(); 						// permet d'éviter l'effacement inconditionnel
			let h = (new URL(tabInfo.url)).host;
			let n = favd.sto.Bookmarks.check_host(h); 			// regarde dans la base s'il est présent ou non
			if( n != null && h!=null && h!= ""&&tabInfo.favIconUrl!=undefined&&tabInfo.favIconUrl!="")
			{
				favd.sto.images.push({url: h,image: tabInfo.favIconUrl});
			}

			await favd.sto.set(); 										// écrire les changements sur images avant des les envoyer sur le content-script
		//	await favd.sto.BookmarkTreeNode.pull({type: "just refresh",value: false});	// j'ai mis une icone à jour donc tu doit rafraichir ton U
			await favd.com.envoi("sto_read",{broadcast: true});
		}
		favd.sto.verrou_anti_grele=true;

		// pour parer la grêle, on créer un buffer : un booléen dans une promise
		// On met un bête système de verrou
		if(favd.b.cs_allowed(tabInfo.url,tabId))
			browser.tabs.executeScript(tabId,{file: FAV_VISJS});
		

		Promise.resolve().then(function() {
  			setTimeout(function() {
  				favd.sto.verrou_anti_grele=false; 	// libération du vérrou
  			},100);
	  	}); // on attend 100 ms avant de redonner le control au démon
		
	}
});
/*
 * Pour faire croire à l'utilisateur que rien n'a changé entre les pages
 * Evenement declenché quand on change d'onglet : 	soit par la creation dun nouvel onglet
 *													soit au clic sur un nouvel onglet
 * 
 */
browser.tabs.onActivated.addListener(async function(activeInfo) {
	try{if(favd.backend.storage.settings.backend.debug.browser.tabs.onActivated)
	{
		favd.group("browser.tabs.onActivated");
		favd.console("activeInfo=","d",activeInfo);
		favd.groupEnd();
	}} catch(e) {}
	await favd.api.sendMessage(activeInfo.previousTabId,{statut: "sto_write"});
	await favd.api.sendMessage(activeInfo.tabId,{statut: "sto_read"});
});
browser.tabs.onRemoved.addListener(function(tabId,removeInfo) {
	try{if(favd.backend.storage.settings.backend.debug.browser.tabs.onRemoved)
	{
		favd.group("browser.tabs.onRemoved");
		favd.console("tabId=","d",tabId);
		favd.console("removeInfo=","d",removeInfo);
		favd.groupEnd();
	}} catch(e) {}
	// Typiquement: cas ou il ne faut pas faire de mécanical toggle
	let index = favd.b.unactivated_tabs.indexOf(tabId);
	if(index!=-1)
		favd.b.unactivated_tabs.splice(index,1);
});



/*
 * Ecoute de l'API browser.bookmark : stockage des events dans un accumulateur
 */
for(let ev of ["Created","Moved","Changed","Removed"])
{
	browser.bookmarks["on"+ev].addListener(async function (id,bookmarkInfo) {
		////console.warn("on"+ev);return;
		try{if(favd.backend.storage.settings.backend.debug.browser.bookmarks["on"+ev]) {
			favd.tools.group("browser.bookmarks.on"+ev);
			favd.tools.console("id","d",id);
			favd.tools.console("bookmarkInfo","d",bookmarkInfo);
			favd.tools.console("favd.sto.cbc_accu","d",favd.sto.cbc_accu);		
			favd.tools.groupEnd();}} catch(e) {}
		if(favd.sto.cbc_accu.length > 0) { favd.sto.cbc_accu.shift();return ;}
		bookmarkInfo.id=id;
		await favd.sto.BookmarkTreeNode.pull({type: ev, value: bookmarkInfo});
	});
}
/*
 * Lancement du démon
 */
window.addEventListener("load", async function(){
	window.favd = new FavDaemon();
	favd.start();

 
 	for(let t of (await browser.tabs.query({})))
 		if(favd.backend.cs_allowed(t.url,t.id))
			browser.tabs.executeScript(t.id,{file: FAV_VISJS});

});
/*
 * Ecoute d'un éventuel changement dans les settings : Non implémenté dans firefox pour l'instant
 * https://developer.mozilla.org/fr/docs/Mozilla/Add-ons/WebExtensions/API/types/BrowserSetting
 * //Pour voir la différence, l'utilisateur devra ou bien recharger l'extension, ou bien fermer, réouvrir le nav.
 */


//browser.browserSettings["openBookmarksInNewTabs"].onChanged(favd.backend.sync_usettings);

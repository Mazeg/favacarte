/*
    Copyright (C) 2020-present Aïdo WILHELM

    This file is part of favacarte.

    favacarte is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    favacarte is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with favacarte.  If not, see <https://www.gnu.org/licenses/>.
*/
document.getElementById("tamere").style.backgroundColor="green";
console.warn("window=",window);
let r = document.documentElement;
r.style.backgroundColor="red";
r.style.width="100%";
r.style.border="none";
r.style.margin="0";
r.style.padding="0";
console.warn("browser.bookmarks.create=",browser.bookmarks.create,"browser.bookmarks=",browser.bookmarks);
 setInterval(function(){ console.warn("Hello"); }, 500);
/*
 * Toujours le meme probleme concernant le profilling du navigateur
 * window.innerWidth - window.outerWidth != 0 		:    sidebar open
 * window.innerHeight - window.outerHeight != 0 	:    devs tools open
 */
console.warn("window.innerWidth=",window.innerWidth);
console.warn("window.personalbar=",window.personalbar);
/*
 * window.personalbar
 */
/*
    Copyright (C) 2020-present Aïdo WILHELM

    This file is part of favacarte.

    favacarte is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    favacarte is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with favacarte.  If not, see <https://www.gnu.org/licenses/>.
*/
class FavOptions {
	constructor()
	{
		this.backend 						= {};
		this.html 							= {};
		this.backend.storage 				= {};
		this.backend.tools 					= new Tools;
		this.backend.communication 			= {};
		this.com = this.backend.communication;
		this.b = this.backend;
		this.sto=this.backend.storage;
		this.backend.tools.compilePref  	= this._backend_tools_compilePref;
		this.backend.storage.get 			= this._backend_storage_get;
		this.backend.storage.set			= this._backend_storage_set;
		this.backend.communication.envoi 	= this._backend_communication_envoi;
		this.html.status 					= this._html_status;
		this._backend_tools_javascript_patch();
	}
	_html_status(msg,type="success")
	{
		let p = document.getElementById("favacarte-statut");
		let c,d;
		for(c of p.childNodes)
			c.parentNode.removeChild(c);

		d = document.createElement("div");
		d.style.opacity="1";
		d.style.transition="opacity 1s";
		d.appendChild(document.createTextNode(msg))
		setTimeout(function(d){
			d.style.opacity="0";
		},1000,d);
		d.className="options-msg-sucess";
		p.appendChild(d);
	}
	_backend_tools_javascript_patch(obj=this,root=obj,exclude_from_patch=["bookmarks","settings","images","config","groups"])
	{
		let b,test,e;
		obj._this = root;
		for(b of Object.keys(obj))
		{
			test = false;
			for(e of exclude_from_patch)
			{
				if(e == b)
				{
					test = true;
					break;
				}
			}
			if(!test && typeof(obj[b]) == 'object' && obj[b] != null && b != '_this' )
				this._backend_tools_javascript_patch(obj[b],root);
		}
	}
	async _backend_storage_get()
	{
		let temp  					 			= await browser.storage.local.get();

		this._this.backend.storage.bookmarks 	= temp.bookmarks;
		this._this.backend.storage.images   	= temp.images;
		this._this.backend.storage.settings 	= temp.settings;
		this._this.backend.storage.groups 		= temp.groups;
		
		if(this._this.backend.storage.images == undefined)
			this._this.backend.storage.images = [];
		if(this._this.backend.storage.settings == undefined)
			this._this.backend.storage.settings = {};
		if(this._this.backend.storage.bookmarks == undefined)
			this._this.backend.storage.bookmarks = {};
		if(this._this.backend.storage.groups == undefined)
			this._this.backend.storage.groups = {};
		return true;
	} // vérifié
	async _backend_storage_set()
	{
		await browser.storage.local.set({"settings": this._this.backend.storage.settings});
	} // vérifié
	_backend_tools_compilePref(obj=this._this.backend.storage.settings,forms="")
	{
		let rv = "",a,k,key,ktype;
		for(k of Object.keys(obj))
		{
			key = forms;
			if(forms!="")
				key+="."+k;
			else
				key=k;
			ktype = typeof(obj[k]);
			switch(ktype)
			{
				case "object":
					if(!Array.isArray(k))
					{
						if(forms=="")
							rv+=this._this._backend_tools_compilePref(obj[k],k);
						else
							rv+=this._this._backend_tools_compilePref(obj[k],forms+"."+k);
						break;
					}
					ktype="array";
				case "number":
				case "boolean":
				case "string":
					rv+=key+"="+ktype+"="+obj[k]+";;";
					break;
			}
		}
		return rv;
	}
	/* But: fournir une interface minimale permettant de controller le debug */
/*	_backend_tools_parsePref(str,obj=this._this.backend.storage.settings,first=true)
	{
		if(str==undefined || str=="")
			return true;
		let 
		if(first)
		{
			try{if(this._this.backend.storage.settings.backend.debug.backend.tools.parsePref)
				{
					if(first)
						this._this.tools.group("backend.tools.parsePref");
					this._this.tools.console("str=","d",str);
				}
			}
			catch(e) { }
			let subs = str.split(";;");
			if(subs.length > 1)
			{
				let t=true;
				for(let sub of subs)
				{
					t&=this._this._backend_tools_parsePref(sub,obj,first);
				}
				return t;
			}
		}
		// Séparation de la clé des valeurs
		let kv = str.split("="),k,v,type,arr;
		k=kv.shift();					// clé
		type=kv.shift(); 				// type de la valeur de la clé
		if(k==undefined || type==undefined) return false;
		v=kv.join("=");					// valeur
		arr = k.split("."); 			// split de la clé (création des objets découlants)
		if(first && !Object.keys(this._this.backend.storage.settings).includes(arr[0]))
			arr.unshift("backend","debug");
		let o = arr.shift(),m;
		if( arr.length == 0)
		{
			let jsvalue;
			switch(type)
			{
				case "boolean":
				case "number":
				case "array":
					jsvalue=JSON.parse(v);
					break;
				case "CSS":
					try{
						m=JSON.parse(v);
						if(Array.isArray(m))
							obj[o]=m;
						else
							obj[o].unshift(v);
					} catch(e) {obj[o].unshift(v);}
					return true;
				default:
				case "string":
					jsvalue=String(v);
					break;
			}
			obj[o]=jsvalue;
			return true;
		}

		if(obj[o]==undefined)
			obj[o]={};
		let rv=this._this._backend_tools_parsePref(arr.join(".")+"="+type+"="+v,obj[o],false);
		try{if(this._this.backend.storage.settings.backend.debug.backend.tools.parsePref)
			if(first)
				this._this.tools.groupEnd();
		}
		catch(e) { }
		return rv;
	}*/
	async _backend_communication_envoi(message)
	{ 
		if( typeof(message) == "string" )
			return browser.runtime.sendMessage({statut: message,value: undefined});
		else
			return browser.runtime.sendMessage(message);
	}
	async html2FavOptions(e,root=document.getElementById("favacarte-tab"))
	{
		let str = "",child,name,type,input,line;
		for(child of root.children) 		// trs
		{
			if(child.tagName!="TR") continue; // skip header
			name  = child.children[0].textContent;
			type  = child.children[1].textContent;
			input = child.children[2].children[0].value;

			if(type=="boolean")
				str+=name+"="+type+"="+input+";;";
			else
				if(type=="CSS")
					for(line of JSON.parse(input))
						str+=name+"="+type+"="+line+";;";
				else
					str+=name+"="+type+"="+input+";;";
		}
	//	console.warn("str=",str,",options=",options);
		this._this.backend.tools.parsePref(str);
	}
	async FavOptions2html(obj=this._this.sto.settings,forme="",ip=document.getElementById("favacarte-tab"),first=true,tr_buffer=[])
	{
		if(first)
			this._this.sto.get();
		if(typeof(obj)=="object"&&!Array.isArray(obj))
		{
			for(let k of Object.keys(obj))
			{

				if(forme=="")
					await this._this.FavOptions2html(obj[k],k,ip,false,tr_buffer);
				else
					await this._this.FavOptions2html(obj[k],forme+"."+k,ip,false,tr_buffer);
			}
		}
		else
		{
			let tr 		= document.createElement("tr"),arr;
			tr.className = "favasettings";
			let name 	= tr.appendChild(document.createElement("td"));
			let type 	= tr.appendChild(document.createElement("td"));
			let value   = tr.appendChild(document.createElement("td"));
			name.appendChild(document.createTextNode(forme));
			tr.setAttribute("favacarteoptionskey",forme);
			let typev 	= typeof(obj);
			let input 	= document.createElement("input");
			input.value=JSON.stringify(obj);
			input.style.width="100px";
			input.style.padding="5px";
			input.style.margin="0";
			input.style.border="none";
			switch(typev)
			{
				case "object":
					if(!Array.isArray(obj))
						break;
					arr=forme.match(/\.CSS$/);
					if(arr!=null&&arr.length>0)
						typev="CSS";
					typev="array";
				case "number":
					input.type="text";
					break;
				case "string":
					input.value=obj;
					input.type="text";
					break;
				case "boolean":
					input=document.createElement("div");
					input.appendChild(document.createTextNode((obj?"true":"false")));
					input.value=(obj?"true":"false");
					input.style.textAlign="center";
					input.style.cursor="pointer";
					input.addEventListener("dblclick",function(e){
						let b = JSON.parse(e.target.value);
						b = ! b;
						e.target.value = b.toString();
						e.target.removeChild(e.target.firstChild);
						e.target.appendChild(document.createTextNode((b?"true":"false")));
					});
					break;
			}
			value.appendChild(input);
			type.appendChild(document.createTextNode(typev));
			let qsa=document.querySelectorAll("tr[favacarteoptionskey='"+forme+"']");
			if(!(qsa==undefined||qsa.length==0))
			{
				for(let q of qsa)
				{
					q.children[0].textContent=forme;
					q.children[1].textContent=typev;
					q.children[2].children[0].value=input.value;
				}
				return;
			}
			tr_buffer.push(tr);
		}
		if(first)
		{
			let i,bgc="#ededf0";
			for(let a of tr_buffer)
			{
				if(ip.children.length>0)
				{
					if(ip.children[ip.children.length-1].style.backgroundColor=="rgb(237, 237, 240)")
						bgc="rgb(255,255,255)";
					else
						bgc="rgb(237, 237, 240)";
				}
				a.style.background=bgc;
				ip.appendChild(a);
			}
		
		}
	}
};
let options;
window.addEventListener("load",async function(){
	options = new FavOptions;
	await options.sto.get();
	await options.FavOptions2html();
	document.getElementById("favacarte-controls-submit").addEventListener("click", async function(e){
		await options.html2FavOptions(e);
		await options.sto.set();
		await options.com.envoi({statut: "options_settings",value:"change"});
		options.html.status(browser.i18n.getMessage("options_status_valider"));
	});
	// faire correspondre l'input avec le boutton
	let show_button = document.querySelector("#favacarte-controls-import-display");
	let real_button = document.querySelector("#favacarte-controls-import");
	


	real_button.style.height    = String(show_button.scrollHeight)+"px";
	real_button.style.width     = String(show_button.scrollWidth)+"px";
	real_button.style.overflow  = "hidden";

    var bodyRect = document.body.getBoundingClientRect(),
    elemRect     = show_button.getBoundingClientRect(),
    offsetV      = elemRect.top - bodyRect.top;
    offsetH      = elemRect.left - bodyRect.left;
 
    real_button.style.position = "absolute";
    real_button.style.top  = String(offsetV)+"px";
    real_button.style.left = String(offsetH+40)+"px";   // le 40 px viens de décalages en tout genre dans l'extension 
    real_button.style.opacity="0";
    real_button.style.cursor="pointer";
    real_button.addEventListener("change",function() {
        let conf_file = this.files[0];
        var reader = new FileReader();
        reader.onload = async function(evt) {
            options.backend.tools.parsePref(evt.target.result);
            await options.sto.set();
            await options.com.envoi({statut: "options_settings",value:"change"});
            options.html.status(browser.i18n.getMessage("options_status_imported"));
        };
        reader.readAsText(conf_file);
    });
  
    // cette partie n'est pas fonctionnelle  
//    options.backend.tools.redirect_events(real_button,show_button);
    
	document.getElementById("favacarte-controls-export").addEventListener("click",async function(e){
	//	await options.com.envoi({statut: "options_proxy",value: "sto_write"}); // dire au content d'écrire sa valeur
		await options.sto.get();
		let rv = options.backend.tools.compilePref();
		let data_url = URL.createObjectURL(new Blob([rv]));
		await browser.downloads.download({filename:"config.pref",saveAs:true,url:data_url});
		options.html.status(browser.i18n.getMessage("options_status_exported"));
	});
	document.getElementById("favacarte-controls-reset").addEventListener("click",async function(e){
		await options.com.envoi({statut: "options_settings",value:"reset"});
		await options.FavOptions2html();
		options.html.status(browser.i18n.getMessage("options_status_reset"));
	});

	// Mise en place de la traduction
	document.getElementById("favacarte-controls-reset").textContent=browser.i18n.getMessage("options_reset");
	document.getElementById("favacarte-controls-submit").textContent=browser.i18n.getMessage("options_submit");
	document.getElementById("favacarte-controls-import").textContent=browser.i18n.getMessage("options_import");
	document.getElementById("favacarte-controls-export").textContent=browser.i18n.getMessage("options_export");
	document.getElementById("favacarte-tab-name").textContent=browser.i18n.getMessage("options_head_name");
	document.getElementById("favacarte-tab-type").textContent=browser.i18n.getMessage("options_head_type");
	document.getElementById("favacarte-tab-value").textContent=browser.i18n.getMessage("options_head_value");
});
browser.runtime.onMessage.addListener(async function(m){
	switch(m.statut)
	{
		case "sto_read":
		case "options_settings":
			await options.sto.get();
			await options.FavOptions2html();
			break;
	}
});

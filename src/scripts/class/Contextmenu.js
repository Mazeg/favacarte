/*
    Copyright (C) 2020-present Aïdo WILHELM

    This file is part of favacarte.

    favacarte is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    favacarte is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with favacarte.  If not, see <https://www.gnu.org/licenses/>.
*/
"use strict";
/*
 * fonctions pour gérer le context menu
 */
class Contextmenu {constructor(){
	this.create_texta 							= this.create_texta;
	this.delete 								= this.delete;
	this.dispatchEvent 							= this.dispatchEvent;
	this.is_active 								= this.is_active;
	this.select 								= this.select;
	this.commit 								= this.commit;
	this.show 									= this.show;
}
	is_active()
	{
		let a = [],d;
		for(d of this._this.html.shadowRoot.children)
			if(d.className == "vis-contextmenu")
				a.push(true);
		return !(a==undefined || a.length <= 0);
	}
	select(ctx,dir,forceIndex=false,unhover=false)
	{
		let ctxfield,ni;
		ctxfield 	= ctx.children;
		ni 			= dir+ctx.menuIndex;
		if(forceIndex)
			ni=dir;
		if(ni>=ctxfield.length)
			ni = 0;
		else if(ni < 0)
			ni = ctxfield.length-1;

		if(ctx.menuIndex!=-1) // il faut passer l'ancienne
		{
			ctxfield[ctx.menuIndex].style.background=this._this.sto.settings.frontend.interface.ctx.background;
			try{this._this.html.noeud.visjs_noeud.children[0].focus();} catch(e) {}
		}

		if(unhover)
			return;
		ctx.menuIndex=ni;
		if(ni >= 0 && ni < ctxfield.length)
		{
			ctxfield[ni].style.background=this._this.sto.settings.frontend.interface.ctx.hover.background;
			try{ctxfield[ni].children[0].children[1].children[0].focus();} catch(e) { }
		}
	}
	delete()
	{
		let d;
		for(d of this._this.html.shadowRoot.children)
		{
			if(d.className == "vis-contextmenu")
			{
				this.commit(d);
				d.parentNode.removeChild(d);
			}
		}
		try{this._this.html.noeud.visjs_noeud.children[0].focus();} catch(e) {}	
	}
	dispatchEvent(e)
	{
		let d;
		for(d of this._this.html.shadowRoot.children)
		{
			if(d.className == "vis-contextmenu")
			{
				d.dispatchEvent(e);
			}
		}
	}
	create_texta(title,value,noeud,id)
	{
		let n,nta;
		n = document.createElement("div");
		n.appendChild(document.createElement("div"));
		n.appendChild(document.createElement("div"));
		n.children[1].appendChild(document.createElement("textarea"));
		nta = n.children[1].children[0];
		nta.id=id;
		n.children[0].appendChild(document.createTextNode(title));
		nta.value=value.replace(/<br \/>/g,"\n");
		nta.style.background 	= this._this.sto.settings.frontend.interface.ctx.textfield.background;
		nta.style.color 		= this._this.sto.settings.frontend.interface.ctx.textfield.font.color;
		if(this._this.sto.settings.frontend.interface.ctx.textfield.font.bold)
			nta.style.fontWeight="bold";
		return n;
	}
  	async commit(menu)
	{
		let ids=[],id,group,note,title;
		if(menu.group!=undefined)
			ids.push(menu.group.id);

		for(id of ids)
		{
			group = this._this.sto.groups[id]; // Suffisant
			note = this._this.html.shadowRoot.getElementById("contextmenu-Note");
			if(note!=undefined) group.note = note.value;
			title = this._this.html.shadowRoot.getElementById("contextmenu-Nom");
			if(title!=undefined) group.title= title.value;
		}
		this._this.f.to_groups();
		await this._this.sto.set();
		await this._this.com.envoi("bookmark_change");
		await this._this.frontend.put_to_runtime();
	}
	show(e,booktype=undefined)
	{
		try{if(this._this.backend.storage.settings.backend.debug.html.contextmenu.show) {
			this._this.tools.group("backend.debug.html.contextmenu.show");
			this._this.tools.console("e=","d",e);
			this._this.tools.console("booktype=","d",booktype);}} catch(e) {}

		let a,b,c,d,desc=[],edge,node,ctx,nr,noeud,_this=this._this;
		for(d of this._this.html.shadowRoot.children)
			if(d.className == "vis-contextmenu")
				d.parentNode.removeChild(d);

		node = this._this.frontend.vis.getNodeAt({x:e.clientX,y:e.clientY});
		edge = this._this.frontend.vis.getEdgeAt({x:e.clientX,y:e.clientY});
		ctx  = document.createElement('div');
		if(e.buttons == 0)
		{
			ctx.style.left=0;
			ctx.style.top=0;
			nr = this._this.f.get_nearest_from_position(window.innerWidth/2,window.innerHeight/2,this._this.sto.settings.frontend.controller.deplacement.nearest_threshold.ctxmenu,booktype);//bookmark
			node = (nr==undefined)?undefined:nr.id;
			edge = this._this.frontend.vis.getEdgeAt({x:window.innerWidth/2,y:window.innerHeight/2});
			try{if(this._this.backend.storage.settings.backend.debug.html.contextmenu.show) { this._this.tools.console("contextmenu from keyboard,nr=","d",nr); }} catch(e){}
		}
		else if(e.buttons==2)
		{
			try{if(this._this.backend.storage.settings.backend.debug.html.contextmenu.show) { this._this.tools.console("contextmenu from mouse","d"); }} catch(e){}
			ctx.style.top=e.clientY+"px";
			ctx.style.left=e.clientX+"px";
		}
		noeud = this._this.sto.groups[node];
		ctx.className="vis-contextmenu";
		ctx.style.color=this._this.sto.settings.frontend.interface.ctx.font.color;
		ctx.style.background=this._this.sto.settings.frontend.interface.ctx.background;
		ctx.group=noeud;
		ctx.edge=edge;
		if(this._this.sto.settings.frontend.interface.ctx.font.bold)
			ctx.style.fontWeight = "bold";

		this._this.html.removeHoverEffects();

		if( node == undefined )
		{
			// un context menu dans le vide (ni sur un node ni sur une edge)
			if( edge == undefined )
			{
				desc.push({
					text: browser.i18n.getMessage("ctx_Nouveau_dossier"),
					click:function(e) { 
						if(e.clientX==0&&e.clientY==0&&e.layerX==0&&e.layerY==0) // on considère que le client ne clique jamais à cet endroit
							_this.f.appendNode(undefined,{type:"folder"});
						else
							_this.f.appendNode({x:e.clientX,y:e.clientY},{type:"folder"});
							
					}});

					if(!this._this.sto.settings.temp.frontend.tabNav)
					{
						desc.push({
						text: browser.i18n.getMessage("ctx_Lier_des_favoris"),
						click:function(e) {
							_this.f.vis.addEdgeMode();
							_this.frontend.datasets.edges.on("add",_frontend_handle_addEdge);}});
					}

					desc.push({
					text: browser.i18n.getMessage("ctx_Marquer_cette_page"),
					click: function (e) { 
						if(e.clientX==0&&e.clientY==0&&e.layerX==0&&e.layerY==0) 
							_this.f.appendNode(undefined,{type:"bookmark"});
						else
							_this.f.appendNode({x:e.clientX,y:e.clientY},{type:"bookmark"});
					}}
				);
			}
			else
			{
				desc.push({
					text: browser.i18n.getMessage("ctx_SupprimerEdge"),
					click: async function() { await _this.f.delete(edge,"edge"); }
				});
			}
		}
		else
		{
			let titre, note = this._this.h.contextmenu.create_texta(browser.i18n.getMessage("ctx_Note"),noeud.note,noeud,"contextmenu-Note");
			if(noeud.id != this._this.sto.settings.frontend.interface.images.default.root.id)
			{
				titre = this._this.h.contextmenu.create_texta(browser.i18n.getMessage("ctx_Nom"),noeud.title,noeud,"contextmenu-Nom");
				desc.push({
					text: browser.i18n.getMessage("ctx_SupprimerNode"),
					click: async function() { await _this.f.delete(noeud,"node",{recursive:false}); }},{
					text: browser.i18n.getMessage("ctx_Supprimer_a_la_feuille"),
					click: async function() { await _this.f.delete(noeud,"node",{recursive:true}); }
				});
			}
			desc.push({
			text: browser.i18n.getMessage("ctx_Lier_des_favoris"),
			click:function(e) {
				_this.f.vis.addEdgeMode();
				_this.f.datasets.edges.on("add",_frontend_handle_addEdge);
			}});
			if(noeud.type=="bookmark")
			{
				desc.push({
					text: browser.i18n.getMessage("ctx_Ouvrir"),
					click: function(e){ _this.api.redirection(noeud.url);} },{
					text: browser.i18n.getMessage("ctx_Ouvrir_dans_un_nouvel_onglet"),
					click: function(e) { _this.api.redirection(noeud.url,"newtab");} },{
					text: browser.i18n.getMessage("ctx_Ouvrir_dans_une_nouvelle_fenetre"),
					click: function(e) { _this.api.redirection(noeud.url,"newwindow");} },{
					text: browser.i18n.getMessage("ctx_Ouvrir_dans_une_navigation_privee"),
					click: function(e) { _this.api.redirection(noeud,"newprivate");}
				});
			}
			else if (noeud.type == "folder")
			{
				desc.push({
					text: browser.i18n.getMessage("ctx_Vider"),
					click: async function(e){ await _this.f.delete(noeud.children,"node",{recursive: true}); }},{
					text: browser.i18n.getMessage("ctx_Ouvrir"),
					click: function(e){ _this.api.redirection(noeud,undefined,_this.sto.settings.frontend.controller.openFolderDepth);} },{
					text: browser.i18n.getMessage("ctx_Ouvrir_dans_un_nouvel_onglet"),
					click: function(e) { _this.api.redirection(noeud,"newtab",_this.sto.settings.frontend.controller.openFolderDepth);} },{
					text: browser.i18n.getMessage("ctx_Ouvrir_dans_une_nouvelle_fenetre"),
					click: function(e) { _this.api.redirection(noeud,"newwindow",_this.sto.settings.frontend.controller.openFolderDepth);} },{
					text: browser.i18n.getMessage("ctx_Ouvrir_dans_une_navigation_privee"),
					click: function(e) { _this.api.redirection(noeud,"newprivate",_this.sto.settings.frontend.controller.openFolderDepth);}
				});
			}
			desc.push(note);
			if(titre!=undefined)
				desc.push(titre);
		}
		ctx.menuIndex=-1;
		for(a=0;a<desc.length;a++)
		{
			b = document.createElement('div');
			b.className="vis-contextmenu-field";
			b.menuIndex = a;
			if(typeof(desc[a])=="string")
			{
				b.textContent = desc[a];
			}
			else if(typeof(desc[a])=="object")
			{
				if(desc[a].text!=undefined && desc[a].click!=undefined)
				{
					b.textContent = desc[a].text;
					b.click = desc[a].click;
					b.addEventListener("click",function(e) {
						this.click(e);
						for(let d of _this.html.shadowRoot.children)
							if(d.className == "vis-contextmenu")
								d.parentNode.removeChild(d);
					});
				}
				else
				{
					b.appendChild(desc[a]);
				}
			}
			b.addEventListener("mouseenter",function(e){_this.html.contextmenu.select(ctx,this.menuIndex,true);});
			b.addEventListener("mouseleave",function(e){_this.html.contextmenu.select(ctx,-1,true,true);});
			ctx.appendChild(b);
		}
		ctx.addEventListener("contextmenu",function(e){e.preventDefault();e.stopImmediatePropagation();});
		ctx.addEventListener("keydown",function(e){
			switch(e.key) {
				case _this.sto.settings.frontend.controller.deplacement.escape:
					_this.html.contextmenu.delete();
					_this.html.noeud.visjs_noeud.children[0].focus();
				break;
				case _this.sto.settings.frontend.controller.deplacement.d.h:
					_this.html.contextmenu.select(this,-1);
					e.preventDefault();
					e.stopImmediatePropagation();
				break;
				case _this.sto.settings.frontend.controller.deplacement.d.b:
					_this.html.contextmenu.select(this,+1);
					e.preventDefault();
					e.stopImmediatePropagation();
				break;
				case _this.sto.settings.frontend.controller.deplacement.submit:
					_this.html.contextmenu.delete();
					if(this.children[this.menuIndex]!=undefined)
					{
						let pe = new MouseEvent("click",e);
						this.children[this.menuIndex].dispatchEvent(pe);
					}
				break;
			}
		});
		this._this.html.shadowRoot.appendChild(ctx);
		try{if(this._this.backend.storage.settings.backend.debug.html.contextmenu.show) { this._this.tools.groupEnd(); }} catch(e){}
	}
};
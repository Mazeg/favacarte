/*
    Copyright (C) 2020-present Aïdo WILHELM

    This file is part of favacarte.

    favacarte is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    favacarte is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with favacarte.  If not, see <https://www.gnu.org/licenses/>.
*/
"use strict";
/*
 * visjs |===| Groups |===| Bookmarks |===| BookmarkTreeNode |===| firefox
 *                                                 *
 *
 *
 */
 class BookmarkTreeNode{ constructor() {
 		this.handlers 			= {}; 							// Contient les handlers qui doivent executer quand l'utilisateur ajoute via l'interface firefox
 		this.requests 			= {}; 							// Contient le code a exécuter pour insérer dans le stockage interne
 		this.to_bookmarks 		= this.to_bookmarks;
		this.insert 			= this.insert;
 		this.push 				= this.push;
		// actions de niveau "Bookmarks"
 		this.requests.onChanged = this.requests_onChanged;
 		this.requests.onCreated = this.requests_onCreated;
 		this.requests.onRemoved = this.requests_onRemoved;
 		this.requests.onMoved   = this.requests_onMoved;
 		// actions de niveau "Groups"
		this.handlers.onCreated = this.handlers_onCreated;
		this.handlers.onChanged = this.handlers_onChanged;
		this.handlers.onRemoved = this.handlers_onRemoved;
		this.handlers.onMoved 	= this.handlers_onMoved; 		
 		// vérifié
 		this.init_build 		= this.init_build;
		this.pull 				= this.pull;
		this._regression_helper = this._regression_helper;
		this.regression 		= _backend_storage_BookmarkTreeNode_regression;
	}
	/* 
	 * But: formater la source BookmarkTreeNode en Bookmarks et la retourner
	 * BookmarkTreeNode -> Bookmarks
	 */
	to_bookmarks (noeud,depth=0,recursive=false)
	{
		let anoeud 								= {},result,i;
		anoeud.id 								= noeud.id;
		anoeud.children							= [];
		anoeud.parent 							= noeud.parentId;
		anoeud.url								= noeud.url;
		anoeud.image 							= undefined; 
		anoeud.title 							= noeud.title;
		anoeud.date 							= noeud.dateAdded;
		anoeud.type 							= noeud.type;
		anoeud.coord 							= {
			x: 	0,
			y: 	0
		};
		anoeud.estCache 						= noeud.estCache==undefined ? ((depth<=this._this.backend.storage.settings.frontend.interface.max_depth)?false:true) : noeud.estCache;
		anoeud.depth 							= depth;
		anoeud.note 							= "";
		delete noeud.dateGroupModified;
		delete noeud.dateAdded;
		delete noeud.index;
		delete noeud.unmodifiable;
		delete noeud.parentId;
		delete noeud.oldIndex;
		delete noeud.oldParentId;
		if(recursive)
		{
			result = {};
			if(noeud.children != null)
			{
				for(i=0;i<noeud.children.length;i++)
				{
					anoeud.children.push(noeud.children[i].id);
					Object.assign(result,this.to_bookmarks(noeud.children[i],depth+1,recursive));
				}
			}
			result[anoeud.id]=anoeud;
			return result;
		}
		return anoeud;
	}
	/*
	 * Cette fonction est appelée dans le cas d'un pull, qui n'est jamais déclenché par le client
	 * état du system:
	 * 	BookmarkTreeNode <--- 		 *
	 *  Bookmarks                    *
	 *  Groups                       *
     *  Frontend                     +
     *
     *
     * * : partie exécutable dans le démon
     * + : partie exécutable dans le client uniquement
     *
     * rq: la propagation à été codée dans l'autre sens F -> G -> B -> B
     *     et dans l'API, le courant circule mieux comme ça, donc pour passer dans l'autre sens il faut faire des modifications à des fonctions (les rendre bidirection)
	 */
	handlers_onCreated(value,t,specialCall=undefined)
	{
		if(specialCall==undefined && this._this.sto.bookmarks[value.parentId]==undefined && (Object.keys(this._this.sto.bookmarks)).length > 0)
			return false;

		let n,g,idx,u,parents,politique_affichage_ui=undefined;
		try { if(this._this.sto.settings.backend.storage.BookmarkTreeNode.handlers.onCreated); 
		switch(this._this.sto.settings.backend.storage.BookmarkTreeNode.handlers.onCreated)
		{
			case "depth":
				politique_affichage_ui=undefined;
			break;
			case false:
				politique_affichage_ui=false;
			break;
		}} catch(e){}


		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.BookmarkTreeNode.handlers.onCreated) {
			this._this.tools.group("backend.debug.backend.storage.BookmarkTreeNode.handlers.onCreated");
			this._this.tools.console("value=","d",value);
			this._this.tools.console("t=","d",t);
			this._this.tools.groupEnd(); }} catch(e) {}

		if(specialCall==undefined)
		{
			// image actuelle
			u = new URL(t.url);
			if(t.favIconUrl!=undefined)
			{
				idx = this._this.sto.images.findIndex(image => image.url == u.host);
				if(idx==-1 && u.host!=undefined&&u.host!=""&&t.faviconUrl!=undefined&&t.faviconUrl!="")
					this._this.sto.images.push({url: u.host,image: t.favIconUrl});
				
			}
		}
		parents = this._this.sto.Groups.get_groups_of(value.parentId);
		g=this._this.sto.Groups.new(
			{
				id:value.id,parents:parents.slice(),
				title:value.title,
				type:value.type,
				url:value.url,
				date:value.dateAdded
			},
			[value.id],
			true,
			undefined,
			[value.parentId],
			politique_affichage_ui);
		return true;
	}
	/*
	 * Fonction exécutée quand je recoit un event de l'API firefox que je n'ai pas induit.//
	 */
	async handlers_onMoved(bookMarkTreeNode,t)
	{
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.BookmarkTreeNode.handlers.onMoved) {
			this._this.tools.group("backend.storage.BookmarkTreeNode.handlers.onMoved");
			this._this.tools.console("bookMarkTreeNode=","d",bookMarkTreeNode);
			this._this.tools.console("t=","d",t);
			this._this.tools.groupEnd(); }} catch(e) {}
		let gfrom 	= this._this.sto.Groups.get_groups_of(bookMarkTreeNode.id);
		let at 		= this._this.sto.groups[this._this.sto.Groups.get_groups_of(bookMarkTreeNode.parentId)[0]];
		let cls 	= this._this.sto.Bookmarks.process_closure(bookMarkTreeNode.id,this._this.sto.bookmarks,"Array",0);
		let booksFirefox,conv_booksFirefox,clsFirefoxBookmarks,a,b,gf;
		// cas IN -> OUT
		if( at == undefined )
		{
			await this._this.sto.BookmarkTreeNode.handlers.onRemoved(bookMarkTreeNode,t);
		}
		else
		{
			// cas OUT -> IN
			if(gfrom.length == 0)
			{
				// ici on doit les faire un par un //
				booksFirefox = await browser.bookmarks.getSubTree(bookMarkTreeNode.id);
				booksFirefox = booksFirefox.first();
				conv_booksFirefox = this._this.sto.BookmarkTreeNode.to_bookmarks(booksFirefox,undefined,true);
				clsFirefoxBookmarks = this._this.sto.Bookmarks.process_closure(bookMarkTreeNode.id,conv_booksFirefox,"Array",0);
				for(a=0;a<clsFirefoxBookmarks.length;a++)
				{
					b=conv_booksFirefox[clsFirefoxBookmarks[a]];
					await this._this.sto.BookmarkTreeNode.handlers.onCreated({dateAdded:b.date,id:b.id,parentId:b.parent,title:b.title,type:b.type,url:b.url},{},true);
				}
			}
			else
			{
				for(gf of gfrom)
				{
					gf 			= this._this.sto.groups[gf];
					await this._this.sto.Groups.move(gf,at,cls,bookMarkTreeNode.parentId);
				}
			}
		}
		return true;
	} // vérifié
	/*
	 * Les suppressions dans l'interface sont toujours récursive
	 */
	async handlers_onRemoved(BookmarkTreeNodeID,t)
	{
		// seule possib: le fav est en dehors de l'arbre
		if(this._this.sto.bookmarks[BookmarkTreeNodeID.id]==undefined) return false;

		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.BookmarkTreeNode.handlers.onRemoved) {
			this._this.tools.group("backend.storage.BookmarkTreeNode.handlers.onRemoved");
			this._this.tools.console("BookmarkTreeNodeID=","d",BookmarkTreeNodeID);
			this._this.tools.console("t=","d",t);
			this._this.tools.groupEnd(); }} catch(e) {}
		if(typeof(BookmarkTreeNodeID)=="object") BookmarkTreeNodeID=BookmarkTreeNodeID.id;
		let groups = this._this.sto.Groups.get_groups_of(BookmarkTreeNodeID),g,cls,ms;
		for(g of groups)
		{
			cls = this._this.sto.Bookmarks.process_closure(BookmarkTreeNodeID,this._this.sto.bookmarks,"Array",0);
			ms = this._this.sto.groups[g].members.slice();
			await this._this.sto.Groups.delete(g,true,{except_ids: cls,default: true});
		}
		return true;
	}
	/*
	 * L'utilisateur a exécuté un changement depuis l'interface firefox
	 */
	async handlers_onChanged(bookMarkTreeNode,t)
	{
		// seule possib: le fav est en dehors de l'arbre
		if(this._this.sto.bookmarks[bookMarkTreeNode.id]==undefined) return false;
		
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.BookmarkTreeNode.handlers.onChanged) {
			this._this.tools.group("backend.storage.BookmarkTreeNode.handlers.onChanged");
			this._this.tools.console("bookMarkTreeNode=","d",bookMarkTreeNode);
			this._this.tools.groupEnd(); }} catch(e) {}		


		bookMarkTreeNode=(await browser.bookmarks.getSubTree(bookMarkTreeNode.id))[0];//récupérer les changements qui ont été faits

		let grps = this._this.sto.Groups.get_groups_of(bookMarkTreeNode.id);


		this._this.sto.Groups.update_at(grps,bookMarkTreeNode,{default: true,except_ids: [bookMarkTreeNode.id]});

		return true;
	}
	async requests_onMoved(k,value={})
	{
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.BookmarkTreeNode.requests.onMoved) {
			this._this.tools.group("backend.storage.BookmarkTreeNode.requests.onMoved");
			this._this.tools.console("value=","d",value);
			this._this.tools.console("k=","d",k);
			this._this.tools.groupEnd(); }} catch(e) {}
		let btn;
		try {
			btn=await browser.bookmarks.move(k,{parentId:value.parentId});
		} catch(e) {

		}

		return this._this.sto.bookmarks[btn.id];
	}
	async requests_onCreated(k,value={})
	{
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.BookmarkTreeNode.requests.onCreated) {
			this._this.tools.group("backend.storage.BookmarkTreeNode.requests.onCreated");
			this._this.tools.console("k=","d",k);
			this._this.tools.console("value=","d",value);
			this._this.tools.groupEnd(); }} catch(e) {}		
		let btn;
		try { btn = await browser.bookmarks.create(value); }
		catch(e) {return false;}
		this._this.backend.storage.Bookmarks.correct(k,btn.id);
		return this._this.sto.bookmarks[btn.id];
	}
  	async requests_onRemoved(k,value={})
 	{
 		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.BookmarkTreeNode.requests.onRemoved) {
			this._this.tools.group("backend.storage.BookmarkTreeNode.requests.onRemoved");
			this._this.tools.console("k=","d",k);
			this._this.tools.console("value=","d",value);
			this._this.tools.groupEnd(); }} catch(e) {}		


 		try { await browser.bookmarks.remove(k); } catch(e) { return false; }
 		delete this._this.sto.bookmarks[k];
 		return true;
 	}
 	async requests_onChanged(k,value={})
 	{
 		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.BookmarkTreeNode.requests.onChanged) {
			this._this.tools.group("backend.storage.BookmarkTreeNode.requests.onChanged");
			this._this.tools.console("k=","d",k);
			this._this.tools.console("value=","d",value);
			this._this.tools.groupEnd(); }} catch(e) {}		
		try { await browser.bookmarks.update(k,{title:value.title,url:value.url}); } catch(e) { return false; }
		return true;
 	}
	// cette fonction ne devrait être appelée que au premier lancement de l'extension
	// pour les fois suivantes, il faudrait plutot regarder si de nouveaux favoris sont appraus dans l'arborescence.
	async init_build()
	{
		let visible_root = await browser.bookmarks.getSubTree(FAV_ROOT);
		visible_root=visible_root[0];
		let manufactured = this._this.sto.BookmarkTreeNode.to_bookmarks(visible_root,0,true),k;
		this._this.backend.storage.bookmarks=manufactured;
		this._this.sto.groups = {};
		this._this.sto.Bookmarks.to_groups();
		for(k of Object.keys(this._this.sto.settings.frontend.interface.images.default))
			this._this.sto.settings.frontend.interface.images.default[k].image = await browser.runtime.getURL(this._this.sto.settings.frontend.interface.images.default[k].image);

		await this._this.sto.set();
		this._this.sto.Bookmarks.sync_icons();
		await this._this.b.sync_usettings();
	}
	/*
	 * But: insérer une structure bookMarkTreeNode dans le stockage interne
	 * cette fonction est toujours appelée dans le cas d'un bookmark qui n'était pas dans l'arbre
	 * ici la prop .children : ce sont des ids
	 */
	insert(bookMarkTreeNode)
	{
		let childs = bookMarkTreeNode.children,c;
		let pid = bookMarkTreeNode.parentId;
		let converted_node = this._this.sto.BookmarkTreeNode.to_bookmarks(bookMarkTreeNode);
		this._this.sto.Bookmarks.insert_at(pid,converted_node);
		if(converted_node.depth > this._this.storage.settings.frontend.interface.max_depth)
			converted_node.estCache = true;  	// c'est un nouveau noeud donc on le met caché
		
		if(childs != undefined)
			for(c of childs)
			{
				c.parentId=bookMarkTreeNode.id;
				this.insert(c);
			}
		
		return converted_node;
	} // vérifié

	// bookmarks ---> Bookmarks
	/* But: signaler à l'API bookmark de firefox les changements sur les favoris */
	// Ceci implique que le client a déjà ajouté dans l'interface graphique
	async push()
	{	
		/*
		 * Ici le bug est due à un flag qui est set sur le bookmark en modification alors qu'il ne devrait pas
		 *	Groups.update_at 		-> Bookarks.update_at
		 *	Groups.to_bookmarks     ->
		 * essayons la fonction B.update_at
		 */
		//console.warn("ok");
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.BookmarkTreeNode.push) {
			this._this.tools.group("backend.storage.BookmarkTreeNode.push");
			this._this.tools.console("this._this.sto.cbc_accu=","d",this._this.sto.cbc_accu);
			this._this.tools.group("remplissage de accu");}} catch(e) {}

		let accu = {},newb,execfile=[],k,b,res_parcours,s,kk;
		for(k of Object.keys(this._this.sto.bookmarks))
		{

			b = this._this.sto.bookmarks[k];
			if(b.state==undefined)
				continue;

			try{if(this._this.backend.storage.settings.backend.debug.backend.storage.BookmarkTreeNode.push) {
				this._this.tools.console("b.state=","d",b.state);
				this._this.tools.console("b=","d",b);
			}} catch(e) {}
			if(b.children==undefined) b.children=[];
			accu[b.id]={id: k,parent: b.parent,state: b.state, value: {parentId:b.parent,url:b.url,title:b.title,type:b.type}, children: b.children.slice()};
		}
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.BookmarkTreeNode.push) {
			this._this.tools.groupEnd();
		}} catch(e) {}
		// Solution 1 : noter les racines de l'accu
		// 				cad toutes les entrées qui ont accu[b.parent]==undefined
		//              ce sont mes racines, et c'est de la que je commence les modifications
		for(k of Object.keys(accu))
		{
			if((accu[k].value.parentId==undefined||accu[accu[k].value.parentId]==undefined||accu[accu[k].value.parentId].state!=accu[k].state)&&accu[k].state)
			{
				res_parcours = this._this.sto.Bookmarks.process_parcours_prefixe(k,accu);
				if(accu[k].state=="Removed")
					execfile=execfile.concat(res_parcours.reverse());
				else
					execfile=execfile.concat(res_parcours);
			}
		}
		
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.BookmarkTreeNode.push) {
			this._this.tools.console("accu=","d",accu);
			this._this.tools.console("execfile=","d",execfile);}} catch(e) {}

		while(execfile.length>0)
		{
			s = execfile.shift();
			if(typeof(s)=="string") s=accu[s];

			if(s==undefined) continue;

			this._this.backend.storage.cbc_accu.push(s.state);
			try{if(this._this.backend.storage.settings.backend.debug.backend.storage.BookmarkTreeNode.push){this._this.tools.console("s=","d",s);}} catch(e) {}


			delete this._this.sto.bookmarks[s.id].state;

			if(this._this.sto.bookmarks[s.id].state=="Removed")
			{
				accu[kk].children				
			}
			else
			{
				newb=await this.requests["on"+s.state](s.id,s.value);
				if(newb!=undefined)
				{
					for(kk of Object.keys(accu))
					{
						if(accu[kk].value.parentId==s.id)
						{
							accu[kk].value.parentId=newb.id;
						}
					}
				}
			}
			delete accu[s.id];
		}

		
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.BookmarkTreeNode.push) {
			this._this.tools.console("this._this.sto.cbc_accu=","d",this._this.sto.cbc_accu);
			this._this.tools.groupEnd();
		}} catch(e) {}
	}
	/*
	 * But: Récupérer toutes les modifications en provenance de l'API firefox et les traiter
	 *      L'utilisateur ajoute un favoris via l'interface firefox, on se charge de traiter les conséquences ici
	 */
	async pull(parameters)
	{
		if(this._this.sto.event_accu.length == 0)
		{
			let t = (await browser.tabs.query({currentWindow: true,active: true}))[0];
			Promise.resolve({_this:this._this,t:t}).then(function(o) {
	  			setTimeout(async function(this_scope,t) {
					try{if(this_scope.backend.storage.settings.backend.debug.backend.storage.BookmarkTreeNode.pull) {
						this_scope.tools.group("backend.storage.BookmarkTreeNode.pull");
						this_scope.tools.console("t","d",t);
					}} catch(e) {}
				  	let obj;
					let test_affect = false;

					while(this_scope.sto.event_accu.length>0)
					{
						obj = this_scope.sto.event_accu.shift();
						test_affect|=(await this_scope.sto.BookmarkTreeNode.handlers["on"+obj.type](obj.value,t));

						try{if(this_scope.backend.storage.settings.backend.debug.backend.storage.BookmarkTreeNode.pull) {
							this_scope.console("Event recu de l'API firefox : "+String(obj.type),"d");
							this_scope.console("obj","d",obj);
							}} catch(e) {}
					}
					if(!test_affect) return ;
					// Ordonne au client d'appliquer les changements
					await this_scope.sto.BookmarkTreeNode.push();
					await this_scope.sto.set();
				  	await this_scope.api.sendMessage(t.id,{statut: "sto_read_exceptUI"});
				  	try{if(this_scope.backend.storage.settings.backend.debug.backend.storage.BookmarkTreeNode.pull) {
						this_scope.tools.groupEnd();
					}} catch(e) {}
		  		}, 300,o._this,o.t);  // On attend 300 ms avant de commencer à vider l'accumulateur
			});						 // TODO : permettre de configurer ce Timeout
		}
		this._this.sto.event_accu.push(parameters);
	} // vérifié
	
	_regression_helper()
	{
		this._this.tools.parsePref("backend.debug.backend.storage.Groups.delete=boolean=true;;");
		this._this.tools.parsePref("backend.debug.backend.storage.BookmarkTreeNode.push=boolean=true;;");
		this._this.tools.parsePref("backend.debug.backend.storage.BookmarkTreeNode.pull=boolean=true;;");
		this._this.tools.parsePref("backend.debug.browser.bookmarks.onChanged=boolean=true;;");
		this._this.tools.parsePref("backend.debug.browser.bookmarks.onRemoved=boolean=true;;");
		this._this.tools.parsePref("backend.debug.browser.bookmarks.onCreated=boolean=true;;");
		this._this.tools.parsePref("backend.debug.backend.storage.Groups.to_bookmarks=boolean=true;;");
		this._this.tools.parsePref("backend.debug.backend.storage.BookmarkTreeNode.requests.onChanged=boolean=true;;");	
		this._this.tools.parsePref("backend.debug.backend.storage.BookmarkTreeNode.requests.onCreated=boolean=true;;");	
		this._this.tools.parsePref("backend.debug.backend.storage.BookmarkTreeNode.requests.onRemoved=boolean=true;;");	
		this._this.tools.parsePref("backend.debug.backend.storage.BookmarkTreeNode.handlers.onChanged=boolean=true;;");	
		this._this.tools.parsePref("backend.debug.backend.storage.BookmarkTreeNode.handlers.onCreated=boolean=true;;");	
		this._this.tools.parsePref("backend.debug.backend.storage.BookmarkTreeNode.handlers.onRemoved=boolean=true;;");	
		this._this.tools.parsePref("backend.debug.backend.tools.groupCollapsed=boolean=false;;");
	}
};
async function _backend_storage_BookmarkTreeNode_regression(controle=0xFFFF,more_tests=false,debug=false,verb=false)
{
	if(debug) this._this.sto.BookmarkTreeNode._regression_helper();
	let waithandlers=200,gsz,a,b,c,d,e,f,g,h,i,ii,iii,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,test,g1,g2,g3,g4,h1,h2,h3,h4,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,tests=[];
	this._this.tools.group("BookmarkTreeNode");
		if(controle&0x1){test=true;
		a=(await browser.bookmarks.getSubTree(FAV_ROOT))[0];
		test&=(a.id=="toolbar_____");
		b= this._this.sto.BookmarkTreeNode.to_bookmarks(a);
		test&=(b.title==a.title)&&(b.url==a.url)&&(b.type==a.type);
		this._this.tools.console("to_bookmarks","a",test);}

		if(controle&0x2){test=true;
		s=Object.keys(this._this.sto.bookmarks).length;
		r= this._this.sto.BookmarkTreeNode.insert({parentId: "toolbar_____",title: "ok",url: "http://tamere.fr/"});
		test&=(r.title=="ok"&&r.url=="http://tamere.fr/");
		test&=(s+1==Object.keys(this._this.sto.bookmarks).length);
		A={id: this._this.tools.gen_bid(),parentId: "toolbar_____",title: "A11",children:
		[{
			title: "B11",
			id: this._this.tools.gen_bid()
		}]};
		s=Object.keys(this._this.sto.bookmarks).length;
		 this._this.sto.BookmarkTreeNode.insert(A);
		test&=(s+2==Object.keys(this._this.sto.bookmarks).length);
		this._this.tools.console("insert","a",test);}

		if(controle&0x4){test=true;
		await this._this.api.reset();		
		await this._this.api.reset(true); // Attention, ces reset sont indispensables à la bonne marche
		//	this._this.tools.parsePref("backend.debug.backend.storage.BookmarkTreeNode.push=boolean=true;;");
		s=(await browser.bookmarks.getSubTree("toolbar_____"))[0].children.length;
		A = this._this.sto.Bookmarks.new({parent: "toolbar_____",title: "OKOK"});
		await this._this.sto.BookmarkTreeNode.push();
		await this._this.tools.sleep(1000);
		c=(await browser.bookmarks.getSubTree("toolbar_____"))[0].children.length;
		test&=(s+1)==c;
		// en plus ici on va tester dans le d'insertion multiple, récursive
		s=(await browser.bookmarks.getSubTree("toolbar_____"))[0].children.length;
		A = this._this.sto.Bookmarks.new({parent: "toolbar_____",title: "A"});
		B = this._this.sto.Bookmarks.new({parent: A.id,title: "B"});
		C = this._this.sto.Bookmarks.new({parent: B.id,title: "C"});
		D = this._this.sto.Bookmarks.new({parent: B.id,title: "D"});
		await this._this.sto.BookmarkTreeNode.push();
		await this._this.tools.sleep(1000);
		c=(await browser.bookmarks.getSubTree("toolbar_____"))[0].children.length;
		test&=(s+1)==c;
		//this._this.tools.parsePref("backend.debug.backend.storage.BookmarkTreeNode.push=boolean=false;;");
		this._this.tools.console("push","a",test);}

		// vérification de la fonction pull : on fait un event sur l'API firefox (browser.bookmarks)
		// et on observe bien les bonnes modification (si ça marche pour tous les handlers, alors on valide la fonction)

		/* Méthodes pour tester les handlers:
		 * 	* provoquer un event sur l'API firefox (avec browser.bookmarks.tamere)
		 *  * attendre 1 seconde (execution du handler)
		 *  * vérifier le bon fonctionnement du handlers
		 */
		if(controle&(0x8+0x10+0x20+0x200)) this._this.tools.group("handlers");
			if(controle&0x8){test=true;
			//
			//	Handler validé dans les situations:
			//	- ajout en héritage multiple
			//	- ajout en héritage simple
			//  - importation d'une grosse liste de favoris via l'interface firefox
			//
			await this._this.api.reset();
			await this._this.api.reset(true); // Attention, ces reset sont indispensables à la bonne marche
/*				this._this.tools.parsePref("backend.debug.backend.storage.Bookmarks.new=boolean=true;;");
			this._this.tools.parsePref("backend.debug.backend.storage.Groups.new=boolean=true;;");
			this._this.tools.parsePref("backend.debug.backend.storage.BookmarkTreeNode.push=boolean=true;;");
			this._this.tools.parsePref("backend.debug.backend.storage.BookmarkTreeNode.pull=boolean=true;;");
			this._this.tools.parsePref("backend.debug.backend.storage.BookmarkTreeNode.handlers.onCreated=boolean=true;;");
			this._this.tools.parsePref("backend.debug.backend.storage.BookmarkTreeNode.requests.onCreated=boolean=true;;");
*/				//     A
			//    / \
			//   B   C
			//    \ /
			//     D
			//     |
			//     E
			//     +
			//     F
			A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
			B = this._this.sto.Groups.new({parents: [A.id],title: "B"});
			C = this._this.sto.Groups.new({parents: [A.id],title: "C"});
			D = this._this.sto.Groups.new({parents: [B.id,C.id],title: "D"});
			E = this._this.sto.Groups.new({parents: [D.id],title: "E"});
			await this._this.sto.BookmarkTreeNode.push();
			await this._this.tools.sleep(1000);
			s=(Object.keys(this._this.sto.bookmarks)).length;
			u=(Object.keys(this._this.sto.groups)).length;
			f=await browser.bookmarks.create({title: "F",parentId: E.members[0]});
			await this._this.tools.sleep(1000);
			F=this._this.sto.groups[this._this.sto.Groups.get_groups_of(f.id)[0]];
			test&=(s+2==(Object.keys(this._this.sto.bookmarks)).length);
			test&=(u+1==(Object.keys(this._this.sto.groups)).length);
			// vérification des groups
			test&=(A.members.length==1)&&(A.children.length==2)&&(A.parents.length==1);
			test&=(B.members.length==1)&&(B.children.length==1)&&(B.parents.length==1);
			test&=(C.members.length==1)&&(C.children.length==1)&&(C.parents.length==1);
			test&=(D.members.length==2)&&(D.children.length==1)&&(D.parents.length==2);
			test&=(E.members.length==2)&&(E.children.length==1)&&(E.parents.length==1);
			test&=(F.members.length==2)&&(F.children.length==0)&&(F.parents.length==1);
			if(debug) this._this.sto.BookmarkTreeNode._regression_helper();
			s=(Object.keys(this._this.sto.bookmarks)).length;
			u=(Object.keys(this._this.sto.groups)).length;
			//			A
			//          |
			//          B
			///
			A=await browser.bookmarks.create({title: "A",parentId: "toolbar_____"});
			B=await browser.bookmarks.create({title: "B",parentId: A.id});
			await this._this.tools.sleep(1000);
			t=(Object.keys(this._this.sto.bookmarks)).length;
			v=(Object.keys(this._this.sto.groups)).length;
			test&=(s+2==t);
			test&=(u+2==v);
			test&=(this._this.sto.cbc_accu.length==0);
			// Essai d'implémentation pour l'héritage multiple
			// 		g1	g2
			//      \ /
			//        g3
			//        +
			//        D
			//
			g1 = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "g1"});
			g2 = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "g2"});
			g3 = this._this.sto.Groups.new({parents: [g1.id,g2.id],title: "g3"});
			await this._this.sto.BookmarkTreeNode.push();
			await this._this.tools.sleep(1000);
			s=(Object.keys(this._this.sto.bookmarks)).length;
			u=(Object.keys(this._this.sto.groups)).length;
			D=await browser.bookmarks.create({title: "D",parentId: g3.members[0]});
			await this._this.tools.sleep(1000);
			test&=(this._this.sto.cbc_accu.length==0);
			t=(Object.keys(this._this.sto.bookmarks)).length;
			v=(Object.keys(this._this.sto.groups)).length;
			test&=(s+2==t);
			test&=(u+1==v);
			// 		 	A
			// 		   / \  
			//        B   C 
			//        |  / 
			//        D-'  
			//        '.....F
			// 
			A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "1"});
            B = this._this.sto.Groups.new({parents: [A.id],title: "1.1"});
            C = this._this.sto.Groups.new({parents: [A.id],title: "1.2"});
            D = this._this.sto.Groups.new({parents: [B.id,C.id],title: "1.1.1"});
            F = this._this.sto.Groups.new({parents: [D.id],title: "1.1.1.1"});
      		await this._this.sto.BookmarkTreeNode.push();
			await this._this.tools.sleep(3000);
			test&=(A.children.length==2)&&(A.members.length==1)&&(A.parents.length==1);
			for(m of A.members)
			{
				b=this._this.sto.bookmarks[m];
				test&=(b.children.length==2)&&(b.parent!=undefined);
			}
			test&=(B.children.length==1)&&(B.members.length==1)&&(B.parents.length==1);
			for(m of B.members)
			{
				b=this._this.sto.bookmarks[m];
				test&=(b.children.length==1)&&(b.parent!=undefined);
			}
			test&=(C.children.length==1)&&(C.members.length==1)&&(C.parents.length==1);
			for(m of C.members)
			{
				b=this._this.sto.bookmarks[m];
				test&=(b.children.length==1)&&(b.parent!=undefined);
			}
			test&=(D.children.length==1)&&(D.members.length==2)&&(D.parents.length==2);
			for(m of D.members)
			{
				b=this._this.sto.bookmarks[m];
				test&=(b.children.length==1)&&(b.parent!=undefined);
			}
			test&=(F.children.length==0)&&(F.members.length==2)&&(F.parents.length==1);
			for(m of F.members)
			{
				b=this._this.sto.bookmarks[m];
				test&=(b.children.length==0)&&(b.parent!=undefined);
			}
			// 		 	A
			// 		   / \  
			//        B   C 
			//        |  / 
			//        D-'  
			//        '.....F
			//
			A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
            B = this._this.sto.Groups.new({parents: [A.id],title: "B"});
            C = this._this.sto.Groups.new({parents: [A.id],title: "C"});
            D = this._this.sto.Groups.new({parents: [B.id,C.id],title: "D"});
            F = this._this.sto.Groups.new({parents: [D.id],title: "F"});
      		await this._this.sto.BookmarkTreeNode.push();
			await this._this.tools.sleep(1000);
			P=await browser.bookmarks.create({title: "P",parentId:F.members.last()});
			//console.warn("P=",P);
			await this._this.tools.sleep(1000);
			P=this._this.sto.groups[this._this.sto.Groups.get_groups_of(P.id)[0]];
			//console.warn("P=",P);
			test&=(this._this.sto.bookmarks[P.members.first()].parent!=this._this.sto.bookmarks[P.members.last()]);
			this._this.tools.console("onCreated","a",test);}

			if(controle&0x10){test=true;
			//
			// Handler validé dans les situations:
			// - héritage simple
			// BUGS:
			// 		héritage multiple: modification via interface firefox de l'un ne donne pas une modification dans l'autre
			A=await browser.bookmarks.create({title: "A",parentId: "toolbar_____"});
			await this._this.tools.sleep(1000);
			i = this._this.sto.bookmarks["toolbar_____"].children[0];
			await browser.bookmarks.update(i,{title: "TEST OK"});
			await this._this.sto.BookmarkTreeNode.push();
			await this._this.tools.sleep(1000);
			c=this._this.sto.bookmarks[i];
			test&=(c.title=="TEST OK");
			g1 = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "g1"});
			g2 = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "g2"});
			g3 = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "g3"});
			A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
			B = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "B"});
			g4 = this._this.sto.Groups.new({parents: [g1.id,g2.id,g3.id,A.id,B.id],title: "g4"});
			await this._this.sto.BookmarkTreeNode.push();
			await this._this.sto.set();
			await this._this.tools.sleep(1000);
			for(j=0;j<g3.members.length;j++)
				for(t of ["endpoint","AAAA"])
				{
					await browser.bookmarks.update(g4.members[j],{title: t});
					await this._this.tools.sleep(2000);				
					for(a of g4.members)
						test&=(this._this.sto.bookmarks[a].title==t);
				}
			if(more_tests){
			g1 = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "g1"});
			g2 = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "g2"});
			g3 = this._this.sto.Groups.new({parents: [g1.id,g2.id],title: "g3"});
			await this._this.sto.BookmarkTreeNode.push();
			await this._this.sto.set();
			await this._this.tools.sleep(1000);
			for(j=0;j<g3.members.length;j++)
				for(t of ["endpoint","AAAA","ZZZZ","OOOO","mlplplpl","AKOZAOEKZAO","kkk","bonjour","au revoir","ok google","yes","no","endline","end","changed","<ANONYME>"])
				{
					await browser.bookmarks.update(g3.members[j],{title: t});
					await this._this.tools.sleep(2000);				
					for(a of g3.members)
						test&=(this._this.sto.bookmarks[a].title==t);
				}
			g1 = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "g1"});
			g2 = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "g2"});
			g3 = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "g3"});
			g4 = this._this.sto.Groups.new({parents: [g1.id,g2.id,g3.id],title: "g4"});
			await this._this.sto.BookmarkTreeNode.push();
			await this._this.sto.set();
			await this._this.tools.sleep(1000);
			for(j=0;j<g4.members.length;j++)
			{

				for(t of ["endpoint","AAAA","ZZZZ","OOOO","mlplplpl","AKOZAOEKZAO","kkk","bonjour","au revoir","ok google","yes","no","endline","end","changed","<ANONYME>"])
				{
					await browser.bookmarks.update(g4.members[j],{title: t});
					await this._this.tools.sleep(2000);				
					for(a of g4.members)
						test&=(this._this.sto.bookmarks[a].title==t);
				}
			}}
			this._this.tools.console("onChanged","a",test);}

			if(controle&0x20){test=true;
			// 		 	A
			// 		   / \  
			//        B   C 
			//        |  / 
			//        D-'  
			//        '.....F
			// 
			A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "1"});
            B = this._this.sto.Groups.new({parents: [A.id],title: "1.1"});
            C = this._this.sto.Groups.new({parents: [A.id],title: "1.2"});
            D = this._this.sto.Groups.new({parents: [B.id,C.id],title: "1.1.1"});
            F = this._this.sto.Groups.new({parents: [D.id],title: "1.1.1.1"});
      		await this._this.sto.BookmarkTreeNode.push();
			await this._this.tools.sleep(3000);
			s=(Object.keys(this._this.sto.bookmarks)).length;
			t=(Object.keys(this._this.sto.groups)).length;
			await browser.bookmarks.removeTree(D.members[0]);
			await this._this.tools.sleep(1000);
			test&=(s-4==(Object.keys(this._this.sto.bookmarks)).length);
			test&=(t-2==(Object.keys(this._this.sto.groups)).length);
			//
			// Handler validé dans les situations:
			// - héritage simple
			// > TODO (une fois que la dépendance est résolue): test en héritage multiple
			//
			g1 = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "g1"});
			await this._this.sto.BookmarkTreeNode.push();
			await this._this.tools.sleep(1000);
			s=(Object.keys(this._this.sto.groups)).length;
			await browser.bookmarks.remove(g1.members[0]); 
			await this._this.tools.sleep(1000);
			t=(Object.keys(this._this.sto.groups)).length; // la taille des bookmarks restant la même jusqu'à appel du push
			test&=(s-1==t);
			g1 = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "<TEST>g1"});
			g2 = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "<TEST>g2"});
			g3 = this._this.sto.Groups.new({parents: [g1.id,g2.id],title: "<TEST>g3"});
			await this._this.sto.BookmarkTreeNode.push();
			await this._this.tools.sleep(1000);
			s=(Object.keys(this._this.sto.bookmarks)).length;
			await browser.bookmarks.remove(g3.members[0]); 
			await this._this.tools.sleep(1000);
			test&=(s-2==(Object.keys(this._this.sto.bookmarks)).length);
			this._this.tools.console("onRemoved","a",test);}

			if(controle&0x200){test=true;
			// A  B
			// |->
			// C
			A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
			B = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "B"});
			C = this._this.sto.Groups.new({parents: [A.id],title: "C"});
			await this._this.sto.BookmarkTreeNode.push();
			await this._this.tools.sleep(1000);
			await browser.bookmarks.move(C.members[0],{parentId:B.members[0]});
			await this._this.tools.sleep(1000);
			a = this._this.sto.bookmarks[A.members[0]];
			b = this._this.sto.bookmarks[B.members[0]];
			g = this._this.sto.Groups.get_groups_of(b);
			C = this._this.sto.groups[this._this.sto.groups[g[0]].children[0]];
			c = this._this.sto.bookmarks[C.members[0]];
			test&=(A.members.length==1)&&(A.children.length==0)&&(A.parents.length==1);
			test&=(B.members.length==1)&&(B.children.length==1)&&(B.parents.length==1);
			test&=(C.members.length==1)&&(C.children.length==0)&&(C.parents.length==1);
			test&=(a.parent == "toolbar_____")&&(a.children.length==0);
			test&=(b.parent == "toolbar_____")&&(b.children.length==1);
			test&=(c.parent == b.id)&&(c.children.length==0);
			//
			//   A       E
			//  / \      /\
			// B   C     //
			//  \ /    //
			//   D  ===
			//
			A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
			B = this._this.sto.Groups.new({parents: [A.id],title: "B"});
			C = this._this.sto.Groups.new({parents: [A.id],title: "C"});
			D = this._this.sto.Groups.new({parents: [B.id,C.id],title: "D"});
			E = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "E"});
			await this._this.sto.BookmarkTreeNode.push();
			await this._this.tools.sleep(1000);
			await browser.bookmarks.move(D.members[0],{parentId:E.members[0]});
			await this._this.tools.sleep(1000);
			test&=(D.members.length==1)&&(D.children.length==0)&&(D.parents.length==1);
			test&=(E.members.length==1)&&(E.children.length==1)&&(E.parents.length==1);
			test&=(B.members.length==1)&&(B.children.length==0)&&(B.parents.length==1);
			test&=(C.members.length==1)&&(C.children.length==0)&&(C.parents.length==1);
			//
			//   A       E
			//  / \      ||
			// B   C     //
			//  \ /    //
			//   D  <==
			//
			A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
			B = this._this.sto.Groups.new({parents: [A.id],title: "B"});
			C = this._this.sto.Groups.new({parents: [A.id],title: "C"});
			D = this._this.sto.Groups.new({parents: [B.id,C.id],title: "D"});
			E = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "E"});
			await this._this.sto.BookmarkTreeNode.push();
			await this._this.tools.sleep(1000);
			await browser.bookmarks.move(E.members[0],{parentId:D.members[0]});
			await this._this.tools.sleep(1000);
			test&=(D.members.length==2)&&(D.children.length==1)&&(D.parents.length==2);
			test&=(E.members.length==2)&&(E.children.length==0)&&(E.parents.length==1);
			//
			//   A       E
			//  / \      |\\
			// B   C     F ||
			//  \ /        //
			//   D  <=====//
			//
			A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
			B = this._this.sto.Groups.new({parents: [A.id],title: "B"});
			C = this._this.sto.Groups.new({parents: [A.id],title: "C"});
			D = this._this.sto.Groups.new({parents: [B.id,C.id],title: "D"});
			E = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "E"});
			F = this._this.sto.Groups.new({parents: [E.id],title: "F"});
			await this._this.sto.BookmarkTreeNode.push();
			await this._this.tools.sleep(1000);
			await browser.bookmarks.move(E.members.first(),{parentId:A.members.last()});
			await this._this.tools.sleep(1000);
			test&=(D.members.length==2)&&(D.children.length==0)&&(D.parents.length==2);
			for(a of D.members) test&=(this._this.sto.bookmarks[a].children.length==0)&&(this._this.sto.bookmarks[a].parent!=undefined);
			test&=(E.members.length==1)&&(E.children.length==1)&&(E.parents.length==1);
			for(a of E.members) test&=(this._this.sto.bookmarks[a].children.length==1)&&(this._this.sto.bookmarks[a].parent!=undefined);
			test&=(F.members.length==1)&&(F.children.length==0)&&(F.parents.length==1);
			for(a of F.members) test&=(this._this.sto.bookmarks[a].children.length==0)&&(this._this.sto.bookmarks[a].parent!=undefined);
			//
			//   A     E
			//  / \    |
			// B   C   F
			//  \ /
			//   D
			//
			//
			A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
			B = this._this.sto.Groups.new({parents: [A.id],title: "B"});
			C = this._this.sto.Groups.new({parents: [A.id],title: "C"});
			D = this._this.sto.Groups.new({parents: [B.id,C.id],title: "D"});
			E = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "E"});
			F = this._this.sto.Groups.new({parents: [E.id],title: "F"});
			await this._this.sto.BookmarkTreeNode.push();
			await this._this.tools.sleep(1000);
			await this._this.sto.set();
			await browser.bookmarks.move(E.members.first(),{parentId:D.members.last()});
			await this._this.tools.sleep(1000);
			await browser.bookmarks.move(E.members.first(),{parentId:A.members.last()});
			await this._this.tools.sleep(1000);
			this._this.tools.console("onMoved","a",test);}
		if(controle&(0x8+0x10+0x20+0x200)) this._this.tools.groupEnd();

		if(controle&(0x40+0x80+0x100+0x400))this._this.tools.group("requests");
			if(controle&0x40){test=true;
			// Handler
			s=((await browser.bookmarks.getSubTree("toolbar_____"))[0]).children.length;
			A=this._this.sto.Bookmarks.new({parent: "toolbar_____",title:"A"});
			B=this._this.sto.Bookmarks.new({parent:A.id,title:"B"});
			await this._this.sto.BookmarkTreeNode.push();
			await this._this.tools.sleep(1000);
			t=((await browser.bookmarks.getSubTree("toolbar_____"))[0]).children.length;
			test&=(s+1==t);
			s=((await browser.bookmarks.getSubTree("toolbar_____"))[0]).children.length;
			A=this._this.sto.Bookmarks.new({parent: "toolbar_____",title:"A"});
			B=this._this.sto.Bookmarks.new({parent: "toolbar_____",title:"B"});
			C=this._this.sto.Bookmarks.new({parent: "toolbar_____",title:"C"});
			E=this._this.sto.Bookmarks.new({parent: "toolbar_____",title:"E"});
			F=this._this.sto.Bookmarks.new({parent: "toolbar_____",title:"F"});
			await this._this.sto.BookmarkTreeNode.push();
			await this._this.tools.sleep(1000);
			t=((await browser.bookmarks.getSubTree("toolbar_____"))[0]).children.length;
			test&=(s+5==t);
			this._this.tools.console("onCreated","a",test);}

			if(controle&0x80){test=true;
			A=this._this.sto.Bookmarks.new({parent: "toolbar_____",title:"A"});
			await this._this.sto.BookmarkTreeNode.push();
			await this._this.tools.sleep(1000);
			i=this._this.sto.bookmarks["toolbar_____"].children[0];
			this._this.sto.Bookmarks.update_at(i,{title:"<UNIQUE>tamere"});
			await this._this.sto.BookmarkTreeNode.push();
			await this._this.tools.sleep(1000);
			s=(await browser.bookmarks.getSubTree(i))[0];
			test&=s.title == "<UNIQUE>tamere";
			this._this.tools.console("onChanged","a",test);}

			if(controle&0x100){test=true;
			// 		 	A
			// 		   / \  
			//        B   C 
			//        |  / 
			//        D-'  
			//        '.....F
			// 
			A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "1"});
            B = this._this.sto.Groups.new({parents: [A.id],title: "1.1"});
            C = this._this.sto.Groups.new({parents: [A.id],title: "1.2"});
            D = this._this.sto.Groups.new({parents: [B.id,C.id],title: "1.1.1"});
            F = this._this.sto.Groups.new({parents: [D.id],title: "1.1.1.1"});
			await this._this.sto.BookmarkTreeNode.push();
			await this._this.tools.sleep(1000);
			u=(Object.keys(this._this.sto.groups)).length;
			this._this.sto.Groups.delete(F);
			await this._this.sto.BookmarkTreeNode.push();
			await this._this.tools.sleep(1000);
			t=(Object.keys(this._this.sto.groups)).length;
			test&=(u-1==t);
			u=(Object.keys(this._this.sto.groups)).length;
			this._this.sto.Groups.delete(D);
			await this._this.sto.BookmarkTreeNode.push();
			await this._this.tools.sleep(1000);
			t=(Object.keys(this._this.sto.groups)).length;
			test&=(u-1==t);
			this._this.tools.console("onRemoved","a",test);}

			if(controle&0x400){test=true;
			// 		 	1
			// 		   / \  
			//       1.1   1.2
			//        |  / 
			//        1.1.1.-'  
			//        '.....1.1.1.1.
			// 
			A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "1"});
            B = this._this.sto.Groups.new({parents: [A.id],title: "1.1"});
            C = this._this.sto.Groups.new({parents: [A.id],title: "1.2"});
            D = this._this.sto.Groups.new({parents: [B.id,C.id],title: "1.1.1"});
            F = this._this.sto.Groups.new({parents: [D.id],title: "1.1.1.1"});
			await this._this.sto.BookmarkTreeNode.push();
			await this._this.tools.sleep(1000);
			u=(Object.keys(this._this.sto.groups)).length;
			v=(Object.keys(this._this.sto.bookmarks)).length;
			await this._this.sto.Groups.move(F,A);
			await this._this.sto.BookmarkTreeNode.push();
			await this._this.tools.sleep(1000);
			t=(Object.keys(this._this.sto.groups)).length;
			r=(Object.keys(this._this.sto.bookmarks)).length;
			test&=(u==t);
			test&=(v-1==r);
			this._this.tools.console("onMoved","a",test);}
		if(controle&(0x40+0x80+0x100^0x400))this._this.tools.groupEnd();
	this._this.tools.groupEnd();
	if(false&debug){
		this._this.tools.parsePref("backend.debug.backend.storage.Groups.delete=boolean=false;;");
		this._this.tools.parsePref("backend.debug.backend.storage.BookmarkTreeNode.push=boolean=false;;");
		this._this.tools.parsePref("backend.debug.backend.storage.BookmarkTreeNode.pull=boolean=false;;");
		this._this.tools.parsePref("backend.debug.browser.bookmarks.onChanged=boolean=true;;");
		this._this.tools.parsePref("backend.debug.browser.bookmarks.onRemoved=boolean=true;;");
		this._this.tools.parsePref("backend.debug.browser.bookmarks.onCreated=boolean=true;;");
		this._this.tools.parsePref("backend.debug.backend.storage.Groups.to_bookmarks=boolean=false;;");
		this._this.tools.parsePref("backend.debug.backend.storage.BookmarkTreeNode.requests.onChanged=boolean=false;;");	
		this._this.tools.parsePref("backend.debug.backend.storage.BookmarkTreeNode.requests.onCreated=boolean=false;;");	
		this._this.tools.parsePref("backend.debug.backend.storage.BookmarkTreeNode.requests.onRemoved=boolean=false;;");	
		this._this.tools.parsePref("backend.debug.backend.storage.BookmarkTreeNode.handlers.onChanged=boolean=false;;");	
		this._this.tools.parsePref("backend.debug.backend.storage.BookmarkTreeNode.handlers.onCreated=boolean=false;;");	
		this._this.tools.parsePref("backend.debug.backend.storage.BookmarkTreeNode.handlers.onRemoved=boolean=false;;");
	}
}
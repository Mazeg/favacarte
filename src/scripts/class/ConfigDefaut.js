/*
    Copyright (C) 2020-present Aïdo WILHELM

    This file is part of favacarte.

    favacarte is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    favacarte is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with favacarte.  If not, see <https://www.gnu.org/licenses/>.
*/
/*
 * Config "hardcoded" de l'interface
 */
class ConfigDefaut {constructor(){
		 														// Config par défaut, en dur, la config "par défaut"
	this.temp= { 																	// Stockage temporaire
		frontend: { 															// Stockage du frontend
			position: { 														// position dans l'ui
				x:												0,		
				y:												0
			},
			scale: 												1,			// niveau de zoom		
			nodes: {
				selected: 										[] 			// noeud selectionnés
			},
			tabNav: 											false		// navtab est-elle active
		},
		backend: {
			active: 											false 		// la div html est-elle active
		}
	};
	this.frontend= {
		interface: {
			images: {
				width: 											32,			// largeur des icones
				height: 										32,			// hauteur des icones
				default: {
						folder: {
							image: 									"data/ui/dossier_web.png"
						},
						bookmark: {
							image: 									"data/ui/page_web.png"
						},
						broken: {
							image: 									"data/ui/undef_bookmark_type.png"
						},
						root: {
						image: 									"data/logo_root.png",
						id: 									"toolbar_____"
					}
				},		
				logorc: 										"data/logo_imgrc.png", 			// logo coin droit 
			},
			transition: {
				opacity: 										150, 				// Durée de l'animation initiale de toggling
				tooltipDelay:  									1000 				// Delai avant apparition de la popup visjs
			},
			cross: 																// Affichage de la coix
			{
				enabled: 										false, 			// Afficher la croix
				width: 											1, 				// Taille en px
				len: 											8, 				// Taille ne px
				color: 											"red"  			// couleur de la croix
			},
			body_blur: 											 "0px", 		// Floutage de l'arriere plan
			opacity: 											 0.8, 			// background opacity
			background: 										 "radial-gradient(ellipse at center 2%, #221385 1%, #180d52 20%, #000000 100%)", // couleur du fond
			hideLogo: 											false,
			hideRootLogo: 										false,
			max_depth: 											2, 			// Niveau maximal affiché dans la vue par défaut niveaux: {0,1,2,3,...,n}: n+1 niveaux
			min_opacity: 										0.5,			// Opacité du graph à max_depth
			font: { 															// configuration de la police
				color: 											'white', 		// Option sur la couleur du texte
				bold: 											true 			// Option configuration de la taille du texte
			},
			edges: { 															// settings des edges visjs
				color: 											[255,255,255],// Couleur des edges [R,G,B]
				width: 											3,			// Largeur des edges					
				smooth: 										false 		// Forme des edges
			},
			nodes: {
				chosen: { 													    // Styles activés quand le noeud est survolé / choisit
					shadow: {
						enabled: 								true, 			 // Activer les ombres
						color: 									[255,255,255,1], // Couleur de l'ombre [R,G,B,A]
						size: 									10, 			 // Taille de l'ombre
						offset: { 												 // Décalages de l'ombre
							x: 									5,				 // Offset X de l'ombre
							y:  								5 				 // Offset Y de l'ombre
						}
					},
					size: 										0 			 	// Taille du point lorsqu'on déplace un noeud (e.g. EditEdgeView)
				} 
			},
			physics: { 															// configuration du moteur physic visjs
				enabled: 										true
			},
			ctx: { 																// settings du context menu
				font: { 														// configuration de la police
					color: 										"white",
					bold: 										false
				},
				background: 									"black",
				textfield: { 													// Styles des textes field du context menu
					font: {
						color: 									"white",
						bold: 									false
					},
					background: 								"black" 		// Background des text field
				},
				hover: {
					background: 								"#180d52" 	// Background color on hovering
				}
			},
			CSS: 												[] 				// Styles CSS à ajouter
		},
		controller: {
			openFolderFullGraphe: 								false, 		// La structure Groups se comporte comme un graphe non orienté, notamment à l'ouverture de liens
			openFolderDepth: 					 				1,			// Niveau de récursion aka nombre de niveau de sous dossiers à ouvrir
			openBookmarksInNewTabs: 							undefined,	// Preference navigteur, ouvrir les favoris dans un nouvel onglet
			openLinkAndKeepInterface: 							false,
			Marquer_cette_page_auto_add: 						true,		// permet d'ajouter automatiquement le favoris au noeud le plus proche
			deplacement: { 													
				nearest_threshold:
				{
					link_node: 			 						100, 			// cercle de déclenchement de la liaison (link) à l'approche d'un noeud
					ctxmenu: 									50			// cercle de déclenchement du contextmenu à l'approche d'un noeud (devrait être toujours < à get_nearest_circle_link) sinon perte de fonctionnalité à la navtab
				},
				vitesse: 							 			30, 					// vitesse de deplacement
				d: { 														// directions de déplacement
					h: 											 "ArrowUp", 		// Touche pour aller vers le haut
					b: 											 "ArrowDown", 		// vers le bas
					g: 				 							 "ArrowLeft",  		// aller à gauche
					d: 											 "ArrowRight"  		// aller à droite
				},
				escape: 										 "Escape", 		
				submit: 						 				 "Enter",
				kwait_time: 									100, 		// temps d'attente avant de conséidérer que l'utilisateur de frappe plus
				anim: {
					total: 	 									200, 			// temps d'animation total pour un pas
			 		fname:  									"linear", 		// cf doc de moveTo visjs
			 		normale: { 												// Parametre de la fonction normale
					 	coef: 									1
				 	}
				}
			}
		}
	};
	this.backend= {
		openLinksWithDaemon: 									true, 			// Utiliser la méthode d'ouverture avec le démon
		debug: 													{ },
		workers: {														
			sync_icons: 										1000*60*60  	// Délai entre chaque essai pour récupérer les images manquantes (en ms)
		},
		api: {
			bookmarks: {
				api_retry: 										3,  			// Nombre maximum d'essai d'ajout à l'API avant d'abandonner				
				newwindow_accu_sleep: 							500 			// Temps de remplissage de l'accumulateur de capacité infinie
			}
		},
		storage: {
			BookmarkTreeNode: {
				handlers: {
					onCreated: 									false 			// politique d'affichage dans l'ui (ou bien depth en fonction de la profondeur)
				}																// ou bien en booléen : on force le comportement : eg : false : afficher tous les nouveau inserts(ne pas les estCache)
			}
		}
	};
}};
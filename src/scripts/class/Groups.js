/*
    Copyright (C) 2020-present Aïdo WILHELM

    This file is part of favacarte.

    favacarte is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    favacarte is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with favacarte.  If not, see <https://www.gnu.org/licenses/>.
*/
"use strict";
/*
 * visjs |===| Groups |===| Bookmarks |===| BookmarkTreeNode |===| firefox
 *                *
 *
 * Définition de l'interface d'interaction Groups
 * les choses sont rangés comme l'utilisateur le voit dans l'interface
 *
 */
class Groups {constructor(){
	this.new 								= this.new;
	this.get_groups_of 						= this.get_groups_of;
	this.delete 							= this.delete;
	this.is_rooted 							= this.is_rooted;
	this.update_at 							= this.update_at;
	this.propagate_attributes 				= this.propagate_attributes;
	this.split 								= this.split;
	this.unlink  							= this.unlink;
	this.delete_empty 						= this.delete_empty;
	this.to_bookmarks 						= this.to_bookmarks;
	this.isGroups 							= this.isGroups;
	this.link_icons                     	= this.link_icons;
	this.get_default 						= this.get_default;
	this.to_frontend 						= this.to_frontend;
	this.copy_at 							= this.copy_at;
	this.move 								= this.move;
	this.epaissir 							= this.epaissir;
	this.affiner 							= this.affiner;
	// vérifié
	this.regression 						= _backend_storage_Groups_regression;
}
	//
	//   A
	//   |
	//   B
	//
	//   A
	//  / \
	// B   C
	//  \ /
	//   D
	//   |
	//   E
	//
	// Problème: tous les bookmarks vont dans members[0]
	epaissir(branch_root,forcePID=undefined)
	{
		let parentId,c,b,rv;
		if(typeof(branch_root)=="string") branch_root=this._this.sto.groups[branch_root];

		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Groups.epaissir) {
			this._this.tools.group("backend.storage.Groups.epaissir");
			this._this.tools.console("branch_root=","d",branch_root); }} catch(e) {}

		//
		// il ne faut mettre le dernier membres, mais le membre qui n'a 
		// il faut ajouter une option pour forcer le parent de ce machin (pour application avec la move)
		//
		c = this._this.api.Object.copy(this._this.sto.bookmarks[branch_root.members.last()]);
		delete c.parent;
		b = this._this.sto.Bookmarks.new(c);
//		if(forcePID!=undefined)
//			b.parent = forcePID;
//		else
			b.parent = this._this.sto.bookmarks[branch_root.members.last()].parent;
		branch_root.members.push(b.id);
		for(c of branch_root.children)
		{
			rv=this.epaissir(c);
			rv.parent = b.id;
			b.children.push(rv.id);
		}

		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Groups.epaissir) {
			this._this.tools.console("b=","d",b);
			this._this.tools.groupEnd(); }} catch(e) {}
		return b;
	}
	async affiner(branch_root,first=true)
	{
		let parentId,c,b;
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Groups.affiner) {
			this._this.tools.group("backend.storage.Groups.affiner");
			this._this.tools.console("branch_root=","d",branch_root);}} catch(e) {}

		if(typeof(branch_root)=="string") branch_root=this._this.sto.groups[branch_root];
		await this._this.sto.Bookmarks.delete(branch_root.members.last(),false,true,false,first);
		for(c of branch_root.children)
			await this.affiner(c,false);
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Groups.affiner) {
			this._this.tools.groupEnd();}} catch(e) {}
	}
	/* But: gérer un arborescence de bookmarks */
	async move(src,dstParent,exclude=[],bookmarkParentId=undefined)
	{
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Groups.move) {
			this._this.tools.group("backend.storage.Groups.move");
			this._this.tools.console("src=","d",src);
			this._this.tools.console("dstParent=","d",dstParent);
			this._this.tools.console("exclude=","d",exclude); }} catch(e) {}
		let p,i,m,ii,a;

		// égualisation au plus:
		while(src.members.length<dstParent.members.length)
			this.epaissir(src);
		// équalisation au moins:
		while(src.members.length>dstParent.members.length)
			await this.affiner(src);
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Groups.move) {
			this._this.tools.console("src(après affinage)=","d",src);
		}} catch(e) {}
		// la source et la destination sont bien sur la même longueur d'onde, on peut à présent les relier

		// Etape de liaison des groups
		for(i of src.parents)
		{
			////////console.warn("i=",i);
			p = this._this.sto.groups[i];
			ii = p.children.indexOf(src.id);
			if(ii!=-1)
				p.children.splice(ii,1);
		}
		dstParent.children.push(src.id);
		src.parents=[dstParent.id];
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Groups.move) {
			this._this.tools.console("src(après liaison groups)=","d",src);
		}} catch(e) {}

		// Etape de liaison des bookmarks
		let bSrc,pbSrc,bDst;
		let pids = dstParent.members.slice();
		for(i=0;i<src.members.length;i++)
		{
			bSrc 	= this._this.sto.bookmarks[src.members[i]];
			pbSrc 	= this._this.sto.bookmarks[bSrc.parent];
			if(exclude.includes(bSrc.id))
			{
				ii = pids.indexOf(bookmarkParentId);
				bDst 	= this._this.sto.bookmarks[pids[ii]];
				pids.splice(ii,1); 
			}
			else
			{
				bDst 	= this._this.sto.bookmarks[pids.shift()];
				if(bSrc.state==undefined)
					bSrc.state="Moved";
			}
			bSrc.parent = bDst.id;
			a = pbSrc.children.indexOf(bSrc.id);
			if(a!=-1) pbSrc.children.splice(a,1);
			bDst.children.push(bSrc.id);
			try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Groups.move) {
				this._this.tools.console("bSrc=","d",bSrc);
				this._this.tools.console("pbSrc=","d",pbSrc);
				this._this.tools.console("bDst=","d",bDst);
			}} catch(e) {}
		}

		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Groups.move) {
			this._this.tools.groupEnd(); }} catch(e) {}
	}
	copy_at(from,at,set_flags=true,recursive=true,accu_traites=[])
	{
		// on va avoir besoin d'un boolean pour skip
		// le set_flag de la fonction
		// TODO TODO
		let gs = (Object.keys(this._this.sto.groups)).length,bs=(Object.keys(this._this.sto.bookmarks)).length;
		let g = this.new({
			parents: [at.id],
			url:from.url,title:from.title,image:from.image,
			date:from.date,type:from.type,coord:{x:from.x,y:from.y},note:from.note
		},[],set_flags);
		let c,i,ii,rv,newg;

		if(recursive)
		{
			for(c of from.children.slice())
			{
				i = accu_traites.findIndex(o => o.id == c );
				if( i == -1)
				{
					ii = accu_traites.push({id: c});ii--;
					rv = this.copy_at(this._this.sto.groups[c],g,set_flags,recursive,accu_traites);
					accu_traites[ii].copyId=rv.id;
				}
				else
				{
					newg = this._this.sto.groups[accu_traites[i].copyId];
					newg.parents.push(g.id); // ici portentiellement on a trop de parents
					g.children.push(newg.id);
				}
			}
		}		
		this.to_bookmarks();
		return g;
	}
	/* But: diviser un Groupe en Branche (supprimer un éventuel) héritage multiple */
	// On part d'un endroit ou le noeud peut etre scindé ou non
    //    E   F
    //     \ /
	//      D
	//      | 
	//      A            
	//      |  
	//      C              Ouvrir à partir de D: pour chaque membre -1, je remplace dans le parent 1, puis dans le parent 2, etc en meme temps je push dans les parents du children
	// 					   Ouvrir à partir de A: pour chaque membre - 1, je fait un push dans les children du parent
	//                                                et pour chaque membre -1, je le met dans les parents du/des child
	//                     Ouvrir à partir de C: pour chaque membre - 1, je créer un groupe et je le push dans le parent.
	split(g,recursive=false,limite_split=-1)
	{
		if(typeof(g)!="object") g = this._this.sto.groups[g];
		let parents = g.parents,members=g.members,a=0,newbranche;
		let enfants = g.children;
		let c;
		while(members.length>1&&a!=limite_split)
		{
			newbranche=this.new({id:members.shift(),title:g.title,url:g.url,date:g.date,children:g.children.slice(),url:g.url,image:g.image,coord:this._this.api.Object.copy(g.coord),estCache:g.estCache,depth:g.estCache,note:g.note});
			a++;
			if(parents.length==0)
				continue;

			newbranche.parents=[parents[0]];
			parent = this._this.sto.groups[parents[0]];

			if(g.parents.length>1)
			{
				parent.children[parent.children.indexOf(g.id)]=newbranche.id;
				parents.shift();
			}
			else
				parent.children.push(newbranche.id);
		
			for(c of g.children)
				this._this.sto.groups[c].parents.push(newbranche.id);	

									
		}
		if(recursive)
			for(c of g.children)
				this.split(this._this.sto.groups[c],recursive,limite_split);
	} // vérifié
	/*
	 * But: délier (<=> libérer le Groups d'une contrainte, du Groups vide (totalement libre) à Groups plein
	 */
	async unlink(gparent,genfant,commit_later=false)
	{
		/*        A          A  A'         A  A'
		 *        |          \ /           \  /
		 *        B           B             B
		 *      /   \        / \            |
		 *     C     D      C   D           C
		 *       a.           b.              c.
		 *
		 * Regardons le cas c.
		 * pour délier: B: je fait un split:
		 * A  A'
		 * |  |
		 * B  B         ensuite il ne reste que à faire 2 suppressions non récursives
		 * |  |
		 * C  C
		 *
		 * Regardons le cas b. : meme concept:
		 *  A          A'
		 *  |          |
		 *  B          B
		 * / \       /   \
		 *C   D     C     D
		 *
		 * Regardons le cas a.
		 *  A
		 *  |
		 *  B
		 * / \
		 *C   D
		 *
		 * Procédure:
		 *  1. appliquer le split au noeud qu'on veut unlink.
		 *  2. unlink chaque B, et suppression de bookmark sur un seul niveau
		 * ====
		 *  Ce n'est pas supprimer des groupes mais bien des liens
		 */
		// création du lien
		// On enleve le liens dans les groups
		//////////////////console.warn(gparent.title+"("+gparent.id+") <- "+genfant.title+"("+genfant.id+")");

		let i = gparent.children.indexOf(genfant.id);
		if(i!=-1) gparent.children.splice(i,1);
		i=genfant.parents.indexOf(gparent.id);
		if(i!=-1) genfant.parents.splice(i,1);

		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Groups.unlink) {
			this._this.tools.group("backend.storage.Groups.unlink");
			this._this.tools.console("gparent=","d",gparent);
			this._this.tools.console("genfant=","d",genfant);}} catch(e) {}

		let mems = genfant.members.slice(),m;
		for(m of mems)
		{
			if(gparent.members.includes(this._this.sto.bookmarks[m].parent))
			{
				await this._this.sto.Bookmarks.delete(m,true,true,false,undefined,true);
			}
		}
		if(!commit_later)
		{
			await this._this.sto.set();
			await this._this.com.envoi("bookmark_change");
		}

		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Groups.unlink) {
			this._this.tools.console("gparent=","d",gparent);
			this._this.tools.console("this._this.sto.groups[gparent.id]=","d",this._this.sto.groups[gparent.id]);
			this._this.tools.console("genfant=","d",genfant);
			this._this.tools.groupEnd(); }} catch(e) {}
	}
	get_default(obj=undefined)
	{
		let n = {	id: undefined,members:[],
										children: [],parents: [],
										url:undefined,image:undefined,title:"",
										date:undefined,type:undefined,coord:{x: 0,y: 0},
										estCache:true,depth:0,note:""};
		if(obj!=undefined)
			n = this._this.api.Object.copy(n,obj);
		return n;
	}
	/* But: 	insérer un nouveau groupe dans la hirarchie */
	// except_ids: permet de forcer l'id du bookmarks
	// il fut aussi une option pour gérer l'héritage multiple : dontcreategroup
	new(o,except_ids=[],set_flag=true,obj=this.get_default(o),parent_ids=[],estCache=undefined)
	{
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Groups.new)	{
			this._this.tools.group("backend.storage.Groups.new");
			this._this.tools.console("o=","d",o);
			this._this.tools.console("except_ids=","d",except_ids);
			this._this.tools.console("set_flag=","d",set_flag);
			}} catch(e) {}

		let id,fid,g,p,m,pid,newb,a,pids,btn_pids,i,pmin;
		// le mec veut forcer le groupe à prendre un id en particulier
		if(this._this.sto.groups[obj.id]==undefined)
			id = obj.id;
		if(id!=undefined && this._this.sto.groups[id]==undefined) id=undefined;
		if(id==undefined) id=this._this.tools.gen_bid();
		g = this._this.sto.groups[id] = this._this.api.Object.copy(obj);
		g.id=id;
		g.members=[];
		g.date = this._this.tools.get_date();  


		pmin = -1;
		if(Array.isArray(g.parents)&&g.parents.length>0)
			for(pid of g.parents)
				if(pmin==-1||this._this.sto.groups[pid].depth<pmin) pmin = this._this.sto.groups[pid].depth;
		g.depth=pmin+1;
		if(estCache!=undefined)
			g.estCache=estCache;
		else
			g.estCache=(this._this.backend.storage.settings.frontend.interface.max_depth < g.depth);

		// ajout des Bookmarks dans les fils
		// Pour tout parent de tout groupes, je doit ajouter un Bookmarks
		if(Array.isArray(g.parents)&&g.parents.length>0)
		{
			for(pid of g.parents)
			{
				if(pid==undefined) {g.parents.splice(g.parents.indexOf(pid));continue;}
				p = this._this.sto.groups[pid];
				// correction de la prodondeur et de estCache
				if(p==undefined || !Array.isArray(p.members)) continue;

				pids = p.members.slice();
				btn_pids=parent_ids.slice();
				//console.warn("pids=",pids);
				//console.warn("btn_pids=",btn_pids);
				for(a=0;a<p.members.length;a++)
				{
					//console.warn("a=",a);
					fid=except_ids.shift();
					if(fid != undefined )
					{
						m=btn_pids.shift();
						i = pids.indexOf(m);
						if(i!=-1)pids.splice(i,1);
					}
					else m = pids.shift();
					//console.warn("fid=",fid,",m=",m);
					// ajouter des Bookmarks à la structure
					newb = this._this.backend.storage.Bookmarks.new(g,m,fid,set_flag&&(fid==undefined));
					g.members.push(newb.id);
				}
				p.children.push(g.id);
			}
		}
		// Cas restant ?
		// + Lancer une copie de groups pour tous les enfants
		// 
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Groups.new) {
			this._this.tools.console("g=","d",g);
			this._this.tools.groupEnd();}}catch(e){}

		return g;
	}
	async delete(obj,recursive=true,set_flag={default: true,except_ids:[]},first=true)
	{
		if(obj==undefined) return false;
		if(typeof(obj)=="string"||typeof(obj)=="number") obj=this._this.sto.groups[obj];
		if(obj==undefined) return false;
		
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Groups.delete)	{
			if(first)
				this._this.tools.group("backend.storage.Groups.delete");
			this._this.tools.console("obj=","d",obj);
			this._this.tools.console("recursive=","d",recursive);
			this._this.tools.console("set_flag=","d",set_flag);
		}} catch(e) {}

		let o,childs,members,m,p,parent,i,c,child;
		if(Array.isArray(obj))
		{
			for(o of obj)
			{
				// OK : ////////////////console.warn("o=",o);
				await this.delete(o,recursive,set_flag,false);
			}
			try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Groups.delete)	{
				if(first)
					this._this.tools.console("this._this.sto.groups=","d",this._this.sto.groups);}} catch(e) {}
			return;
		}
		else if(recursive)
		{
			childs = obj.children.slice();
			if(childs==undefined) childs=[];
			await this.delete(childs ,recursive,set_flag,false);
		}
//		////////////////console.warn("obj.title=",obj.title,",obj.id=",obj.id);


		// Suppression dans le stockage interne
		members = obj.members;
		if(obj.members==undefined) members=[];
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Groups.delete)	{
			this._this.tools.console("members","d",members);}} catch(e) {}
		for(m of members.slice())
		{
			//////////console.warn("START");
			if(set_flag.except_ids.includes(m)) // pas besoin de le synchroniser avec l'interface firefox
				await this._this.sto.Bookmarks.delete_at(m,false);
			else
				await this._this.sto.Bookmarks.delete(m,false,undefined,false,first);
			//////////console.warn("END");
		}

//////////console.warn("ok");
		// politique de suppression dans les parents : po
		for(p of obj.parents)
		{
			parent = this._this.sto.groups[p];
			try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Groups.delete)	{
				this._this.tools.console("p=","d",p);
				this._this.tools.console("parent=","d",parent);
				this._this.tools.console("obj=","d",obj);
			}} catch(e) {}
			if(parent==undefined) continue;
	//		console.warn("obj.id=",obj.id);
	//		console.warn("parent.children=",parent.children);
			i = parent.children.indexOf(obj.id);
			if(i==-1) continue;
			parent.children.splice(i,1);
	//		console.warn("i=",i);
			if(!recursive)
				parent.children=parent.children.concat(obj.children);
	//		console.warn("parent.children=",parent.children);
			// un genre de unlink
			// pour chaque enfant ajouté de cette maniere, on doit ajouter une edge, manuellement
		}
//////////console.warn("ok");
		// pour tous les enfants qui ont ce mec comme parent, on met : tous les parents du dessus
		for(c of obj.children)
		{
			child = this._this.sto.groups[c];
			if(child==undefined) continue;
			i = child.parents.indexOf(obj.id);
			if(i==-1) continue;
			child.parents.splice(i,1);
			if(!recursive)
				child.parents=child.parents.concat(obj.parents); // faire un distinct push plutot
		}	
		delete this._this.sto.groups[obj.id];

//////////console.warn("ok");


		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Groups.delete){
			if(first)
			{
				this._this.tools.groupEnd();
			}
		}} catch(e) {}
			
	}
	/* Permet de supprimer les groups qui ne sont plus reliés à la racine */
	async delete_empty(groups=Object.keys(this._this.sto.groups))
	{
		if(!Array.isArray(groups))
		{
			if(typeof(groups)=="object")
				groups=[groups.id];
			else
				groups=[groups];
		}
		let g,k,m;
		for(k of groups)
		{
			g=this._this.sto.groups[k];
			if(Array.isArray(g.members))for(m of g.members)
			{
				if(this._this.sto.Bookmarks[m]==undefined)
					g.members.splice(g.members.indexOf(m),1);
			}
			if(!Array.isArray(g.members)||g.members.length==0)
			{
				await this.delete(g,false);
			}
		}
	}
	get_groups_of(n)
	{
		let gids = [],gid;
		if(typeof(n)=="object")
			n=n.id;
		for(gid of Object.keys(this._this.sto.groups))
		{
			if( this._this.sto.groups[gid].members.includes(n) )
				gids.push(gid);
		}
		return gids;
	}
	is_rooted(g)
	{
		return ((g.members!=undefined&&g.members.length>0)||(g.parents!=undefined&&g.parents.length>0));
	}
	/*
	 * But: convertir les groups en bookmarks (souvent appelée par put_to_storage)
	 * // cette fonction n'étant pas indispensable à l'affichage, on pourrait éventuellement plus tard la déplacer dans le daemon
	 * // ce qui fait que le frontend n'aurait à deal que avec les objets de groupes 
	 * // le daemon s'occuperait des conversions nécessaires au fonctionnement
	 */
	to_bookmarks()
	{
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Groups.to_bookmarks) {
			this._this.tools.group("backend.storage.Groups.to_bookmarks");
			this._this.tools.console("this._this.sto.bookmarks=","d",this._this.sto.bookmarks);
			this._this.tools.console("this._this.sto.groups=","d",this._this.sto.groups); }} catch(e) {}

		let k,g,m,bm;
		for(k of Object.keys(this._this.sto.groups))
		{
			g = this._this.sto.groups[k];
			try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Groups.to_bookmarks) {
				this._this.tools.console("k=","d",k);
				this._this.tools.console("g=","d",g);}} catch(e) {}

			this._this.sto.Bookmarks.process_heritage_multiple(g);
		}
		// propagation des propriétés au sein des groups
		// tous les membres des groupes auront la meme props pour toutes celles listées ci-dessous
		// et ne sert uniquement que a ça
		for(k of Object.keys(this._this.sto.groups))
		{
			g = this._this.sto.groups[k];

			// question: un nouveau favoris viens d'être intégré: comment procéder ?
			for(m of g.members)
			{

				bm 			=this._this.sto.bookmarks[m];
				if(bm == undefined){ g.members.splice(g.members.indexOf(m),1); continue; }
				if(bm.state==undefined && (bm.url!=g.url||g.title!=bm.title||bm.note!=g.note)) // Alors il y a eu modification utilisateur
				{
					//////////////console.warn("(to_bookmarks)I set the flag on ",m,"=",bm.state);
					bm.state="Changed";
				}
				if(bm.parent==undefined && g.parents.length==1) 		bm.parent=g.parents[0];
				if(g.url!=undefined) 									bm.url = g.url;
				if(g.image!=undefined) 									bm.image = g.image;
				if(g.title!=undefined) 									bm.title = g.title;
				if(g.date!=undefined) 									bm.date = g.date;
				if(g.type!=undefined) 									bm.type = g.type;
				if(g.coord!=undefined) 									bm.coord = g.coord;
				if(g.estCache!=undefined) 								bm.estCache = g.estCache;
				if(g.depth!=undefined) 									bm.depth = g.depth;
				if(g.note!=undefined) 									bm.note = g.note;
				if(g.state!=undefined) 									bm.state = g.state;
			}
			delete g.state;
		}
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Groups.to_bookmarks) {
			this._this.tools.console("this._this.sto.bookmarks=","d",this._this.sto.bookmarks);
			this._this.tools.groupEnd();
		}} catch(e) {}
		return true;
	}
	link_icons()
	{
		let k,noeud,i;
		for(k of Object.keys(this._this.backend.storage.groups))
		{
			noeud = this._this.backend.storage.groups[k];
			if(noeud.image == undefined && noeud.url != undefined)
			{
				i = this._this.backend.storage.images.findIndex( image => image.url == (new URL(noeud.url)).host);
				if(i != -1) // dans ce context: null <=> undefined
					noeud.image = i;
			}

		}
	}
	/* But: propager des attributs du parent sur les enfants, de manière récursive */
	// on peut faire propager un attributs : le state sur tous les enfants de l'arbre
	propagate_attributes(gparent,genfant,state=undefined)
	{
		let c;
		genfant.depth  = gparent.depth + 1;


		// traiter les autres états ?
		if(state=="Created")
			//
			// Si l'enfant est déjà enraciné, pas besoin de le recréer
			if(!this.is_rooted(genfant))
			{
				genfant.state  = state;
			}

		if(genfant.children == undefined)
			genfant.children=[];

		for(c of genfant.children)
			this.propagate_attributes(genfant,this._this.sto.groups[c],state);
		
	}
	/*
	 * But: Générer la donnée pour le réseau visjs
	 * + axes d'amélioration : mettre des Datasets visjs
	 * 	C'est une fonction récursive, dont on peut controller le niveau de récursivité d'affichage avec le param depth
	 */
	to_frontend()
	{
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Groups.to_frontend) {this._this.tools.group("backend.storage.Groups.to_frontend"); }} catch(e) {}
		let nodes=[],edges=[],key,noeud_n,url,hidden,opacity,id;

		for(key of Object.keys(this._this.sto.groups))
		{
			noeud_n = this._this.sto.groups[key];
			if(typeof(noeud_n)=='undefined')
				continue;
//			from = this._this.api.Object.copy(from_default,from);
//			noeud_n
			noeud_n=this.get_default(noeud_n);
			url=undefined;
			hidden=noeud_n.estCache;
			if(hidden)
				opacity=this._this.sto.settings.frontend.interface.min_opacity;
			else
			{
				if(noeud_n.depth > this._this.sto.settings.frontend.interface.max_depth)
				{
					opacity=this._this.sto.settings.frontend.interface.min_opacity;
				}
				else
				{
					opacity=(1 - (1 - this._this.sto.settings.frontend.interface.min_opacity) * ( noeud_n.depth / this._this.sto.settings.frontend.interface.max_depth ));
				}
			}
			if(noeud_n.image != undefined)
				url=this._this.sto.images[noeud_n.image].image;
			try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Groups.to_frontend) {this._this.tools.console("this._this.sto.settings.frontend.interface.images.default.root.image=","d",this._this.sto.settings.frontend.interface.images.default.root.image);}} catch(e) {}
			if(noeud_n.image == undefined) 
			{							
				if(noeud_n.type=="folder")
					if(noeud_n.id == this._this.sto.settings.frontend.interface.images.default.root.id)
						url=this._this.sto.settings.frontend.interface.images.default.root.image;
					else
						url=this._this.sto.settings.frontend.interface.images.default.folder.image;
		   		else
		   			if(noeud_n.type=="bookmark")
		   			    url=this._this.sto.settings.frontend.interface.images.default.bookmark.image;
		   			else
						url=this._this.sto.settings.frontend.interface.images.default.broken.image;
			}
			if(typeof(noeud_n.coord)=='undefined')
				noeud_n.coord={x: undefined, y: undefined};

			nodes.push({
				id:  					noeud_n.id, 
				label:  				noeud_n.title,
				url: 					noeud_n.url,
				type: 					noeud_n.type,
				image:  				url,
				x: 						noeud_n.coord.x,
				y: 						noeud_n.coord.y,
				
				title:                  "<div class='vis-tooltip-label'>"+noeud_n.title.replace(/\n/g,"<br />")+"</div><div class='vis-tooltip-url'>"+((noeud_n.url==undefined)?"<br/>":noeud_n.url)+"</div><div class='vis-tooltip-date'>"+((noeud_n.date==undefined)?browser.i18n.getMessage("tooltip_unknow_date"):this._this.tools.get_date(noeud_n.date))+"</div><div class='vis-tooltip-note'>"+(noeud_n.note==undefined?"":noeud_n.note).replace(/\n/g,"<br />")+"</div>",
				hidden: 				hidden,
				favaOptions: {
					image_opacity: 		opacity 																				// options personalisée du réseau
				}
			});
			for(id of noeud_n.children)
			{
				edges.push({
					id: 					noeud_n.id+id,
					arrows: {
						from: 				noeud_n.id,
						to: 				id
					},
					from:           		noeud_n.id,
					to: 					id,
					color: 					'rgba('+this._this.sto.settings.frontend.interface.edges.color[0]+','+this._this.sto.settings.frontend.interface.edges.color[1]+','+this._this.sto.settings.frontend.interface.edges.color[2]+','+opacity+')'

				});
			}
		}
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Groups.to_frontend){this._this.tools.groupEnd();}} catch(e) {}
		return {nodes: nodes,edges: edges};
	}
	/* But: update les informations au sein d'un group, et pour tout le groupe */
	update_at(target_gid,ginsert,set_flag={default: true,except_ids:[]},ginsert_default={
	   date:this._this.tools.get_date(),type:"folder"
	   ,note:"",image: undefined,url:undefined,title:undefined})
	{
		let id,groupe,keys,k,m;
		ginsert = this._this.api.Object.copy(ginsert_default,ginsert);
		////////////////console.warn("ginsert.title=",ginsert.title);
		if(typeof(target_gid)=="object")
		{
			if(Array.isArray(target_gid))
			{
				for(id of target_gid)
					this.update_at(id,ginsert,set_flag);
				return;
			}
			else
				target_gid=target_gid.id;
		}
		groupe = this._this.sto.groups[target_gid];
		if(groupe==undefined) return false;
		keys=["url","image","title","date","type","note","title"];
		for(k of keys) if (ginsert[k]!=undefined)
		{
			////////////////console.warn("k=",k);
			////////////////console.warn("groupe[k]=",groupe[k]);
			////////////////console.warn("ginsert[k]=",ginsert[k]);
			groupe[k] = ginsert[k];
		}
		////////////////console.warn("END");

		if(typeof(groupe)=="object" && Array.isArray(groupe.members))
		{
			for(m of groupe.members)
			{
//				//////////////console.warn("set_flag.except_ids.=",set_flag.except_ids,",m=",m);
				if(set_flag.except_ids.includes(m))
				{
					//////////////console.warn("BEfore exclusion: ",this._this.sto.bookmarks[m].state);
					this._this.sto.Bookmarks.update_at(m,{
					url: ginsert.url,image: ginsert.image,date: ginsert.date,type: ginsert.type,note:ginsert.note,
					title:ginsert.title},false);
					//////////////console.warn("AFter exclusion: ",this._this.sto.bookmarks[m].state);
				}
				else
				{
					this._this.sto.Bookmarks.update_at(m,{
					url: ginsert.url,image: ginsert.image,date: ginsert.date,type: ginsert.type,note:ginsert.note,
					title:ginsert.title},set_flag.default);
				}
			}
		}
		return true;
	}
	isGroups(g)
	{
		if(g.parents!=undefined || g.members!=undefined)
			return true;
		if(g.parent!=undefined)
			return false;
		return false;
	}
}
async function _backend_storage_Groups_regression(controle=0xFFFFFFFF,more_tests=false,debug=false,verb=false)
{
	let gsz,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,test,g1,g2,g3,h1,h2,h3,h4,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,tests=[];
	this._this.tools.group("Groups");
		if(controle&0x1){test=true;
		gsz = Object.keys(this._this.sto.groups).length;
		g = this._this.sto.Groups.new();
		test=(Object.keys(this._this.sto.groups).length==gsz+1);
		/* 			toolbar_____
		 *   		/        \
		 * 		   g1		  g2
		 *            \     /
		 *				 g3
		 */
		g1 = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "g1"});
		g2 = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "g2"});
		g3 = this._this.sto.Groups.new({parents: [g1.id,g2.id],title: "g3"});
		test&=(g1.members.length==1)&&(g2.members.length==1)&&(g2.title=="g2")&&(g1.title == "g1");
		test&=(g1.parents.length==1)&&(g2.parents.length==1);
		test&=(g3.parents.length==2)&&(g3.members.length==2)&&(this._this.sto.bookmarks[g3.members[0]].parent!=this._this.sto.bookmarks[g3.members[1]].parent);
		test&=(g3.title=="g3")&&(this._this.sto.bookmarks[g3.members[1]].title=="g3");
		g1 = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "XxX",url:"http://tamere.fr/",note:"ok"});
		test&=(g1.title=="XxX")&&(g1.url=="http://tamere.fr/")&&(g1.note=="ok");
		/*      toolbar_____
		           |
		           A
                   /
                  B
                 / \
                C   D 
		 */
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
		B = this._this.sto.Groups.new({parents: [A.id],title: "B"});			
		C = this._this.sto.Groups.new({parents: [B.id],title: "C"});
		D = this._this.sto.Groups.new({parents: [B.id],title: "D"});
		test&=(A.children.length==1)&&(A.parents.length==1)&&(A.members.length==1);
		test&=(B.children.length==2)&&(B.parents.length==1)&&(B.members.length==1);
		test&=(C.children.length==0)&&(C.parents.length==1)&&(C.members.length==1);
		test&=(D.children.length==0)&&(D.parents.length==1)&&(D.members.length==1);
		/* 				A
		 *  	       / \
		 *            B   C
		 *           / \ 
		 *          F   D
		 */
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "TEST-1"});
		B = this._this.sto.Groups.new({parents: [A.id],title: "TEST-1-1"});	
		C = this._this.sto.Groups.new({parents: [A.id],title: "TEST-1-2"});		
		F = this._this.sto.Groups.new({parents: [B.id],title: "TEST-1-1-1"});
		D = this._this.sto.Groups.new({parents: [B.id],title: "TEST-1-1-2"});
		test&=(A.children.length==2)&&(C.children.length==0)&&(B.children.length==2);
		test&=(F.parents.length==1)&&(D.parents.length==1);
		test&=(A.members.length==1)&&(B.members.length==1)&&(C.members.length==1)&&(F.members.length==1)&&(D.members.length==1);
		/*       A
		 *      / \
		 *     B   C
		 *      \ /
		 *       D
		 */
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
		B = this._this.sto.Groups.new({parents: [A.id],title: "B"});	
		C = this._this.sto.Groups.new({parents: [A.id],title: "C"});	
		D = this._this.sto.Groups.new({parents: [C.id,B.id],title: "D"});
		test&=(D.members.length==2);
		E=this._this.sto.Groups.new({parents:[D.id]});
		test&=(E.members.length==2);
		/* 		 	A____
		 * 		   / \   \
		 *        B   C   F
		 *       /|  / \
		 *      / D-'   \
		 *     ''........:.-P
		 */
		s=(Object.keys(this._this.sto.groups)).length;
		t=(Object.keys(this._this.sto.bookmarks)).length;
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A===A"});
        B = this._this.sto.Groups.new({parents: [A.id],title: "B===B"});
        C = this._this.sto.Groups.new({parents: [A.id],title: "C===C"});
        D = this._this.sto.Groups.new({parents: [B.id,C.id],title: "D===D"});
        F = this._this.sto.Groups.new({parents: [A.id],title: "F===F"});
        P = this._this.sto.Groups.new(D);
        test&=(P.members.length==2)&&(D.members.length==2);
        test&=(P.parents.length==2)&&(D.parents.length==2);
        test&=(P.children.length==0)&&(D.children.length==0);
        test&=(C.children.length==2)&&(B.children.length==2);
        for(a of D.members) test&=(this._this.sto.bookmarks[a].children.length==0);
        for(a of F.members) test&=(this._this.sto.bookmarks[a].children.length==0);
       	test&=(s+6==(Object.keys(this._this.sto.groups)).length);
		test&=(t+8==(Object.keys(this._this.sto.bookmarks)).length);
        /* 		 	A
		 * 		   / \  
		 *        B   C 
		 *        |  / 
		 *        D-'  
		 *        '.....F
		 */ 
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A=o=A"});
        B = this._this.sto.Groups.new({parents: [A.id],title: "B=o=B"});
        C = this._this.sto.Groups.new({parents: [A.id],title: "C=o=C"});
        D = this._this.sto.Groups.new({parents: [B.id,C.id],title: "D=o=D"});
        F = this._this.sto.Groups.new({parents: [D.id],title: "F=o=F"});
    //    //////////console.warn("1.");
        test&=(A.members.length==1)&&(A.parents.length==1)&&(A.children.length==2);
        for(a of A.members) {
           test&=(this._this.sto.bookmarks[a].children.length==2);
           test&=(this._this.sto.bookmarks[a].parent!=undefined);}
    //    //////////console.warn("2.");
        test&=(B.members.length==1)&&(B.parents.length==1)&&(B.children.length==1);
        for(a of B.members) {
           test&=(this._this.sto.bookmarks[a].children.length==1);
           test&=(this._this.sto.bookmarks[a].parent!=undefined);}
    //    //////////console.warn("3.");
        test&=(C.members.length==1)&&(C.parents.length==1)&&(C.children.length==1);
        for(a of C.members) {
           test&=(this._this.sto.bookmarks[a].children.length==1);
           test&=(this._this.sto.bookmarks[a].parent!=undefined);}
    //    //////////console.warn("4.");
        test&=(D.members.length==2)&&(D.parents.length==2)&&(D.children.length==1);
        for(a of D.members) {
           test&=(this._this.sto.bookmarks[a].children.length==1);
           test&=(this._this.sto.bookmarks[a].parent!=undefined);}
    //    //////////console.warn("5.");
        test&=(F.members.length==2)&&(F.parents.length==1)&&(F.children.length==0);
        for(a of F.members) {
           test&=(this._this.sto.bookmarks[a].children.length==0);
           test&=(this._this.sto.bookmarks[a].parent!=undefined);}
		this._this.tools.console("new","a",test);}

		if(controle&0x2){test=true;
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
		B = this._this.sto.Groups.new({parents: [A.id],title: "B"});			
		C = this._this.sto.Groups.new({parents: [B.id],title: "C"});
		D = this._this.sto.Groups.new({parents: [B.id],title: "D"});
		g = this._this.sto.Groups.get_groups_of(A.members[0]);
		test=(g!=[]&&g!=undefined&&g.length==1);
		g = this._this.sto.Groups.get_groups_of(B.members[0]);
		test&=(g!=[]&&g!=undefined&&g.length==1);
		g = this._this.sto.Groups.get_groups_of(C.members[0]);
		test&=(g!=[]&&g!=undefined&&g.length==1);
		g = this._this.sto.Groups.get_groups_of(D.members[0]);
		test&=(g!=[]&&g!=undefined&&g.length==1);
		this._this.tools.console("get_groups_of","a",test);}

		if(controle&0x4){test=true;
		tests=[1,2,3];
		if(tests.includes(1))
		{
			A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
			gsz = Object.keys(this._this.sto.groups).length;
			await this._this.sto.Groups.delete(A);
			test&=Object.keys(this._this.sto.groups).length==gsz-1;
		}
		if(tests.includes(2))
		{
			gsz = Object.keys(this._this.sto.groups).length;	
			a=this._this.sto.Groups.new();
			g=this._this.sto.Groups.new();
			await this._this.sto.Groups.delete_empty(g);
			test&=Object.keys(this._this.sto.groups).length==gsz+1;
		}
		if(tests.includes(3))
		{
			d=50;
			o=[];
			t=[];
			s=Object.keys(this._this.sto.groups).length;
			for(c=0;c<d;c++)
			{
				g1 = this._this.sto.Groups.new({parents: o.slice(),title: "g"+c});
				t.push(g1.id);
				o=[g1.id];
			}
			await this._this.sto.Groups.delete(t);
			d=50;
			o=[];
			t=[];
			s=Object.keys(this._this.sto.groups).length;
			for(c=0;c<d;c++)
			{
				g1 = this._this.sto.Groups.new({parents: o.slice(),title: "g"+c});
				if(c==0)
					t.push(g1.id);
				o=[g1.id];
			}
			await this._this.sto.Groups.delete(t);
			d=50;
			o=[];
			t=[];
			s=Object.keys(this._this.sto.groups).length;
			for(c=0;c<d;c++)
			{
				g1 = this._this.sto.Groups.new({parents: o.slice(),title: "g"+c});
				if((c%3)==0)
				{
					t.push(g1.id);
					o=[g1.id];
				}
			}
			await this._this.sto.Groups.delete(t);
			test&=Object.keys(this._this.sto.groups).length==s;
		}
		//
		// 		A
		//		|
		//      B
		//    
		//
		//
		await this._this.api.reset();		
		await this._this.api.reset(true);
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
        B = this._this.sto.Groups.new({parents: [A.id],title: "B"});
     //   C = this.new({parents: [A.id],title: "C"});
        await this._this.sto.BookmarkTreeNode.push();
        await this._this.tools.sleep(1000);
        s = this._this.sto.groups["toolbar_____"].children.length;
        await this._this.sto.Groups.delete(A,true);
        await this._this.tools.sleep(1000);
        test&=(this._this.sto.groups["toolbar_____"].children.length==s-1);
		this._this.tools.console("delete","a",test);}

		if(controle&0x8){test=true;
		a = this._this.sto.Groups.new({parents: [],title: "a"});
        g1 = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "g1"});
        g2 = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "g2"});
        g3 = this._this.sto.Groups.new({parents: [g1.id,g2.id],title: "g3"});
		test=(this._this.sto.Groups.is_rooted(a)==false)&&(this._this.sto.Groups.is_rooted(g1))&&(this._this.sto.Groups.is_rooted(g2))&&(this._this.sto.Groups.is_rooted(g3));
		this._this.tools.console("is_rooted","a",test);}

		if(controle&0x10){test=true;
		a=this._this.sto.Groups.new();
		test=this._this.sto.Groups.update_at(a,{note: "test"});
		test&=(a.note=="test");
		g1 = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "g1"});
		g2 = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "g2"});
		g3 = this._this.sto.Groups.new({parents: [g1.id,g2.id],title: "g3"});
		for(m of a.members)
			test&=(this._this.sto.bookmarks[m].note=="test");
		this._this.sto.Groups.update_at(g3,{note: "OKGOOOGLE"});
		for(m of g3.members)
			test&=(this._this.sto.bookmarks[m].note=="OKGOOOGLE")&&(this._this.sto.bookmarks[m].state=="Changed");
		/*             toolbar___
		 *             /    \
		 *            h1     h2
		 *              \  /
		 *               h3
		 */
		h1 = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "h1",depth: 1});
		h2 = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "h2"});
		h3 = this._this.sto.Groups.new({parents: [h1.id,h2.id],title: "h3"});
		this._this.sto.Groups.update_at(h3,{note: "TEST: OK"});
		test&=(h3.members.length==2)&&(h3.note=="TEST: OK");
		for(m of h3.members)
			test&=(this._this.sto.bookmarks[m].note=="TEST: OK");
		this._this.tools.console("update_at","a",test);}

		if(controle&0x20){test=true;
		/*             toolbar___
		 *             /    \
		 *            h1     h2
		 *              \  /
		 *               h3
		 */
		h1 = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "h1",depth: 1});
		h2 = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "h2"});
		h3 = this._this.sto.Groups.new({parents: [h1.id,h2.id],title: "h3"});
		h4 = this._this.sto.Groups.new({parents: [h3.id],title: "h4"});
		test=(h3.children.length==1)&&(h3.parents.length==2);
		this._this.sto.Groups.propagate_attributes(h1,h3);
		test&=(h3.depth==h1.depth+1)&&(h4.depth==h1.depth+2);
		this._this.tools.console("propagate_attributes","a",test);}

		if(controle&0x40){test=true;
	    //    E   F
	    //     \ /
		//      D
		//      | 
		//      A            
		//      |  
		//      C             
		E = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "E"});
		F = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "F"});
		D = this._this.sto.Groups.new({parents: [E.id,F.id],title: "D"});
		A = this._this.sto.Groups.new({parents: [D.id],title: "A"});
		C = this._this.sto.Groups.new({parents: [A.id],title: "C"});
		test=(C.members.length==2)&&(A.members.length==2)&&(D.members.length==2)&&(A.parents.length==1);
		gsz=(Object.keys(this._this.sto.groups).length);
		this._this.sto.Groups.split(A);
		test&=(gsz+1==(Object.keys(this._this.sto.groups).length));
		test&=(A.members.length==1)&&(A.parents.length==1)&&(C.parents.length==2)&&(D.children.length==2);
		//
		//
		//  A  B   C   D
		//   \  \ /  /
		//      E
		//      |
		//      F
		//
		//
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
		B = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "B"});			
		C = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "C"});
		D = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "D"});
		E = this._this.sto.Groups.new({parents: [A.id,B.id,C.id,D.id],title: "E"});
		F = this._this.sto.Groups.new({parents: [E.id],title: "F"});
		test&=(E.members.length==4)&&(E.parents.length==4)&&(C.children.length==1);
		test&=(F.members.length==4)&&(F.parents.length==1);
		this._this.sto.Groups.split(E);
		test&=(F.parents.length==4);
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
		B = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "B"});			
		C = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "C"});
		D = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "D"});
		E = this._this.sto.Groups.new({parents: [A.id,B.id,C.id,D.id],title: "E"});
		F = this._this.sto.Groups.new({parents: [E.id],title: "F"});
		gsz=(Object.keys(this._this.sto.groups)).length;
		this._this.sto.Groups.split(E,true);
		test&=(F.members.length==1)&&(E.members.length==1)&&((Object.keys(this._this.sto.groups)).length==gsz+6);
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
		B = this._this.sto.Groups.new({parents: [A.id],title: "B"});			
		this._this.sto.Groups.split(A,true);
		test&=(A.children.length==1)&&(B.parents.length==1)&&(A.members.length*B.members.length==1);
		/* 				A
		 *  	       / \
		 *            B   C
		 *           / \ /  \
		 *          F   D    E
		 *              |
		 *              Z
		 *              |
		 *              X
		 *             / \     X
		 *            R   S  R  S
		 *             \ /    
		 *              T    T  T
		 */
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
		B = this._this.sto.Groups.new({parents: [A.id],title: "B"});	
		C = this._this.sto.Groups.new({parents: [A.id],title: "C"});		
		F = this._this.sto.Groups.new({parents: [B.id],title: "F"});
		D = this._this.sto.Groups.new({parents: [B.id,C.id],title: "D"});
		E = this._this.sto.Groups.new({parents: [C.id],title: "E"});	
		Z = this._this.sto.Groups.new({parents: [D.id],title: "Z"});	
		X = this._this.sto.Groups.new({parents: [Z.id],title: "X"});
		R = this._this.sto.Groups.new({parents: [X.id],title: "R"});
		S = this._this.sto.Groups.new({parents: [X.id],title: "S"});
		T = this._this.sto.Groups.new({parents: [R.id,S.id],title: "T"});
		this._this.sto.Groups.split(T);
		test&=(T.members.length==1&&T.parents.length==1);
		this._this.tools.console("split","a",test);}
		
		if(controle&0x80){test=true;
		//                 toolbar_____
		//                  |
		//					A
		//                 /*<---------------------- break;
		//                B
		//               / \
		//              C   D 
		//
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
		B = this._this.sto.Groups.new({parents: [A.id],title: "B"});			
		C = this._this.sto.Groups.new({parents: [B.id],title: "C"});
		D = this._this.sto.Groups.new({parents: [B.id],title: "D"});
		test&=(A.children.length==1)&&(A.parents.length==1)&&(A.members.length==1);
		test&=(B.children.length==2)&&(B.parents.length==1)&&(B.members.length==1);
		test&=(C.children.length==0)&&(C.parents.length==1)&&(C.members.length==1);
		test&=(D.children.length==0)&&(D.parents.length==1)&&(D.members.length==1);
		await this._this.sto.Groups.unlink(A,B,true);
		test&=(A.children.length==0)&&(A.parents.length==1)&&(A.members.length==1);
		test&=(B.children.length==2)&&(B.parents.length==0)&&(B.members.length==0);
		test&=(C.children.length==0)&&(C.parents.length==1)&&(C.members.length==0);
		test&=(D.children.length==0)&&(D.parents.length==1)&&(D.members.length==0);
		//					toolbar_____
		//					/     \
		//				   g1     <unknow>(g2)
		//
		g1 = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "g1"});
		g2 = this._this.sto.groups["toolbar_____"];
		gsz = g2.children.length;
		test = (g1.title=="g1")&&(g1.parents.length==1)&&(g1.members.length==1);
		await this._this.sto.Groups.unlink(g2,g1,true);
		test&=(g1.parents.length==0)&&(g2.children.length==gsz-1)&&(g1.members.length==0);
		// 				A
		//  	       / \
		//            B   C
		//           / \ /* \
		//          F   D    E
		//              |
		//              Z
		//              |
		//              X
		//
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
		B = this._this.sto.Groups.new({parents: [A.id],title: "B"});	
		C = this._this.sto.Groups.new({parents: [A.id],title: "C"});		
		F = this._this.sto.Groups.new({parents: [B.id],title: "F"});
		D = this._this.sto.Groups.new({parents: [B.id,C.id],title: "D"});
		E = this._this.sto.Groups.new({parents: [C.id],title: "E"});	
		Z = this._this.sto.Groups.new({parents: [D.id],title: "Z"});	
		X = this._this.sto.Groups.new({parents: [Z.id],title: "X"});
		await this._this.sto.Groups.unlink(C,D,true);
		test&=(C.children.length==1)&&(C.members.length==1)&&(C.parents.length==1);
		test&=(D.children.length==1)&&(D.members.length==1)&&(D.parents.length==1);
		test&=(E.children.length==0)&&(E.members.length==1)&&(E.parents.length==1);
		test&=(B.children.length==2)&&(B.members.length==1)&&(B.parents.length==1);
		test&=(Z.children.length==1)&&(Z.members.length==1)&&(Z.parents.length==1);
		test&=(X.children.length==0)&&(X.members.length==1)&&(X.parents.length==1);
		await this._this.sto.Groups.unlink(C,E,true);
		test&=(E.members.length==0);
		//         A
		//		   |*
		//         B
		//         |
		//         C
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
		B = this._this.sto.Groups.new({parents: [A.id],title: "B"});	
		C = this._this.sto.Groups.new({parents: [B.id],title: "C"});
		await this._this.sto.Groups.unlink(A,B,true);
		test&=(A.children.length==0)&&(A.members.length==1)&&(A.parents.length==1);
		test&=(B.children.length==1)&&(B.members.length==0)&&(B.parents.length==0);
		test&=(C.children.length==0)&&(C.members.length==0)&&(C.parents.length==1);
		test&=(this._this.sto.bookmarks[A.members.first()].children.length==0);
		//       toolbar
		// 		   |*
		//         A
		//		   |
		//         B
		//         |
		//         C
		T = this._this.sto.groups["toolbar_____"];
		s = T.children.length;
		t = this._this.sto.bookmarks[T.members.first()].children.length;
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
		B = this._this.sto.Groups.new({parents: [A.id],title: "B"});	
		C = this._this.sto.Groups.new({parents: [B.id],title: "C"});
		await this._this.sto.Groups.unlink(T,A,true);
		test&=(s==T.children.length);
		test&=(this._this.sto.bookmarks[T.members.first()].children.length==t);
		// 		A
		//     / \
		//    B   C
		//     \ /
		//      D
		//      |*
		//      E
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
		B = this._this.sto.Groups.new({parents: [A.id],title: "B"});	
		C = this._this.sto.Groups.new({parents: [A.id],title: "C"});
		D = this._this.sto.Groups.new({parents: [B.id,C.id],title: "D"});
		E = this._this.sto.Groups.new({parents: [D.id],title: "E"});
		await this._this.sto.Groups.unlink(D,E,true);
		D.children.push(E.id);
		E.parents.push(D.id);
		this._this.sto.Groups.to_bookmarks();
		await this._this.sto.Groups.unlink(D,E,true);
		test&=(D.children.length==0)&&(D.parents.length==2)&&(D.members.length==2);
		for(m of D.members) test&=this._this.sto.bookmarks[m].children.length==0;
		test&=(E.children.length==0)&&(E.parents.length==0)&&(E.members.length==0);
		//	    C             F
		//	  /   \         /   \
		//	 A     D ----- E     H
		//	  \   /         \   /
		//	    B             G
			A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"},undefined,undefined,undefined,undefined,false);
		B = this._this.sto.Groups.new({parents: [A.id],title: "B"},undefined,undefined,undefined,undefined,false);
		C = this._this.sto.Groups.new({parents: [A.id],title: "C"},undefined,undefined,undefined,undefined,false);
		D = this._this.sto.Groups.new({parents: [B.id,C.id],title: "D"},undefined,undefined,undefined,undefined,false);
		E = this._this.sto.Groups.new({parents: [D.id],title: "E"},undefined,undefined,undefined,undefined,false);
		F = this._this.sto.Groups.new({parents: [E.id],title: "F"},undefined,undefined,undefined,undefined,false);
		G = this._this.sto.Groups.new({parents: [E.id],title: "G"},undefined,undefined,undefined,undefined,false);
		H = this._this.sto.Groups.new({parents: [G.id,F.id],title: "H"},undefined,undefined,undefined,undefined,false);
		await this._this.sto.set();
		await this._this.sto.BookmarkTreeNode.push();
		await this._this.tools.sleep(1000);
		await this._this.sto.Groups.unlink(D,E,true);
		await this._this.sto.BookmarkTreeNode.push();
		await this._this.tools.sleep(1000);
		this._this.tools.console("unlink","a",test);}

		if(controle&0x100){test=true;
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
		B = this._this.sto.Groups.new({parents: [A.id],title: "B"});	
		C = this._this.sto.Groups.new({parents: [A.id],title: "C"});		
		F = this._this.sto.Groups.new({parents: [B.id],title: "F"});
		D = this._this.sto.Groups.new({parents: [B.id,C.id],title: "D"});
		E = this._this.sto.Groups.new({parents: [C.id],title: "E"});	
		Z = this._this.sto.Groups.new({parents: [D.id],title: "Z"});	
		X = this._this.sto.Groups.new({parents: [Z.id],title: "X"});
		await this._this.sto.Groups.unlink(C,D,true);
		await this._this.sto.Groups.unlink(C,E,true);
		await this._this.sto.Groups.delete_empty(E);
		test&=(this._this.sto.groups[E.id]==undefined);
		s=Object.keys(this._this.sto.groups).length;
		d=10;
		t=[];
		for(a=0;a<d;a++)
		{
			t.push(this._this.sto.Groups.new({parents: [],title: "X"+A}));
			t[t.length-1]=t[t.length-1].id;
		}
		await this._this.sto.Groups.delete_empty(t);
		test&=(s==Object.keys(this._this.sto.groups).length);
		this._this.tools.console("delete_empty","a",test);}

		if(controle&0x200){test=true;
		/*                 toolbar_____
		                    |
							A
		                   /
		                  B
		                 / \
		                C   D 
		 */
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
		B = this._this.sto.Groups.new({parents: [A.id],title: "B"});			
		C = this._this.sto.Groups.new({parents: [B.id],title: "C"});
		D = this._this.sto.Groups.new({parents: [B.id],title: "D"});
		this._this.sto.Groups.to_bookmarks();
		t=A.members.concat(B.members,C.members,D.members);
		test&=(t.length==4);
		for(a of t)
		{
			b=this._this.sto.bookmarks[a];
			test&=(b!=undefined);
		}
		/* 				A
		 *  	       / \
		 *            B   C
		 *           / \ /  \
		 *          F   D    E
		 *              |
		 *              Z
		 *              |			/* 				A
		 *  	       / \
		 *            B   C
		 *           / \ 
		 *          F   D
		 */
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
		B = this._this.sto.Groups.new({parents: [A.id],title: "B"});	
		C = this._this.sto.Groups.new({parents: [A.id],title: "C"});		
		F = this._this.sto.Groups.new({parents: [B.id],title: "F"});
		D = this._this.sto.Groups.new({parents: [B.id,C.id],title: "D"});
		E = this._this.sto.Groups.new({parents: [C.id],title: "E"});	
		Z = this._this.sto.Groups.new({parents: [D.id],title: "Z"});	
		X = this._this.sto.Groups.new({parents: [Z.id],title: "X"});
		this._this.sto.Groups.to_bookmarks();
		t=A.members.concat(B.members,C.members,D.members,E.members,F.members,X.members,Z.members);
		test&=(t.length==11);
		for(a of t)
		{
			b=this._this.sto.bookmarks[a];
			test&=(b!=undefined);
		}
		/* 				A
		 *  	       / \
		 *            B   C
		 *           / \ /  \
		 *          F   D    E
		 *              |
		 *              Z
		 *              |
		 *              X
		 *             / \     X
		 *            R   S  R  S
		 *             \ /    
		 *              T    T  T
		 */
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
		B = this._this.sto.Groups.new({parents: [A.id],title: "B"});	
		C = this._this.sto.Groups.new({parents: [A.id],title: "C"});		
		F = this._this.sto.Groups.new({parents: [B.id],title: "F"});
		D = this._this.sto.Groups.new({parents: [B.id,C.id],title: "D"});
		E = this._this.sto.Groups.new({parents: [C.id],title: "E"});	
		Z = this._this.sto.Groups.new({parents: [D.id],title: "Z"});	
		X = this._this.sto.Groups.new({parents: [Z.id],title: "X"});
		R = this._this.sto.Groups.new({parents: [X.id],title: "R"});
		S = this._this.sto.Groups.new({parents: [X.id],title: "S"});
		T = this._this.sto.Groups.new({parents: [R.id,S.id],title: "T"});
		this._this.sto.Groups.to_bookmarks();
		t=A.members.concat(B.members,C.members,D.members,E.members,F.members,X.members,Z.members,R.members,S.members,T.members);
		test&=(t.length==19);
		for(a of t)
		{
			b=this._this.sto.bookmarks[a];
			test&=(b!=undefined);
		}
		for(k of Object.keys(A))
		{
			if(["parents","children","members","id"].includes(k)) continue;
			for(m of A.members)
			{
				b=this._this.sto.bookmarks[m];
				test&=(b[k]==A[k]);
			}
		}
		this._this.tools.console("to_bookmarks","a",test);}

		if(controle&0x400){test=true;
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "TEST-1"});
		test&=(this._this.sto.Groups.isGroups(A) && !this._this.sto.Groups.isGroups(this._this.sto.bookmarks[A.members[0]]));
		this._this.tools.console("isGroups","a",test);}


		if(controle&0x800){test=true;
		i=this._this.sto.images.push({url: "tamere.fr",image:"data:image/x-icon;base64,AAABAAIAEBAAAAEAIABoBAAAJgAAACAgAAABACAAqBAAAI4EAAAoAAAAEAAAACAAAAABACAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP///zD9/f2W/f392P39/fn9/f35/f391/39/ZT+/v4uAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/v7+Cf39/Zn///////////////////////////////////////////39/ZX///8IAAAAAAAAAAAAAAAA/v7+Cf39/cH/////+v35/7TZp/92ul3/WKs6/1iqOv9yuFn/rNWd//j79v///////f39v////wgAAAAAAAAAAP39/Zn/////7PXp/3G3WP9TqDT/U6g0/1OoNP9TqDT/U6g0/1OoNP+Or1j//vDo///////9/f2VAAAAAP///zD/////+vz5/3G3V/9TqDT/WKo6/6LQkf/U6cz/1urO/6rUm/+Zo0r/8IZB//adZ////v7///////7+/i79/f2Y/////4nWzf9Lqkj/Vqo4/9Xqzv///////////////////////ebY//SHRv/0hUL//NjD///////9/f2U/f392v////8sxPH/Ebzt/43RsP/////////////////////////////////4roL/9IVC//i1jf///////f391/39/fr/////Cr37/wW8+/+16/7/////////////////9IVC//SFQv/0hUL/9IVC//SFQv/3pnX///////39/fn9/f36/////wu++/8FvPv/tuz+//////////////////SFQv/0hUL/9IVC//SFQv/0hUL/96p7///////9/f35/f392/////81yfz/CrL5/2uk9v///////////////////////////////////////////////////////f392P39/Zn/////ks/7/zdS7P84Rur/0NT6///////////////////////9/f////////////////////////39/Zb+/v4y//////n5/v9WYu3/NUPq/ztJ6/+VnPT/z9L6/9HU+v+WnfT/Ul7t/+Hj/P////////////////////8wAAAAAP39/Z3/////6Or9/1hj7v81Q+r/NUPq/zVD6v81Q+r/NUPq/zVD6v9sdvD////////////9/f2YAAAAAAAAAAD///8K/f39w//////5+f7/paz2/11p7v88Suv/Okfq/1pm7v+iqfX/+fn+///////9/f3B/v7+CQAAAAAAAAAAAAAAAP///wr9/f2d///////////////////////////////////////////9/f2Z/v7+CQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP7+/jL9/f2Z/f392/39/fr9/f36/f392v39/Zj///8wAAAAAAAAAAAAAAAAAAAAAPAPAADAAwAAgAEAAIABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIABAACAAQAAwAMAAPAPAAAoAAAAIAAAAEAAAAABACAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP7+/g3+/v5X/f39mf39/cj9/f3q/f39+f39/fn9/f3q/f39yP39/Zn+/v5W////DAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP7+/iT9/f2c/f399f/////////////////////////////////////////////////////9/f31/f39mv7+/iMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP7+/gn9/f2K/f39+////////////////////////////////////////////////////////////////////////////f39+v39/Yf///8IAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD+/v4k/f390v////////////////////////////////////////////////////////////////////////////////////////////////39/dD///8iAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA////MP39/er//////////////////////////+r05v+v16H/gsBs/2WxSf9Wqjj/Vqk3/2OwRv99vWX/pdKV/97u2P////////////////////////////39/ej+/v4vAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP7+/iT9/f3q/////////////////////+v15/+Pxnv/VKk2/1OoNP9TqDT/U6g0/1OoNP9TqDT/U6g0/1OoNP9TqDT/U6g0/36+Z//d7tf///////////////////////39/ej///8iAAAAAAAAAAAAAAAAAAAAAAAAAAD///8K/f390//////////////////////E4bn/XKw+/1OoNP9TqDT/U6g0/1OoNP9TqDT/U6g0/1OoNP9TqDT/U6g0/1OoNP9TqDT/U6g0/1apN/+x0pv///////////////////////39/dD///8IAAAAAAAAAAAAAAAAAAAAAP39/Yv/////////////////////sdij/1OoNP9TqDT/U6g0/1OoNP9TqDT/U6g0/1OoNP9TqDT/U6g0/1OoNP9TqDT/U6g0/1OoNP9TqDT/YKU1/8qOPv/5wZ////////////////////////39/YcAAAAAAAAAAAAAAAD+/v4l/f39+////////////////8Lgt/9TqDT/U6g0/1OoNP9TqDT/U6g0/1OoNP9utlT/n86N/7faqv+426v/pdKV/3u8ZP9UqDX/U6g0/3egN//jiUH/9IVC//SFQv/82MP//////////////////f39+v7+/iMAAAAAAAAAAP39/Z3////////////////q9Ob/W6w+/1OoNP9TqDT/U6g0/1OoNP9nskz/zOXC/////////////////////////////////+Dv2v+osWP/8YVC//SFQv/0hUL/9IVC//WQVP/++fb//////////////////f39mgAAAAD+/v4O/f399v///////////////4LHj/9TqDT/U6g0/1OoNP9TqDT/dblc//L58P/////////////////////////////////////////////8+v/3p3f/9IVC//SFQv/0hUL/9IVC//rIqf/////////////////9/f31////DP7+/ln////////////////f9v7/Cbz2/zOwhv9TqDT/U6g0/2KwRv/v9+z///////////////////////////////////////////////////////738//1kFT/9IVC//SFQv/0hUL/9plg///////////////////////+/v5W/f39nP///////////////4jf/f8FvPv/Bbz7/yG1s/9QqDz/vN2w//////////////////////////////////////////////////////////////////rHqP/0hUL/9IVC//SFQv/0hUL//vDn//////////////////39/Zn9/f3L////////////////R878/wW8+/8FvPv/Bbz7/y7C5P/7/fr//////////////////////////////////////////////////////////////////ere//SFQv/0hUL/9IVC//SFQv/718H//////////////////f39yP39/ez///////////////8cwvv/Bbz7/wW8+/8FvPv/WNL8///////////////////////////////////////0hUL/9IVC//SFQv/0hUL/9IVC//SFQv/0hUL/9IVC//SFQv/0hUL/9IVC//rIqv/////////////////9/f3q/f39+v///////////////we9+/8FvPv/Bbz7/wW8+/993P3///////////////////////////////////////SFQv/0hUL/9IVC//SFQv/0hUL/9IVC//SFQv/0hUL/9IVC//SFQv/0hUL/+cGf//////////////////39/fn9/f36////////////////B737/wW8+/8FvPv/Bbz7/33c/f//////////////////////////////////////9IVC//SFQv/0hUL/9IVC//SFQv/0hUL/9IVC//SFQv/0hUL/9IVC//SFQv/6xaX//////////////////f39+f39/e3///////////////8cwvv/Bbz7/wW8+/8FvPv/WdP8///////////////////////////////////////0hUL/9IVC//SFQv/0hUL/9IVC//SFQv/0hUL/9IVC//SFQv/0hUL/9IVC//vVv//////////////////9/f3q/f39y////////////////0bN/P8FvPv/Bbz7/wW8+/8hrvn/+/v///////////////////////////////////////////////////////////////////////////////////////////////////////////////////39/cj9/f2c////////////////ht/9/wW8+/8FvPv/FZP1/zRJ6/+zuPf//////////////////////////////////////////////////////////////////////////////////////////////////////////////////f39mf7+/lr////////////////d9v7/B7n7/yB38f81Q+r/NUPq/0hV7P/u8P3////////////////////////////////////////////////////////////////////////////////////////////////////////////+/v5X////D/39/ff///////////////9tkPT/NUPq/zVD6v81Q+r/NUPq/2Fs7//y8v7////////////////////////////////////////////09f7//////////////////////////////////////////////////f399f7+/g0AAAAA/f39n////////////////+Tm/P89Suv/NUPq/zVD6v81Q+r/NUPq/1Bc7f/IzPn/////////////////////////////////x8v5/0xY7P+MlPP////////////////////////////////////////////9/f2cAAAAAAAAAAD+/v4n/f39/P///////////////7W69/81Q+r/NUPq/zVD6v81Q+r/NUPq/zVD6v9ZZe7/k5v0/6609/+vtff/lJv0/1pm7v81Q+r/NUPq/zVD6v+GjvL//v7//////////////////////////////f39+/7+/iQAAAAAAAAAAAAAAAD9/f2N/////////////////////6Cn9f81Q+r/NUPq/zVD6v81Q+r/NUPq/zVD6v81Q+r/NUPq/zVD6v81Q+r/NUPq/zVD6v81Q+r/NUPq/zVD6v+BivL////////////////////////////9/f2KAAAAAAAAAAAAAAAAAAAAAP7+/gv9/f3V/////////////////////7W69/8+S+v/NUPq/zVD6v81Q+r/NUPq/zVD6v81Q+r/NUPq/zVD6v81Q+r/NUPq/zVD6v81Q+r/P0zr/7q/+P///////////////////////f390v7+/gkAAAAAAAAAAAAAAAAAAAAAAAAAAP7+/ib9/f3r/////////////////////+Xn/P94gfH/NkTq/zVD6v81Q+r/NUPq/zVD6v81Q+r/NUPq/zVD6v81Q+r/NkTq/3Z/8f/l5/z///////////////////////39/er+/v4kAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP7+/jL9/f3r///////////////////////////k5vz/nqX1/2p08P9IVez/OEbq/zdF6v9GU+z/aHLv/5qh9f/i5Pz////////////////////////////9/f3q////MAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP7+/ib9/f3V/////////////////////////////////////////////////////////////////////////////////////////////////f390v7+/iQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP///wr9/f2N/f39/P///////////////////////////////////////////////////////////////////////////f39+/39/Yv+/v4JAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD+/v4n/f39n/39/ff//////////////////////////////////////////////////////f399v39/Z3+/v4lAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/v7+Dv7+/lr9/f2c/f39y/39/e39/f36/f39+v39/ez9/f3L/f39nP7+/ln+/v4OAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP/AA///AAD//AAAP/gAAB/wAAAP4AAAB8AAAAPAAAADgAAAAYAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAABgAAAAcAAAAPAAAAD4AAAB/AAAA/4AAAf/AAAP/8AAP//wAP/"});
		A=this._this.sto.Groups.new({parents: ["toolbar_____"],title: "TEST-1",url:"http://tamere.fr/"});
		this._this.sto.Groups.link_icons();
		i--;
		test&=(this._this.sto.images[i].url==this._this.sto.images[A.image].url);
		this._this.tools.console("link_icons","a",test);}
		
		if(controle&0x1000){test=true;
		n=this._this.sto.Groups.get_default();
		test&=(typeof(n)=="object")&&(n.members.length==0);
		this._this.tools.console("get_default","a",test);}

		if(controle&0x2000){test=true;
		d=this._this.sto.Groups.to_frontend();
		test&=(d.nodes.length==(Object.keys(this._this.sto.groups).length));
		this._this.tools.console("to_frontend","a",test);}

		if(controle&0x4000){test=true;		
		/* 				A
		 *  	       / \
		 *            B   C
		 *           / \ 
		 *          F   D
		 */
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "TEST-1"});
		B = this._this.sto.Groups.new({parents: [A.id],title: "TEST-1-1"});
		s=(Object.keys(this._this.sto.groups).length);
		b=(Object.keys(this._this.sto.bookmarks).length);
		c=this._this.sto.Groups.copy_at(B,A);
		test&=((s+1)==(Object.keys(this._this.sto.groups).length));
		test&=((b+1)==(Object.keys(this._this.sto.bookmarks).length));
		/*                 toolbar_____
		                    |
							A
		                   /
		                  B
		                 / \
		                C   D 
		 */
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A1"});
		B = this._this.sto.Groups.new({parents: [A.id],title: "B1"});			
		C = this._this.sto.Groups.new({parents: [B.id],title: "C1"});
		//D = this.new({parents: [B.id],title: "D1"});
		this._this.sto.Groups.to_bookmarks();
		s=(Object.keys(this._this.sto.groups).length);
		b=(Object.keys(this._this.sto.bookmarks).length);
		this._this.sto.Groups.copy_at(B,A);
		test&=((s+2)==(Object.keys(this._this.sto.groups).length));
		test&=((b+2)==(Object.keys(this._this.sto.bookmarks).length));
		/*                 toolbar_____
		                    |
							A
		                   /
		                  B
		                 / \
		                C   D 
		 */
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A1"});
		B = this._this.sto.Groups.new({parents: [A.id],title: "B1"});			
		C = this._this.sto.Groups.new({parents: [B.id],title: "C1"});
		D = this._this.sto.Groups.new({parents: [B.id],title: "D1"});
		this._this.sto.Groups.to_bookmarks();
		s=(Object.keys(this._this.sto.groups).length);
		b=(Object.keys(this._this.sto.bookmarks).length);
		this._this.sto.Groups.copy_at(B,A);
		test&=((s+3)==(Object.keys(this._this.sto.groups).length));
		test&=((b+3)==(Object.keys(this._this.sto.bookmarks).length));
		/*
				A
				|  \
				B    F
			   / \
			  C   D
			   \ /
                E
        */
        A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
        B = this._this.sto.Groups.new({parents: [A.id],title: "B"});
        C = this._this.sto.Groups.new({parents: [B.id],title: "C"});
        D = this._this.sto.Groups.new({parents: [B.id],title: "D"});
        E = this._this.sto.Groups.new({parents: [C.id,D.id],title: "E"});
        F = this._this.sto.Groups.new({parents: [A.id],title: "F"});
        s=(Object.keys(this._this.sto.groups).length);
		b=(Object.keys(this._this.sto.bookmarks).length);
	    this._this.sto.Groups.copy_at(B,F);
	    test&=((s+4)==(Object.keys(this._this.sto.groups).length));
		test&=((b+5)==(Object.keys(this._this.sto.bookmarks).length));
		/*
				A
				|  \
				B    F
			   / \
			  C   D
			   \ /
                E
        */
	        A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
        B = this._this.sto.Groups.new({parents: [A.id],title: "B"});
        C = this._this.sto.Groups.new({parents: [B.id],title: "C"});
        D = this._this.sto.Groups.new({parents: [B.id],title: "D"});
        E = this._this.sto.Groups.new({parents: [C.id,D.id],title: "E"});
        F = this._this.sto.Groups.new({parents: [A.id],title: "F"});
        s=(Object.keys(this._this.sto.groups).length);
		b=(Object.keys(this._this.sto.bookmarks).length);
        this._this.sto.Groups.copy_at(E,F);
        test&=((s+1)==(Object.keys(this._this.sto.groups).length));
		test&=((b+1)==(Object.keys(this._this.sto.bookmarks).length));
		/*
				A
				|  \
				B    F
			   / \
			  C   D
			   \ /
                E
        */
	        A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
        B = this._this.sto.Groups.new({parents: [A.id],title: "B"});
        C = this._this.sto.Groups.new({parents: [B.id],title: "C"});
        D = this._this.sto.Groups.new({parents: [B.id],title: "D"});
        E = this._this.sto.Groups.new({parents: [C.id,D.id],title: "E"});
        F = this._this.sto.Groups.new({parents: [A.id],title: "F"});
        s=(Object.keys(this._this.sto.groups).length);
		b=(Object.keys(this._this.sto.bookmarks).length);
        this._this.sto.Groups.copy_at(F,E);
        test&=((s+1)==(Object.keys(this._this.sto.groups).length));
		test&=((b+2)==(Object.keys(this._this.sto.bookmarks).length));
		this._this.tools.console("copy_at","a",test);}

		if(controle&0x8000){test=true;
		await this._this.api.reset();		
		await this._this.api.reset(true);
		//                 toolbar_____
		//                    |
		//					  A
		//                   /
		//                  B
		//                 / \
		//                C   D 
		//
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
		B = this._this.sto.Groups.new({parents: [A.id],title: "B"});			
		//////console.warn("<===>");
		C = this._this.sto.Groups.new({parents: [B.id],title: "C"});
		D = this._this.sto.Groups.new({parents: [B.id],title: "D"});
		//////console.warn("<===>");
		await this._this.sto.BookmarkTreeNode.push();
		//////console.warn("<===>");
		await this._this.tools.sleep(1000);
		//////console.warn("<===>");
		t = this._this.sto.groups["toolbar_____"];
		u=(Object.keys(this._this.sto.groups)).length;
		//////console.warn("<===>");
		v=(Object.keys(this._this.sto.bookmarks)).length;
		w=t.children.length;
		await this._this.sto.Groups.move(C,t);
		//////console.warn("<===>");
		test&=(u==(Object.keys(this._this.sto.groups)).length);
		//////console.warn("<===>");
		test&=(v==(Object.keys(this._this.sto.bookmarks)).length);
		test&=(w+1==t.children.length);
		test&=(B.children.length==1);
		//////console.warn("<===>");
		//
		//		A
		//		|  \
		//		B    F
		//	   / \
		//	  C   D
		//	   \ /
        //      E
        //
	        A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
        B = this._this.sto.Groups.new({parents: [A.id],title: "B"});
        C = this._this.sto.Groups.new({parents: [B.id],title: "C"});
        D = this._this.sto.Groups.new({parents: [B.id],title: "D"});
        E = this._this.sto.Groups.new({parents: [C.id,D.id],title: "E"});
        F = this._this.sto.Groups.new({parents: [A.id],title: "F"});
        await this._this.sto.BookmarkTreeNode.push();
		await this._this.tools.sleep(1000);
		await this._this.sto.Groups.move(B,F);
		test&=(A.children.length==1)&&(A.parents.length==1)&&(A.members.length==1);
		test&=(F.children.length==1)&&(F.parents.length==1)&&(F.members.length==1);
		test&=(B.children.length==2)&&(B.parents.length==1)&&(B.members.length==1)&&(B.parents.includes(F.id));
		//////console.warn("<===>");
		//
		//		A
		//		|  \
		//		B    F
		//	   / \
		//	  C   D
		//	   \ /
        //      E
        // 		|
        //      G
        //      |
        //      H
        //
	        A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
        B = this._this.sto.Groups.new({parents: [A.id],title: "B"});
        C = this._this.sto.Groups.new({parents: [B.id],title: "C"});
        D = this._this.sto.Groups.new({parents: [B.id],title: "D"});
        E = this._this.sto.Groups.new({parents: [C.id,D.id],title: "E"});
        F = this._this.sto.Groups.new({parents: [A.id],title: "F"});
        G = this._this.sto.Groups.new({parents: [E.id],title: "G"});
        H = this._this.sto.Groups.new({parents: [G.id],title: "H"});
        await this._this.sto.BookmarkTreeNode.push();
		await this._this.tools.sleep(1000);
		await this._this.sto.Groups.move(B,F);
		test&=(A.children.length==1)&&(A.parents.length==1)&&(A.members.length==1);
		test&=(F.children.length==1)&&(F.parents.length==1)&&(F.members.length==1);
		test&=(B.children.length==2)&&(B.parents.length==1)&&(B.members.length==1)&&(B.parents.includes(F.id));
		//////console.warn("<===>");
		//
		//		A
		//		|  \
		//		B    F
		//	   / \
		//	  C   D
		//	   \ /
        //      E
        // 		|
        //      G
        //      |
        //      H
        //
	        A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
        B = this._this.sto.Groups.new({parents: [A.id],title: "B"});
        C = this._this.sto.Groups.new({parents: [B.id],title: "C"});
        D = this._this.sto.Groups.new({parents: [B.id],title: "D"});
        E = this._this.sto.Groups.new({parents: [C.id,D.id],title: "E"});
        F = this._this.sto.Groups.new({parents: [A.id],title: "F"});
        G = this._this.sto.Groups.new({parents: [E.id],title: "G"});
        H = this._this.sto.Groups.new({parents: [G.id],title: "H"});
        await this._this.sto.BookmarkTreeNode.push();
		await this._this.tools.sleep(1000);
		await this._this.sto.Groups.move(H,F);
		F=this._this.sto.groups[F.id];
		test&=(A.children.length==2)&&(A.parents.length==1)&&(A.members.length==1);
		test&=(H.children.length==0)&&(H.parents.length==1)&&(H.members.length==1);
		test&=(F.children.length==1)&&(F.parents.length==1)&&(F.members.length==1);
		//////console.warn("<===>");
		//
		//		A__
		//		|  \
		//		B   'F
		//	   / \
		//	  C   D
		//	   \ /
        //      E
        // 		|
        //      G
        //      |
        //      H
        //
	        A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
        B = this._this.sto.Groups.new({parents: [A.id],title: "B"});
        C = this._this.sto.Groups.new({parents: [B.id],title: "C"});
        D = this._this.sto.Groups.new({parents: [B.id],title: "D"});
        E = this._this.sto.Groups.new({parents: [C.id,D.id],title: "E"});
        F = this._this.sto.Groups.new({parents: [A.id],title: "F"});
        G = this._this.sto.Groups.new({parents: [E.id],title: "G"});
        H = this._this.sto.Groups.new({parents: [G.id],title: "H"});
        await this._this.sto.BookmarkTreeNode.push();
		await this._this.tools.sleep(1000);
		await this._this.sto.Groups.move(F,H);
		test&=(A.children.length==1)&&(A.parents.length==1)&&(A.members.length==1);
		test&=(H.children.length==1)&&(H.parents.length==1)&&(H.members.length==2);
		test&=(F.children.length==0)&&(F.parents.length==1)&&(F.members.length==2)&&(F.parents.includes(H.id));
		//
		//		A__
		//		|  \
		//		B   'F --- I
		//	   / \
		//	  C   D
		//	   \ /
        //      E
        // 		|
        //      G
        //      |
        //      H
        ///
	        A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
        B = this._this.sto.Groups.new({parents: [A.id],title: "B"});
        C = this._this.sto.Groups.new({parents: [B.id],title: "C"});
        D = this._this.sto.Groups.new({parents: [B.id],title: "D"});
        E = this._this.sto.Groups.new({parents: [C.id,D.id],title: "E"});
        F = this._this.sto.Groups.new({parents: [A.id],title: "F"});
        G = this._this.sto.Groups.new({parents: [E.id],title: "G"});
        H = this._this.sto.Groups.new({parents: [G.id],title: "H"});
        I = this._this.sto.Groups.new({parents: [F.id],title: "I"});
        await this._this.sto.BookmarkTreeNode.push();
		await this._this.tools.sleep(1000);
		await this._this.sto.Groups.move(F,H);
		await this._this.sto.BookmarkTreeNode.push();
		await this._this.tools.sleep(1000);
		test&=(A.children.length==1)&&(A.parents.length==1)&&(A.members.length==1);
		test&=(H.children.length==1)&&(H.parents.length==1)&&(H.members.length==2);
		test&=(F.children.length==1)&&(F.parents.length==1)&&(F.members.length==2)&&(F.parents.includes(H.id));
		test&=(I.children.length==0)&&(I.parents.length==1)&&(I.members.length==2);
		test&=(this._this.sto.bookmarks[I.members.first()].parent!=this._this.sto.bookmarks[I.members.last()].parent);
		//
		//		A
		//		|  .... G .... H
		//		B
		//	   / \
		//	  C   D
		//	   \ /
        //      E
        // 		|
        //      G
        //      |
        //      H
	        A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
        B = this._this.sto.Groups.new({parents: [A.id],title: "B"});
        C = this._this.sto.Groups.new({parents: [B.id],title: "C"});
        D = this._this.sto.Groups.new({parents: [B.id],title: "D"});
        E = this._this.sto.Groups.new({parents: [C.id,D.id],title: "E"});
        G = this._this.sto.Groups.new({parents: [E.id],title: "G"});
        H = this._this.sto.Groups.new({parents: [G.id],title: "H"});
        await this._this.sto.BookmarkTreeNode.push();
		await this._this.tools.sleep(1000);
		await this._this.sto.Groups.move(G,A);
		await this._this.sto.BookmarkTreeNode.push();
		await this._this.tools.sleep(1000);
		test&=(A.children.length==2)&&(A.parents.length==1)&&(A.members.length==1);
		test&=(B.children.length==2)&&(B.parents.length==1)&&(B.members.length==1);
		test&=(C.children.length==1)&&(C.parents.length==1)&&(C.members.length==1);
		test&=(D.children.length==1)&&(D.parents.length==1)&&(D.members.length==1);
		test&=(E.children.length==0)&&(E.parents.length==2)&&(E.members.length==2);
		test&=(G.children.length==1)&&(G.parents.length==1)&&(G.members.length==1);
		test&=(H.children.length==0)&&(H.parents.length==1)&&(H.members.length==1);
		this._this.tools.console("move","a",test);}

		if(controle&0x10000){test=true;
		//
		//		A__
		//		|  \
		//		B   'F
		//	   / \
		//	  C   D
		//	   \ /
        //      E
        // 		|
        //      G
        //      |
        //      H
        //
	    A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
        B = this._this.sto.Groups.new({parents: [A.id],title: "B"});
        C = this._this.sto.Groups.new({parents: [B.id],title: "C"});
        D = this._this.sto.Groups.new({parents: [B.id],title: "D"});
        E = this._this.sto.Groups.new({parents: [C.id,D.id],title: "E"});
        F = this._this.sto.Groups.new({parents: [A.id],title: "F"});
        G = this._this.sto.Groups.new({parents: [E.id],title: "G"});
        H = this._this.sto.Groups.new({parents: [G.id],title: "H"});
        this._this.sto.Groups.epaissir(A);
        test&=(A.children.length==2)&&(A.parents.length==1)&&(A.members.length==2);
        for(a of A.members) test&=(this._this.sto.bookmarks[a].children.length==2)&&(this._this.sto.bookmarks[a].parent!=undefined);
        test&=(B.children.length==2)&&(B.parents.length==1)&&(B.members.length==2);
        for(a of B.members) test&=(this._this.sto.bookmarks[a].children.length==2)&&(this._this.sto.bookmarks[a].parent!=undefined);
        test&=(C.children.length==1)&&(C.parents.length==1)&&(C.members.length==2);
        for(a of C.members) test&=(this._this.sto.bookmarks[a].children.length==1)&&(this._this.sto.bookmarks[a].parent!=undefined);
        test&=(D.children.length==1)&&(D.parents.length==1)&&(D.members.length==2);
        for(a of D.members) test&=(this._this.sto.bookmarks[a].children.length==1)&&(this._this.sto.bookmarks[a].parent!=undefined);
        test&=(E.children.length==1)&&(E.parents.length==2)&&(E.members.length==4);
        for(a of E.members) test&=(this._this.sto.bookmarks[a].children.length==1)&&(this._this.sto.bookmarks[a].parent!=undefined);
        test&=(F.children.length==0)&&(F.parents.length==1)&&(F.members.length==2);
        for(a of F.members) test&=(this._this.sto.bookmarks[a].children.length==0)&&(this._this.sto.bookmarks[a].parent!=undefined);
        test&=(G.children.length==1)&&(G.parents.length==1)&&(G.members.length==4);
        for(a of G.members) test&=(this._this.sto.bookmarks[a].children.length==1)&&(this._this.sto.bookmarks[a].parent!=undefined);
        test&=(H.children.length==0)&&(H.parents.length==1)&&(H.members.length==4);
        for(a of H.members) test&=(this._this.sto.bookmarks[a].children.length==0)&&(this._this.sto.bookmarks[a].parent!=undefined);
		this._this.tools.console("epaissir","a",test);}

		if(controle&0x20000){test=true;
		 /* 	    A
		 * 		   / \  
		 *        B   C 
		 *        |  / 
		 *        D-'  
		 *        '.....F
		 */ 
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
        B = this._this.sto.Groups.new({parents: [A.id],title: "B"});
        C = this._this.sto.Groups.new({parents: [A.id],title: "C"});
        D = this._this.sto.Groups.new({parents: [B.id,C.id],title: "D"});
        F = this._this.sto.Groups.new({parents: [D.id],title: "F"});
        this._this.sto.Groups.epaissir(A);
        await this._this.sto.Groups.affiner(A);
        test&=(A.members.length==1)&&(A.parents.length==1)&&(A.children.length==2);
        for(a of A.members) {
           test&=(this._this.sto.bookmarks[a].children.length==2);
           test&=(this._this.sto.bookmarks[a].parent!=undefined);}
        test&=(B.members.length==1)&&(B.parents.length==1)&&(B.children.length==1);
        for(a of B.members) {
           test&=(this._this.sto.bookmarks[a].children.length==1);
           test&=(this._this.sto.bookmarks[a].parent!=undefined);}
        test&=(C.members.length==1)&&(C.parents.length==1)&&(C.children.length==1);
        for(a of C.members) {
           test&=(this._this.sto.bookmarks[a].children.length==1);
           test&=(this._this.sto.bookmarks[a].parent!=undefined);}
        test&=(D.members.length==2)&&(D.parents.length==2)&&(D.children.length==1);
        for(a of D.members) {
           test&=(this._this.sto.bookmarks[a].children.length==1);
           test&=(this._this.sto.bookmarks[a].parent!=undefined);}
        test&=(F.members.length==2)&&(F.parents.length==1)&&(F.children.length==0);
        for(a of F.members) {
           test&=(this._this.sto.bookmarks[a].children.length==0);
           test&=(this._this.sto.bookmarks[a].parent!=undefined);}
        /* 		 	A____
		 * 		   / \   \
		 *        B   C   F
		 *       /|  / \
		 *      / D-'   \
		 *     ''........:.-P
		 */
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
        B = this._this.sto.Groups.new({parents: [A.id],title: "B"});
        C = this._this.sto.Groups.new({parents: [A.id],title: "C"});
        D = this._this.sto.Groups.new({parents: [B.id,C.id],title: "D"});
        F = this._this.sto.Groups.new({parents: [A.id],title: "F"});
        P = this._this.sto.Groups.new(D);
        this._this.sto.Groups.epaissir(A);this._this.sto.Groups.epaissir(A);
        await this._this.sto.Groups.affiner(A);await this._this.sto.Groups.affiner(A);
        test&=(P.members.length==2)&&(D.members.length==2);
        test&=(P.parents.length==2)&&(D.parents.length==2);
        test&=(P.children.length==0)&&(D.children.length==0);
        test&=(C.children.length==2)&&(B.children.length==2);
        for(a of D.members) test&=(this._this.sto.bookmarks[a].children.length==0);
        for(a of F.members) test&=(this._this.sto.bookmarks[a].children.length==0);
		this._this.tools.console("affiner","a",test);}
	this._this.tools.groupEnd();
}
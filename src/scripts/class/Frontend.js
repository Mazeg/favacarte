/*
    Copyright (C) 2020-present Aïdo WILHELM

    This file is part of favacarte.

    favacarte is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    favacarte is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with favacarte.  If not, see <https://www.gnu.org/licenses/>.
*/
"use strict";
/*
 * Interface de haut niveau d'interaction avec la favacarte
 * chaque action "Ajouter un noeud", "supprimer une edge"
 * va déclencher des changements dans les couches sous-jaccentes
 */
class Frontend {constructor(){
	this.api 												= {};
	this.handlers 											= {};
	this.appendNode 											= this.appendNode;
	this.delete												= this.delete;
	this.get_nearest_from_position							= this.get_nearest_from_position;
	// vérifié
	this.appendEdge 										= this.appendEdge;
	this.put_to_runtime										= this.put_to_runtime;
	this.coord_DOM2visjs									= this.coord_DOM2visjs;		
	this.coord_visjs2DOM									= this.coord_visjs2DOM;
	this.set_overlay 										= this.set_overlay;
	this.to_groups     										= this.to_groups;
	this.gaussian_compute 									= this.gaussian_compute;
	this.highlight_nearest 									= this.highlight_nearest;
	this.open_nearest_bookmark								= this.open_nearest_bookmark;
	this.normale_changed 									= this.normale_changed;
	this.api.click 											= this.api_click;
	this.handlers.doubleclickNode 							= this.handlers_doubleclickNode;
	this.handlers.clickNode 								= this.handlers_clickNode;
	this.handlers.hoverNode 								= this.handlers_hoverNode;
	this.regression 										= _frontend_regression;
}
	/*
	 * 	But: bouger à telle position de l'écran (en DOM)
	 */
	moveTo(domx=window.innerWidth/2,domy=window.innerHeight/2,scale=1)
	{
		this._this.frontend.vis.moveTo({position:this.coord_DOM2visjs(domx,domy),scale:scale});
	}
	// Retourne le groupe qui a été ajouté
	async appendNode(dom={x:window.innerWidth/2,y:window.innerHeight/2},details={type:"folder",title:undefined,estCache:undefined})
	{
		try{if(this._this.backend.storage.settings.backend.debug.frontend.appendNode) {
			this._this.tools.group("backend.debug.frontend.appendNode");
			this._this.tools.console("details=","d",details);
			this._this.tools.console("dom=","d",dom);}} catch(e) {}
		if(dom==undefined) dom={x:window.innerWidth/2,y:window.innerHeight/2};
		if(dom.y==undefined&&typeof(dom)=="object") {y=dom.y;x=dom.x;}
		let pid=undefined,visjscoo,url=undefined,title=details.title,rv,parents=[];
		// Choix du parentId
		// Solution n°1 : ajout à la racine
		// pid = this._this.sto.settings.frontend.interface.images.default.root.id;		
		// Solution n°2 : ajouter au noeud le plus proche, si c'est possible
		if(this._this.sto.settings.frontend.controller.Marquer_cette_page_auto_add)
		{
			pid = this.get_nearest_from_position(dom.x,dom.y,this._this.sto.settings.frontend.controller.deplacement.nearest_threshold.link_node,["bookmark"]);
			if(pid!=undefined)pid=pid.id;
		}
		if(details.type=="bookmark")
		{
			
			url=window.document.location.toString();
			if(title==""||title==undefined)
				title = window.document.title;
			if(title==""||title==undefined)
				title = window.document.location.toString();
		}
		else
			if(title==undefined||title=="")
				title=browser.i18n.getMessage("ctx_Nouveau_dossier");

		// Solution n°3 : ajouter un groupe vide
		try{if(this._this.backend.storage.settings.backend.debug.frontend.appendNode) {
			this._this.tools.console("pid=","d",pid);
			this._this.tools.console("title=","d",title);}} catch(e) {}
		// Ajout du group au position DOM: x,y
		visjscoo = this._this.f.coord_DOM2visjs(dom.x,dom.y);

		if(pid!=undefined)parents.push(pid);
		rv=this._this.sto.Groups.new(
			{
				parents:parents,
				url:url,
				title:title,
				type:details.type,
				coord:{x:visjscoo.x,y:visjscoo.y},
				estCache:false,depth:1,
			},
			undefined,undefined,undefined,undefined,details.estCache);		

		await this._this.sto.set();
		await this._this.com.envoi("bookmark_change");
		await this._this.frontend.put_to_runtime();
		try{if(this._this.backend.storage.settings.backend.debug.frontend.appendNode) {
			this._this.tools.console("rv=","d",rv);
			this._this.tools.groupEnd();}} catch(e) {}
		return rv;
	}
	async appendEdge(edge)
	{
		try{if(this._this.backend.storage.settings.backend.debug.frontend.appendEdge) {
			this._this.tools.group("backend.debug.frontend.appendEdge");
			this._this.tools.console("edge=","d",edge);}} catch(e) {}
		let eparent = edge.to, eenfant = edge.from, gparent = this._this.sto.groups[eparent], genfant = this._this.sto.groups[eenfant];
		let state       = undefined;

		let gedges = this._this.f.datasets.edges.get({
			filter: function (item) {
	   		return ((item.from==eparent&&item.to==eenfant)||(item.to==eparent&&item.from==eenfant));
	  		}
		});

		if(edge.from == edge.to || gedges.length > 1 )
		{
			this._this.frontend.datasets.edges.remove(edge.id);
			return; // désactiver le loopback
		}
		if( gparent.type == "bookmark" && genfant.type == "folder" )
		{
			eparent = edge.from;
			eenfant = edge.to;
		}
		else if( gparent.type == "folder" && genfant.type == "bookmark") 	// cas classique
		{
			// dans ce cas on laisse dérouler normalement
		}
		else if( gparent.type == "bookmark" && genfant.type == "bookmark") 	// cas non traité
		{
			this._this.frontend.datasets.edges.remove(edge.id);
			this._this.tools.console("links bookmark/bookmark : NOT IMPLEMENTED","e");
			return;
		}
		else if( gparent.type == "folder" && genfant.type == "folder") 		// cas classique
		{
			/*
				Implé actuelle: from-to : from est parent et to enfant
				Ajout d'une edge de C à B:
				Solution:
					si C est non rooted et B rooted ou inverse								: alors on inverse
					else
						que la depth de C est > depth B 	: alors on inverse
			*/
			if(!this._this.sto.Groups.is_rooted(gparent) && this._this.sto.Groups.is_rooted(genfant))
			{
				eparent = edge.from;
				eenfant = edge.to;
				state = "Created";
			}
			else if ((this._this.sto.Groups.is_rooted(gparent)&&this._this.sto.Groups.is_rooted(genfant)) || (!this._this.sto.Groups.is_rooted(gparent)&&!this._this.sto.Groups.is_rooted(genfant)))
			{
				// cmp sur la depth
				if( gparent.depth > genfant.depth )
				{
					eparent = edge.from;
					eenfant = edge.to;
				}
			}
			else if (this._this.sto.Groups.is_rooted(gparent)&&!this._this.sto.Groups.is_rooted(genfant))
			{
				state = "Created";
			}
		}
		gparent 	= this._this.sto.groups[eparent];
		genfant 	= this._this.sto.groups[eenfant];


		try{if(this._this.backend.storage.settings.backend.debug.frontend.appendEdge) {
			this._this.tools.console("gparent=","d",gparent);
			this._this.tools.console("genfant=","d",genfant);
			this._this.tools.console("eparent=","d",eparent);
			this._this.tools.console("eenfant=","d",eenfant);
			this._this.tools.console("state=","d",state);}} catch(e) {}

		/* Fonction d'enracinement
		 *
		 */
		// liaison du premier membre
		gparent.children.push(eenfant);
		genfant.parents.push(eparent);

		// ajout dans le dataset des edges (permet de contrer le bug graphique 38/)
		this._this.frontend.datasets.edges.add({id:eenfant+eparent,from:eenfant,to:eparent});

		// surtout propager state, on en a besoin par la suite
		// Probleme ici les groupes sous-jaccents peuvent être enracinés (et donc pas de propagation de la valeur Created)
		this._this.sto.Groups.propagate_attributes(gparent,genfant,state);
		// stockage des coordonnées et traduction vers l'interface interne
		this._this.f.to_groups();
		// application de la profondeur et de l'attribut enraciné
		await this._this.sto.set();
		await this._this.com.envoi("bookmark_change");
		try{if(this._this.backend.storage.settings.backend.debug.frontend.appendEdge) {
			this._this.tools.groupEnd();}} catch(e) {}
	}
	/*
	 * But: supprimer un element du graphe (noeud ou edge)
	 */
	async delete(ids,type="edge",details={})
	{
		try{if(this._this.backend.storage.settings.backend.debug.frontend.delete) {
			this._this.tools.group("backend.debug.frontend.delete");
			this._this.tools.console("ids=","d",ids);
			this._this.tools.console("type=","d",type);
			this._this.tools.console("details=","d",details);}} catch(e) {}
//////console.warn("===");
		if(!Array.isArray(ids))
		{
			if(typeof(ids)=="object")
				ids=[ids.id];
			else
				ids=[ids];
		}
//////console.warn("===");
		if(type=="edge")
		{
			for(let edgeId of ids)
			{
				let edge = this._this.frontend.datasets.edges.get(edgeId),from=edge.from,to=edge.to;
				if(edge==undefined)
					return;
				if(this._this.sto.groups[from].depth<this._this.sto.groups[to].depth)
				{
					from= edge.to;
					to  = edge.from;
				}
				// Supprimer l'edge de gparent à genfant
				// gparent <-- genfant
				let gparent=this._this.sto.groups[to],genfant=this._this.sto.groups[from];
				try{if(this._this.backend.storage.settings.backend.debug.frontend.delete) {
					this._this.tools.console("gparent=","d",gparent);
					this._this.tools.console("genfant=","d",genfant);}} catch(e) {}
				await this._this.sto.Groups.unlink(gparent,genfant,true);
			}
		}
		else if(type=="node")
		{
			await this._this.sto.Groups.delete(ids.slice(),details.recursive);
		}
//////console.warn("===");
		await this._this.frontend.put_to_runtime();
//////console.warn("===");
		await this._this.sto.set();
//////console.warn("===");
	//	//console.warn("this._this.sto.groups=",this._this.sto.groups);
		await this._this.com.envoi("bookmark_change");
		////console.warn("===");
		try{if(this._this.backend.storage.settings.backend.debug.frontend.delete) {
			this._this.tools.groupEnd();}} catch(e) {}
	}
	/*
	 * But: traduire les coordonnées sur un schéma DOM en coordonnées sur un schéma visjs
	 *  VISJSx,VISJSy : la view position
	 *  scale         : la view scale
	 *  DOMx,DOMy     : les coordonnées DOM de click
	 */
	coord_DOM2visjs(DOMx,DOMy,visjsnetwork=this._this.f.vis)
	{
		let gvp    = visjsnetwork.getViewPosition();
		let scale  = visjsnetwork.getScale();
		DOMx = DOMx - window.innerWidth/2;
	 	DOMy = DOMy - window.innerHeight/2;
	 	DOMx = DOMx/scale;
	 	DOMy = DOMy/scale;
	 	DOMx = DOMx + gvp.x;
	 	DOMy = DOMy + gvp.y;
	 	return {x: DOMx,y: DOMy};
	}
	coord_visjs2DOM(VISJSX,VISJSY,visjsnetwork=this._this.f.vis)
	{
		let gvp    = visjsnetwork.getViewPosition();
		let scale  = visjsnetwork.getScale();
		VISJSX 	= VISJSX - gvp.x;
		VISJSY 	= VISJSY - gvp.y;
		VISJSX 	= VISJSX*scale;
		VISJSY 	= VISJSY*scale;
		VISJSX 	= VISJSX + window.innerWidth/2;
	 	VISJSY 	= VISJSY + window.innerHeight/2;
	 	return {x: VISJSX,y: VISJSY};
	}
	/*
     * But: Ramener les coordonnées du Frontend dans Groups, puis ramener dans les bookmarks
	 */
	to_groups(visjsnetwork=this._this.f.vis)
	{
		try{if(this._this.backend.storage.settings.backend.debug.frontend.to_groups) {
			this._this.tools.group("backend.debug.frontend.to_groups");
			this._this.tools.console("visjsnetwork=","d",visjsnetwork);}} catch(e) {}
		if(visjsnetwork==null)
			return false;
		let key,noeud,pos,rv;
		this._this.sto.settings.temp.frontend.nodes.selected 	= visjsnetwork.getSelectedNodes();
		this._this.sto.settings.temp.frontend.position 			= visjsnetwork.getViewPosition();
		this._this.sto.settings.temp.frontend.scale    			= visjsnetwork.getScale();
		for(key of Object.keys(this._this.sto.groups))
		{
			noeud 										= this._this.sto.groups[key];
			pos  										= visjsnetwork.getPositions([key]);
			pos 											= pos[key];
			if(pos == ""||typeof(pos) == 'undefined') // la doc dit renvoi un objet vide
				pos={x:0,y:0};
			noeud.coord 					= {x:pos.x,y:pos.y};			
		}
		rv = this._this.backend.storage.Groups.to_bookmarks();
		try{if(this._this.backend.storage.settings.backend.debug.frontend.to_groups) {
			this._this.tools.console("rv=","d",rv);
			this._this.tools.groupEnd();}} catch(e) {}
		return rv;
	}
	/* But: créer un objet visNetwork et l'injecter dans l'interface utilisateur */
	async put_to_runtime()
	{
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.frontend.put_to_runtime) {
			this._this.tools.group("backend.storage.frontend.put_to_runtime");}} catch(e) {}
		for(;typeof(vis)=="undefined";) await this._this.tools.sleep(100);
		let data,_this,dts,to_remove,a,b,ii,i;
		this._this.sto.Groups.link_icons();
		data 									= this._this.sto.Groups.to_frontend();
		if(typeof(this._this.frontend.vis) == 'undefined')
		{
			this._this.frontend.datasets.nodes=new vis.DataSet(data.nodes);
			this._this.frontend.datasets.edges=new vis.DataSet(data.edges);

			this._this.frontend.vis  				= new vis.Network(this._this.html.noeud.visjs_noeud, {nodes:this._this.frontend.datasets.nodes,edges:this._this.frontend.datasets.edges}, {
				physics:this._this.sto.settings.frontend.interface.physics,
				interaction: {
					hover:  				true,
					tooltipDelay: 			this._this.sto.settings.frontend.interface.transition.tooltipDelay//,
			/*		keyboard:
					{
						enabled: 			true,
						speed:
						{
							x: 				10,
							y: 				10,
							zoom: 			0.02
						},
						bindToWindow: 		true
					},
					navigationButtons: 		true*/
				},
				edges: {
					selectionWidth: 		0,
					hoverWidth:     		0,
					chosen:         		false,
					color: 					'rgba('+this._this.sto.settings.frontend.interface.edges.color[0]+','+this._this.sto.settings.frontend.interface.edges.color[1]+','+this._this.sto.settings.frontend.interface.edges.color[2]+','+1+')',//"#0d0730",
					width: 					this._this.sto.settings.frontend.interface.edges.width,
					smooth: 				this._this.sto.settings.frontend.interface.edges.smooth
				},
				nodes: {
					borderWidth:   			0,
					font: {
						color:   			this._this.sto.settings.frontend.interface.font.color,
						bold:    			(this._this.sto.settings.frontend.interface.font.bold?{}:false)
					},
					brokenImage:  			this._this.sto.settings.frontend.interface.images.default.broken.image,
					shape:  				'image',
					chosen: {
						node: function(values, id, selected, hovering) {
							_this = this.body.container._this;
							values.shadow=			_this.sto.settings.frontend.interface.nodes.chosen.shadow.enabled;
							values.shadowColor=  	'rgba('+_this.sto.settings.frontend.interface.nodes.chosen.shadow.color[0]+','+_this.sto.settings.frontend.interface.nodes.chosen.shadow.color[1]+','+_this.sto.settings.frontend.interface.nodes.chosen.shadow.color[2]+','+_this.sto.settings.frontend.interface.nodes.chosen.shadow.color[3]+')';
							values.shadowSize=     	_this.sto.settings.frontend.interface.nodes.chosen.shadow.size;
							values.shadowX=        	_this.sto.settings.frontend.interface.nodes.chosen.shadow.offset.x;
							values.shadowY=        	_this.sto.settings.frontend.interface.nodes.chosen.shadow.offset.y;
							values.size=            _this.sto.settings.frontend.interface.nodes.chosen.size;
						}
					}
				}
			});
			/*
			 * Events qui se chargent de mettre la surcouche "lien"
			 */
			_this = this._this;
			this._this.frontend.vis.on("doubleClick",_this.f.handlers.doubleclickNode);
			this._this.frontend.vis.on("hoverNode",_this.f.handlers.hoverNode);
			this._this.frontend.vis.on("dragEnd",function(e) { _this.frontend.handlers.hoverNode({node: e.nodes[0],pointer:e.pointer},this,true); });
			this._this.frontend.vis.on("zoom",function(o) { _this.sto.settings.temp.frontend.scale = o.scale; });
		}
		else 
		{
			// Gérer la suppression de noeud
			dts = ["nodes","edges"];
			for(a = 0;a < dts.length;a++)
			{
				this._this.f.datasets[dts[a]].update(data[dts[a]]);
				if( this._this.f.datasets[dts[a]].length > data[dts[a]].length)
				{
					// procédure de suppression
					// NOK  : n*n
					// TODO : ranger cette merde : peu de favoris = OK
					to_remove = [];
					this._this.f.datasets[dts[a]].forEach(function(event, properties, senderId) {
						for(b of data[dts[a]])
						{
							if(b.id == properties)
								return;
						}
						to_remove.push(properties);
					});
					this._this.f.datasets[dts[a]].remove(to_remove);
					for(ii of to_remove)
					{
						while(1)
						{
							i = this._this.sto.settings.temp.frontend.nodes.selected.indexOf(ii);
							if(i==-1)
								break;
							this._this.sto.settings.temp.frontend.nodes.selected.splice(i,1);
						}
					}
				}
			}
		}
		// Bouger sans une transition
		this._this.frontend.vis.moveTo({
			position:{
				x: this._this.sto.settings.temp.frontend.position.x,
				y: this._this.sto.settings.temp.frontend.position.y
			},
			scale: this._this.sto.settings.temp.frontend.scale
		});
		this._this.f.vis.unselectAll();
		this._this.frontend.vis.selectNodes(this._this.sto.settings.temp.frontend.nodes.selected);
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.frontend.put_to_runtime) {
			this._this.tools.console("data","d",data);
			this._this.tools.console("sto.groups","d",this._this.sto.groups);
			this._this.tools.console("sto.bookmarks","d",this._this.sto.bookmarks);			
			this._this.tools.groupEnd(); }} catch(e) {}
	}
	/*
	 * But: récupérer le noeud le plus proche de w/2,h/2(DOM) et le retourner
	 * retourne undefined dans les cas ou on a pas de favoris ou bien si les favoris sont trop éloignés
	 */
	get_nearest_from_position(x=undefined,y=undefined,threshold=this._this.sto.settings.frontend.controller.deplacement.nearest_threshold.link_node,exclude_type=[])
	{
		try{if(this._this.backend.storage.settings.backend.debug.frontend.get_nearest_from_position) {
			this._this.tools.group("backend.debug.frontend.get_nearest_from_position");
			this._this.tools.console("threshold","d",threshold);
			this._this.tools.console("x=","d",x);
			this._this.tools.console("y=","d",y);}} catch(e) {}

  		let best = {score:0,id:undefined},score=0,score_x=0,score_y=0;
  		let node,node2,nodeDOM,nodeDOMcr={},noeud;
  		let screenDOM = {x: window.innerWidth,y:window.innerHeight};
  		let k,norme;
  		this._this.f.vis.storePositions();
//////////console.warn("=");
		for(k of Object.keys(this._this.sto.groups))
		{
//////////console.warn("=");
			node = this._this.f.datasets.nodes.get(k);
			node2 = this._this.f.vis.getPositions(k)[k];
			//console.warn("node=",node,",node2=",node2);
			noeud   = this._this.sto.groups[k];
//////////console.warn("=noeud=",noeud,"node=",node);
			if(exclude_type.includes(noeud.type)) continue;
//////////console.warn("=");
			nodeDOM = this._this.f.coord_visjs2DOM(node.x,node.y);
			try{if(this._this.backend.storage.settings.backend.debug.frontend.get_nearest_from_position) {
				this._this.tools.console("node title=","d",k);
				this._this.tools.console("nodeDOM=","d",nodeDOM);
				this._this.tools.console("node.hidden=","d",node.hidden);
				this._this.tools.console("screenDOM=","d",screenDOM);}} catch(e) {}

			// tester si on est en dehors de l'écran ou bien en noeud caché
			if(node==undefined||node.hidden||
			   nodeDOM==undefined||nodeDOM.x<0||nodeDOM.y<0||
			   nodeDOM.x>screenDOM.x||nodeDOM.y>screenDOM.y)
				continue;
//////////console.warn("=");
			// cas ou x, y sont définits :
			// 
			if(x!=undefined&&y!=undefined)
			{
				// ramener les points dans le graph avec 0,0 au centre
				nodeDOMcr.x=nodeDOM.x-x;
				nodeDOMcr.y=nodeDOM.y-y;			
			}
			// cas de base, le curseur est à 0,0 (soit screenDOM.x/2,screenDOM.y/2)
			// le but est de produire un autre cas
			else
			{
				// centrer
				nodeDOMcr.x=nodeDOM.x-screenDOM.x/2;
				nodeDOMcr.y=nodeDOM.y-screenDOM.y/2;
			}
//////////console.warn("=");
			// tester si on dépasse le cercle limite définit par l'utilisateur
			norme = Math.sqrt(nodeDOMcr.x * nodeDOMcr.x + nodeDOMcr.y * nodeDOMcr.y);
			try{if(this._this.backend.storage.settings.backend.debug.frontend.get_nearest_from_position) {
				this._this.tools.console("norme=","d",norme);
				this._this.tools.console("threshold=","d",threshold);}}catch(e){}
			if(norme > threshold)
				continue;
			// réduire
			nodeDOMcr.x/=screenDOM.x/2;
			nodeDOMcr.y/=screenDOM.y/2;
			score_x=this._this.f.normale_changed(nodeDOMcr.x);
			score_y=this._this.f.normale_changed(nodeDOMcr.y);
			score = score_y + score_x;
//////////console.warn("=");
			// ici il faut fixer une valeur de score minimale à atteindre pour qu'il y ai un plus proche
			if(score > best.score)
			{
				best.score=score;
				best.id=node.id;
			}
			try{if(this._this.backend.storage.settings.backend.debug.frontend.get_nearest_from_position) {
				this._this.tools.console("score_x=","d",score_x);
				this._this.tools.console("score_y=","d",score_y);
				this._this.tools.console("score=","d",score);}} catch(e) {}
		}
		try{if(this._this.backend.storage.settings.backend.debug.frontend.get_nearest_from_position) {
			this._this.tools.console("best=","d",best);
			if(best.id!=undefined) this._this.tools.console("best title=","d",this._this.sto.groups[best.id].title);
			this._this.tools.groupEnd();}} catch(e) {}
		if(best.id==undefined)
			return undefined;
		return best;
	}
   	/*
	 * But : mettre un lien sur le graph sous la forme d'un <a> cf 58 pour des détails sur les coords
	 */
	set_overlay(id,max_index=999999999999999999)
	{
		try{if(this._this.backend.storage.settings.backend.debug.frontend.set_overlay) {
			this._this.tools.group("frontend.set_overlay");
			this._this.tools.console("id=","d",id);
			this._this.tools.groupEnd();}} catch(e) {}

		let target_id  = this._this.html.noeud.visjs_noeud.id;
		let overlay_id = target_id+"_hover_overlay_"+id;
		let d = this._this.html.shadowRoot.getElementById(overlay_id);
		if(d!=undefined)
			d.parentNode.removeChild(d);

		if(id==undefined) return;

		let scale 	= this._this.f.vis.getScale();
		let pos 	= this._this.f.vis.getPositions(id)[id];
		let cc 		= this.coord_visjs2DOM(pos.x,pos.y);
		let width 	= this._this.f.vis.body.nodes[id].shape.width * scale;
		let height 	= this._this.f.vis.body.nodes[id].shape.height * scale;
		cc.x-=width/2;
		cc.y-=height/2;
		let target 						= this._this.html.shadowRoot.getElementById(target_id);
		let mine 						= document.createElement("a");
		mine.className              	= this._this.html.noeud.id + "_link";
		mine.href 						= this._this.sto.groups[id].url;
		mine.id 						= overlay_id;
		mine.style.position 			= "fixed";
		mine.style.top 					= cc.y+"px";
		mine.style.left 				= cc.x+"px";
		mine.style.opacity 				= 0;
		// Debug version
		try{if(this._this.backend.storage.settings.backend.debug.frontend.set_overlay)
		{
			mine.style.opacity 			= 1;
			mine.style.backgroundColor  = 'red';
		}} catch(e) {}

		mine.style.zIndex   			= max_index;
		mine.style.width    			= width+"px"; 
		mine.style.height   			= height+"px";
		target.parentNode.insertBefore(mine,target);


		let _this=this._this;
		mine.addEventListener("wheel",function(e){
			_this.f.set_overlay(_this.f.vis.getNodeAt({x:e.clientX,y:e.clientY}));});

		// on redirige les events sur la div de u dessous
		// cad le canvas visjs
		this._this.tools.redirect_events(mine,this._this.html.noeud.visjs_noeud.children[0].children[0]);

		mine.addEventListener("mouseleave",function(e){
			e.srcElement.parentNode.removeChild(e.srcElement);});
		mine.addEventListener("keypress",function(e){
			e.srcElement.parentNode.removeChild(e.srcElement);});
		mine.addEventListener("click",this._this.f.handlers.clickNode);
	}
	/* But: changer les valeurs Note, Nom et valider les changements
	 */
	api_click(evt,DOMx,DOMy=undefined)
	{
		let obj = {x:DOMx,y:DOMy};
		if(DOMy==undefined)
			obj=DOMx;
		let n = new MouseEvent(evt,obj);
		this._this.html.noeud.visjs_noeud.children[0].children[0].dispatchEvent(n);
	}
	/*
	 * But: 		Afficher en subbrillance le noeud actuellement sélectionné 
	 */
	async highlight_nearest()
	{
		this._this.f.vis.unselectAll();
		let m = this._this.f.get_nearest_from_position();
		if(m==undefined || m.id==undefined || m.id=="")
			return false;
	
		this._this.f.vis.selectNodes([m.id],false);
		let n = this._this.sto.groups[m.id],a;
		if(n.children!=undefined)
		{
			for(a=0;a<n.children.length;a++)
			{
				this._this.sto.groups[n.children[a]].estCache=false;
				this._this.frontend.datasets.nodes.update({id:n.children[a],hidden:false});
			}
		}
		// on doit set une option sur le noeud m.id et le marquer comme étant le plus proche
		return true;
	}
	/*
	 * But: ouvrir le lien du favori
	 * => ou bien dans une nouvelle fenetre, ou bien dans la meme => en fonction du param
	 * Feat: cas dans d'un dossier : on ouvre tout le monde dans des onglets !
	 */
	async open_nearest_bookmark()
	{
		let match = this._this.f.get_nearest_from_position();
		if(match==undefined || match.id==undefined || match.id=="")
			return false;

		let n = this._this.sto.groups[match.id];
		if(n.type == "folder")
		{
			this._this.api.redirection(n,undefined,this._this.sto.settings.frontend.controller.openFolderDepth);
		}
		else if(n.type=="bookmark")
		{
			if(n.url != undefined)
				this._this.api.redirection(n.url);
			else return false;
		} else return false;
		return true;
	}
	normale_changed(x)
	{
		return this._this.frontend.gaussian_compute(x,0.1);
	}
	gaussian_compute(x,coef)
	{
		return ( 1.0 / Math.sqrt(coef * Math.PI)) * Math.exp( (-1) * x*x);
	}
/*
	 * But : gérer le click utilisateur
	 *  cette méthode est asynchrone (éviter de geler l'iface graphique pdt le double click)
	 */
	//handlers_doubleclick
	async handlers_doubleclickNode(obj)
	{	
		let Fav_this = this.body.container._this;
		let noeud = Fav_this.sto.groups[obj.nodes[0]];
		if(noeud != null && noeud.url != undefined && noeud.type == "bookmark")
			Fav_this.api.redirection(noeud.url);
	}
	// handlers_click
	async handlers_clickNode(e)
	{
		e.stopImmediatePropagation();
		e.preventDefault();
		fav.api.redirection(this.href);
	}
	/*
	 *
	 * But : intercepter le passage au dessus du noeud
	 * Rq:  si deux noeuds ont le ID, le passage au dessus du noeud ne marchera pas
	 * 		solution : 	find_BookmarkTreeNode renvoie un tableau des noeuds à modifier => problèmes de performances
	 *					pour le moment, on reste comme ça étant donné les faibles chances de tomber sur ce cas (à part aux tests : due cp/coller)
	 * Rq: il faut déclencher ce handle sur un dragNode end également.
	 * handle_hoverNode
	 */
	async handlers_hoverNode(obj,this_scope=undefined,force_stock=false)
	{
		if(obj==undefined ||typeof(obj.node)=='Array')
			return;

		if(obj.event!=undefined && obj.event.buttons!=0) // Ignorer les hover events quand on fait un déplacement 
			return;

		let win_this;
		if(this == undefined)
			win_this = this_scope.body.container._this;
		else
		{
			if( this._this != undefined)
				win_this = this._this;
			else
				win_this = this.body.container._this;
		}
		
		let noeud = win_this.sto.groups[obj.node];
		let test = false,key;
		if(noeud != null)
		{
			if(noeud.type == "bookmark")
			{
				win_this.f.set_overlay(noeud.id);
			}
			if(noeud.depth >= win_this.sto.settings.frontend.interface.max_depth && noeud.children != undefined)
			{
				for(key of noeud.children)
				{
					if(win_this.sto.groups[key].estCache)
					{
						test = true;
						win_this.sto.groups[key].estCache = false;	
					}
				}
			}
		}
		if(test||force_stock)
		{
		    win_this.f.to_groups();
			await win_this.frontend.put_to_runtime();
			await win_this.sto.set();
		}
	}//

};
async function _frontend_regression(controle=0xFFFF,more_tests=false,debug=false,verb=false)
{
	let test=true,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,dconf,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z;
	this._this.tools.group("frontend");
		if(controle&0x1){test=true;
		this._this.f.vis.moveTo({position: {x:0,y:0},scale: 1});
		a=Object.keys(this._this.sto.groups).length;
		b=Object.keys(this._this.sto.bookmarks).length;
		await this._this.frontend.appendNode({x:0,y:0},{title:"L1",type:"folder"});
		test&=(a+1==Object.keys(this._this.sto.groups).length);
		test&=(b==Object.keys(this._this.sto.bookmarks).length);
		a=Object.keys(this._this.sto.groups).length;
		b=Object.keys(this._this.sto.bookmarks).length;
		await this._this.frontend.appendNode({x:window.innerWidth/2,y:0},{title:"L2",type:"folder"});
		await this._this.frontend.appendNode({x:0,y:window.innerHeight/2},{title:"L3",type:"folder"});
		test&=(a+2==Object.keys(this._this.sto.groups).length);
		test&=(b==Object.keys(this._this.sto.bookmarks).length);
		////////console.warn("2.test=",test);
		a=Object.keys(this._this.sto.groups).length;
		b=Object.keys(this._this.sto.bookmarks).length;
		g=await this._this.frontend.appendNode({x:0,y:window.innerHeight},{title:"L4",type:"folder"});
		h=await this._this.frontend.appendNode({x:10,y:window.innerHeight},{title:"L5",type:"bookmark"});
		test&=(a+2==Object.keys(this._this.sto.groups).length);
		////////console.warn("3.1.test=",test);
		test&=(b==Object.keys(this._this.sto.bookmarks).length);
		////////console.warn("3.2.test=",test);
		g=this._this.sto.groups[g.id];
		test&=(g.children.length==1)&&(h.parents.length==1);
		////////console.warn("3.3.test=",test);
		a=Object.keys(this._this.sto.groups).length; // Pour ce test, on suppose qu'un noeud est en (0,0)
		b=Object.keys(this._this.sto.bookmarks).length;
		await this._this.frontend.appendNode({x:window.innerWidth/2+10,y:window.innerHeight/2+10},{title:"L6",type:"folder"});
		await this._this.frontend.appendNode({x:window.innerWidth/2+20,y:window.innerHeight/2+20},{title:"L61",type:"bookmark"});
		test&=(a+2==Object.keys(this._this.sto.groups).length);
		test&=(b+2==Object.keys(this._this.sto.bookmarks).length);
		// Eventuellement: faire un héritage multiple (une fois que l'autre fonction sera prouvée)
		A=await this._this.frontend.appendNode({x:window.innerWidth/2,y:window.innerHeight/2-20},{title:"A",type:"folder",estCache:false});
		await this._this.frontend.appendNode({x:window.innerWidth/2,y:window.innerHeight/2-40},{title:"B",type:"folder",estCache:false});   			
		await this._this.frontend.appendNode({x:window.innerWidth/2,y:window.innerHeight/2-60},{title:"C",type:"folder",estCache:false});   			
		await this._this.frontend.appendNode({x:window.innerWidth/2,y:window.innerHeight/2-80},{title:"D",type:"folder",estCache:false});   			
		await this._this.frontend.appendNode({x:window.innerWidth/2,y:window.innerHeight/2-100},{title:"E",type:"folder",estCache:false});   			
		await this._this.frontend.appendNode({x:window.innerWidth/2,y:window.innerHeight/2-120},{title:"F",type:"folder",estCache:false});   			
		await this._this.frontend.appendNode({x:window.innerWidth/2,y:window.innerHeight/2-140},{title:"G",type:"folder",estCache:false});   			
		await this._this.frontend.appendNode({x:window.innerWidth/2,y:window.innerHeight/2-160},{title:"H",type:"folder",estCache:false});   			
		await this._this.frontend.appendNode({x:window.innerWidth/2-20,y:window.innerHeight/2-160},{title:"H",type:"folder",estCache:false});   			
		await this._this.frontend.appendNode({x:window.innerWidth/2-40,y:window.innerHeight/2-160},{title:"H",type:"folder",estCache:false});   			
		await this._this.frontend.appendNode({x:window.innerWidth/2-60,y:window.innerHeight/2-160},{title:"H",type:"folder",estCache:false});   			
		await this._this.frontend.appendNode({x:window.innerWidth/2-80,y:window.innerHeight/2-160},{title:"H",type:"folder",estCache:false});   			
		await this._this.frontend.appendNode({x:window.innerWidth/2-100,y:window.innerHeight/2-160},{title:"H",type:"folder",estCache:false});   			
		await this._this.frontend.appendNode({x:window.innerWidth/2-120,y:window.innerHeight/2-160},{title:"H",type:"folder",estCache:false});   			
		await this._this.frontend.appendNode({x:window.innerWidth/2-140,y:window.innerHeight/2-160},{title:"H",type:"folder",estCache:false});   			
		await this._this.frontend.appendNode({x:window.innerWidth/2-160,y:window.innerHeight/2-160},{title:"H",type:"folder",estCache:false});   			
		await this._this.frontend.appendNode({x:window.innerWidth/2-160,y:window.innerHeight/2-140},{title:"H",type:"folder",estCache:false});   			
		await this._this.frontend.appendNode({x:window.innerWidth/2-160,y:window.innerHeight/2-120},{title:"H",type:"folder",estCache:false});   			
		await this._this.frontend.delete([A.id],"node");
		this._this.tools.console("appendNode","a",test);}

		if(controle&0x4){test=true;
		await this._this.frontend.appendNode({x:window.innerWidth-50,y:window.innerHeight-50},{title:"L4",type:"folder"});
		await this._this.frontend.appendNode({x:window.innerWidth-20,y:window.innerHeight-20},{title:"L5",type:"bookmark"});
		a=this._this.frontend.get_nearest_from_position(window.innerWidth,window.innerHeight,50);
		test&=(a!=undefined)&&(a.id!=undefined)&&(this._this.sto.groups[a.id].title=="L5");
		a=this._this.frontend.get_nearest_from_position(window.innerWidth-60,window.innerHeight-60,50);
		test&=(a!=undefined)&&(a.id!=undefined)&&(this._this.sto.groups[a.id].title=="L4");
		this._this.tools.console("get_nearest_from_position","a",test);}

	//      
	//     			M1 == M2 
	//
	//
	// (à cause des blocages le test de reg échouera ici si on laisse le début) : les com.envoi ne seront pas effectués
	if(controle&0x2){test=true;
	/*await this._this.f.delete(this._this.sto.groups["toolbar_____"].children,"node",{recursive: true}); 
		c=Object.keys(this._this.sto.groups).length;
		z=this._this.sto.groups["toolbar_____"].children.length;
		a=await this._this.frontend.appendNode({x:window.innerWidth,y:0},{title:"M1",type:"folder"});
		b=await this._this.frontend.appendNode({x:window.innerWidth-50,y:0},{title:"M2",type:"folder"});
		await this._this.frontend.delete([a.id,b.id],"node");
		test&=(c==Object.keys(this._this.sto.groups).length);
		for(i=0;i<10;i++) {
   		c=Object.keys(this._this.sto.groups).length;
   		a=await this._this.frontend.appendNode({x:window.innerWidth,y:0},{title:"M1",type:"folder"});
   		b=await this._this.frontend.appendNode({x:window.innerWidth-50,y:0},{title:"M2",type:"folder"});
   		await this._this.frontend.delete([a.id,b.id],"node");
   		test&=(c==Object.keys(this._this.sto.groups).length);
   	}
		c=Object.keys(this._this.sto.groups).length;
		e=[];
		for(d=0;d<10;d++)
			e.push(await this._this.frontend.appendNode({x:window.innerWidth-50-5*d,y:0},{title:"M"+d,type:"folder"}));
		await this._this.frontend.delete(e,"node");
		test&=(c==Object.keys(this._this.sto.groups).length);
		test&=(z==this._this.sto.groups["toolbar_____"].children.length);
		if(more_tests) {
   		c=Object.keys(this._this.sto.groups).length;
   		e=[];
   		for(i=0;i<100;i++)
   		{
   			j=Math.random();
   			if(j>=.4&&j<=.6)j=.3;
   			k=Math.random();
   			if(k>=.4&&k<=.6)k=.3;
	   		e.push(await this._this.frontend.appendNode({x:window.innerWidth*j,y:window.innerHeight*k},{title:"NODE "+i,type:"folder"}));
   		}
	   	await this._this.frontend.delete(e,"node");
	   	test&=(c==Object.keys(this._this.sto.groups).length);
	}
	//
	// BOGO TEST !!!
	// But: reproduire le bug constaté
	//
	for(a=0;a<10;a++)
	{
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title:"A"});
		B = this._this.sto.Groups.new({parents: [A.id],title:"B"});
		C = this._this.sto.Groups.new({parents: [B.id],title:"C"});
		await this._this.sto.set();
		await this._this.com.envoi("bookmark_change");
		await this._this.tools.sleep(100);
		await this._this.frontend.delete(A.id,"node");
		await this._this.tools.sleep(100);
	}
	// validation du delete des edges
	//
	//
	//                A
	//                |
	// 				toolbar
	//            
	//             
	//
	//
	A = await this._this.frontend.appendNode({x:window.innerWidth/2,y:window.innerHeight/2-20},{title:"A",type:"folder"});
		B = await this._this.frontend.appendNode({x:window.innerWidth/2,y:window.innerHeight/2+20},{title:"B",type:"bookmark"});
	T = this._this.sto.groups["toolbar_____"];
		s = T.children.length;
		v = this._this.f.datasets.edges.getIds().length;
		w = A.parents.length;
		await this._this.frontend.delete("toolbar_____"+A.id,"edge");
		A=this._this.sto.groups[A.id];
		test&=(T.children.length+1==s)&&(v == 1+this._this.f.datasets.edges.getIds().length);
		test&=(w == A.parents.length+1);
		A = await this._this.frontend.appendNode({x:window.innerWidth/2+30,y:window.innerHeight/2},{title:"A",type:"folder"});
		B = await this._this.frontend.appendNode({x:window.innerWidth/2+60,y:window.innerHeight/2},{title:"B",type:"folder"});
		C = await this._this.frontend.appendNode({x:window.innerWidth/2+90,y:window.innerHeight/2},{title:"C",type:"bookmark"});
		v = this._this.f.datasets.edges.getIds().length;
		await this._this.frontend.delete("toolbar_____"+A.id,"edge");
		A=this._this.sto.groups[A.id];B=this._this.sto.groups[B.id];C=this._this.sto.groups[C.id];
		test&=(v == 1+this._this.f.datasets.edges.getIds().length);
		for(g of [A,B,C]) test&=(g.members.length==0);*/
		//	    C             F
	//	  /   \         /   \
	//	 A     D ----- E     H
	//	  \   /         \   /
	//	    B             G
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"},undefined,undefined,undefined,undefined,false);
	B = this._this.sto.Groups.new({parents: [A.id],title: "B"},undefined,undefined,undefined,undefined,false);
	C = this._this.sto.Groups.new({parents: [A.id],title: "C"},undefined,undefined,undefined,undefined,false);
	D = this._this.sto.Groups.new({parents: [B.id,C.id],title: "D"},undefined,undefined,undefined,undefined,false);
	E = this._this.sto.Groups.new({parents: [D.id],title: "E"},undefined,undefined,undefined,undefined,false);
	F = this._this.sto.Groups.new({parents: [E.id],title: "F"},undefined,undefined,undefined,undefined,false);
	G = this._this.sto.Groups.new({parents: [E.id],title: "G"},undefined,undefined,undefined,undefined,false);
	H = this._this.sto.Groups.new({parents: [G.id,F.id],title: "H"},undefined,undefined,undefined,undefined,false);
	await this._this.sto.set();
	await this._this.com.envoi("bookmark_change");
	await this._this.frontend.delete(D.id+E.id,"edge");
	await this._this.tools.sleep(1000);
	D=this._this.sto.groups[D.id];
	test&=(D.children.length==0);
		this._this.tools.console("delete","a",test);}

		if(controle&0x8){test=true;
		a=this.coord_DOM2visjs(0,0);
		b=this.coord_visjs2DOM(a.x,a.y);
		test&=0<=(b.x+b.y)<=2;
		this._this.tools.console("convertion visjs/DOM","a",test);}

		if(controle&0x10){test=true;
		await this._this.f.delete(this._this.sto.groups["toolbar_____"].children,"node",{recursive: true}); 
		this._this.sto.groups={"toolbar_____": this._this.sto.groups["toolbar_____"]};
		// validation du delete des edges
	//
	//
	// 		toolbar -- A -- B -- C
	//
	//
		A = await this._this.frontend.appendNode({x:window.innerWidth/2+30,y:window.innerHeight/2},{title:"A",type:"folder"});
		B = await this._this.frontend.appendNode({x:window.innerWidth/2+60,y:window.innerHeight/2},{title:"B",type:"folder"});
		C = await this._this.frontend.appendNode({x:window.innerWidth/2+90,y:window.innerHeight/2},{title:"C",type:"bookmark"});
		s = this._this.sto.groups["toolbar_____"].children.length;
		t = this._this.sto.bookmarks["toolbar_____"].children.length;
		await this._this.frontend.delete("toolbar_____"+A.id,"edge");
		A=this._this.sto.groups[A.id];B=this._this.sto.groups[B.id];C=this._this.sto.groups[C.id];
		await this._this.frontend.appendEdge({from: "toolbar_____",to:A.id});
		A=this._this.sto.groups[A.id];B=this._this.sto.groups[B.id];C=this._this.sto.groups[C.id];
		test&=(s==this._this.sto.groups["toolbar_____"].children.length);
		test&=(t==this._this.sto.bookmarks["toolbar_____"].children.length);
		this._this.tools.console("appendEdge","a",test);}

	this._this.tools.groupEnd();
}

/*
    Copyright (C) 2020-present Aïdo WILHELM

    This file is part of favacarte.

    favacarte is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    favacarte is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with favacarte.  If not, see <https://www.gnu.org/licenses/>.
*/
"use strict";
/*
 * visjs |===| Groups |===| Bookmarks |===| BookmarkTreeNode |===| firefox
 *                              *
 *
 *
 */
class Bookmarks{ constructor() {
	this.new 						= this.new;
	this.delete 					= this.delete;
	this.update_at 					= this.update_at;
	this.delete_at               	= this.delete_at;
	this.insert_at 					= this.insert_at;
	this.copy_branche_at 			= this.copy_branche_at;
	this.link_icons              	= this.link_icons;
	this.to_groups 					= this.to_groups;
	this.check_host 				= this.check_host;
	this.process_closure 			= this.process_closure;
	this.sync_icons 				= this.sync_icons;
	this.correct 					= this.correct;
	this.process_heritage_multiple 	= this.process_heritage_multiple;
	// vérifié
	this.regression 				= _backend_storage_Bookmarks_regression;
}
	/* But: produire le parcours en préfixe d'un arbre */
	process_parcours_prefixe(A,ensemble)
	{
		if(typeof(A)=="string") A = this._this.sto.bookmarks[A];
		let res = [A.id],c;
		if(A.children!=undefined)for(c of A.children)
		{
			if((Array.isArray(ensemble) && ensemble.includes(c)) || (typeof(ensemble)=="object" && (Object.keys(ensemble)).includes(c)))
				res=res.concat(this.process_parcours_prefixe(c,ensemble));
		}
		return res;
	}
	/* But: faire la cloture de A : A+ */
	process_closure(A,ensemble=this._this.sto.bookmarks,out_fmt="Array",direction=0)
	{
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Bookmarks.process_closure) {
			this._this.tools.group("backend.storage.Bookmarks.process_closure");
			this._this.tools.console("A=","d",A);
			this._this.tools.console("ensemble=","d",ensemble);
			this._this.tools.console("out_fmt=","d",out_fmt);
			this._this.tools.console("direction=","d",direction);
		}} catch(e) {}
		if(typeof(A)=="string")A=ensemble[A];
		if(A==undefined) return;
		let closure,B,pc;
		if(out_fmt=="Array")
		{
			closure = [];
			closure.push(A.id);
			if(direction==0)
				for(B of A.children)
				{
					pc = this.process_closure(ensemble[B],ensemble,out_fmt,direction);
					if(pc!=undefined) closure=closure.concat(pc);
				}
			if(direction==1)
			{
				pc = this.process_closure(ensemble[A.parent],ensemble,out_fmt,direction);
				if(pc!=undefined) closure=closure.concat(pc);
			}
		}
		if(out_fmt=="Object")
		{
			closure = {};
			closure[A.id]={};
			if(direction==0)
				for(B of A.children)
					closure = this._this.api.Object.copy(closure,this.process_closure(ensemble[B],ensemble,out_fmt,direction));
			if(direction==1)
				closure=this._this.api.Object.copy(closure,this.process_closure(ensemble[A.parent],ensemble,out_fmt,direction));
		}
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Bookmarks.process_closure) {
			this._this.tools.groupEnd();
		}} catch(e) {}
		return closure;
	}
	/* But: changer l'id d'un bookmark
	 */
	correct(newbookmark,newId)
	{
		if(newbookmark==undefined) return; 
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Bookmarks.correct) {
			this._this.tools.group("backend.storage.Bookmarks.correct");
			this._this.tools.console("newbookmark=","d",newbookmark);
			this._this.tools.console("newId=","d",newId);
		}} catch(e) {}
		if(typeof(newbookmark)=="string") newbookmark=this._this.sto.bookmarks[newbookmark];
		// Corriger bookmarks
		let p,i,e,benfant,gs,g;
		// correction du parent
		p = this._this.sto.bookmarks[newbookmark.parent];
		if(p!=undefined)
		{
			i=p.children.indexOf(newbookmark.id);
			if(i!=-1)
				p.children[i]=newId;
		}
		// correction des enfants
		for(e of newbookmark.children)
		{
			benfant = this._this.sto.bookmarks[e];
			if(benfant!=undefined&&benfant.parent==newbookmark.id)
				benfant.parent=newId;
		}
		gs=this._this.sto.Groups.get_groups_of(newbookmark.id);
		for(i of gs)
		{
			g = this._this.sto.groups[i];
			e = g.members.indexOf(newbookmark.id);
			if(e!=-1)
				g.members[e]=newId;
		}

		// changement de référence
		this._this.sto.bookmarks[newId]=this._this.sto.bookmarks[newbookmark.id];
		this._this.sto.bookmarks[newbookmark.id]=undefined;
		delete this._this.sto.bookmarks[newbookmark.id];
		this._this.sto.bookmarks[newId].id=newId;
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Bookmarks.correct) {
			this._this.tools.console("this._this.sto.bookmarks[newId]=","d",this._this.sto.bookmarks[newId]);
			this._this.tools.groupEnd();}} catch(e) {}

		return this._this.sto.bookmarks[newId];
	}
	/* But: supprimer un noeud avec target_id depuis une racine 'noeud'
	 * Rq:  on ne pourra pas supprimer la racine
	 * 		fonction de bas niveau
	 */
	//0x4
	delete_at(target_id,recursive=true) // vérifié
	{
		if(target_id=="object") target_id=target_id.id;
		let n = this._this.backend.storage.bookmarks[target_id];
		if(n==undefined)
			return false;
		let p = this._this.sto.bookmarks[n.parent];
		if( p != undefined )
		{
			p.children.splice(p.children.indexOf(n.id),1); // un seul parent pour l'instant
		}
		if( n.type == "folder" && n.children != undefined)
		{	
			if(recursive) while(n.children.length>0)
			{
				this.delete_at(n.children[0],recursive);
			}
		}
		return (delete this._this.backend.storage.bookmarks[target_id]);
	} // vérifié
		/*
	 * But: Inserer un noeud 'insert' dans les enfants d'un noeud 'target_id' à partir d'une racine noeud_n
	 * Rq:  insert et noeud_n doivent être au format bookmarks
	 */
	insert_at(target_id,insert) // vérifié
	{
		let cnoeud = this._this.sto.bookmarks[target_id];
		if(target_id==undefined||cnoeud==undefined||insert==undefined)
			return false;
		if(cnoeud.children==undefined)
			cnoeud.children=[];
		insert.parent 					= cnoeud.id;
		insert.depth 					= cnoeud.depth + 1;

		cnoeud.children.push(insert.id);
		this._this.sto.bookmarks[insert.id]	= insert;
		return true;
	} // vérifié
	/*
	 * But: pour les nouveaux favoris: mettre la bonne image au bon endroit
	 */
	link_icons()
	{
		let k,noeud,i;
		for(k of Object.keys(this._this.backend.storage.bookmarks))
		{
			noeud = this._this.backend.storage.bookmarks[k];
			if(noeud.image == undefined && noeud.url != undefined)
			{
				i = this._this.backend.storage.images.findIndex( image => image.url == (new URL(noeud.url)).host);
				if(i != -1) // dans ce context: null <=> undefined
					noeud.image = i;
			}
		}
	}	/* Construction initiake de l'arbre */
	/* But: récupérer les icones des liens qui n'en n'ont pas 
	 * Rq: cette fonction s'exécute de façcon asynchrone (y compris si vous faite un await) => due aux aléas du réseau
	 */
	async sync_icons(noeud=this._this.backend.storage.bookmarks)
	{
		let keys = Object.keys(this._this.sto.bookmarks),k,h,this_keeper,xhr;
		for(k of keys)
		{
			noeud = this._this.sto.bookmarks[k];
			if(noeud.url != undefined)
			{
				this_keeper = this._this;
				h = (new URL(noeud.url)).host;
				if(h != undefined && noeud.type=='bookmark' && this._this.backend.storage.images.find( image => image.url == h)==undefined)
				{
					xhr = new XMLHttpRequest();
					xhr.addEventListener("load",function()
					{
						var reader = new FileReader();
					    reader.onloadend=async function()
					    {
					    	if(this.result!=undefined&&this.result!=""&&this.status==200)
					    	{
						    	this_keeper.sto.images.push({url:h,image:this.result});
						    	noeud.image=this_keeper.sto.images.length-1;
						    	this_keeper.sto.set();
						    }
					    }
					    reader.readAsDataURL(this.response);
					});
					xhr.open('GET', "https://"+h+"/favicon.ico");
					xhr.responseType = 'blob';
					xhr.send();
				}
			}
		}
	}
	/* But: vérifier qu'il ne manque pas d'icones */
	// Attention cette fonction est appellée dans un contexte différent
	async sync_icons_worker()
	{
		while(true)
		{

			await this._this.sto.Bookmarks.sync_icons();
			await this._this.tools.sleep(this._this.backend.storage.settings.backend.workers.sync_icons); // toutes les heures, on regarde s'il manque des icones
		}
	}
	/* But: traduire la structure bookmarks en groups */
	to_groups(obj=this._this.sto.bookmarks)
	{	
		let keys = [];
		if( typeof(obj) == "object" )
		{
			if(Array.isArray(obj))
				keys = obj;
			else
				keys = Object.keys(obj);
		}
		else
			keys = [obj];

		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Bookmarks.to_groups) {
			this._this.tools.group("backend.storage.Bookmarks.to_groups");
			this._this.tools.console("keys=","d",keys);
			this._this.tools.console("obj=","d",obj);
			this._this.tools.groupEnd(); }} catch(e) {}


		let n,coord,parents,group,gparent,groupsOfN,k;
		for(k of keys)
		{

			n = this._this.sto.bookmarks[k];
			// si un bookmark a déjà un group, inutile de créer un nouveau
	/*		groupsOfN=this._this.sto.Groups.get_groups_of(n);
			if(!(groupsOfN==undefined||groupsOfN==[]))
				continue;
*/

			// Si je ne fait partie d'aucun group, alors, on créer un group associé avec ce bookmark



			parents = [n.parent];

			if( this._this.backend.storage.settings.frontend.interface.images.default.root.id == k)
				parents=[];
			if(n.coord!=undefined)
				coord = n.coord;
			group = {
				children: n.children.slice(), // tres mauvais
				parents : parents,
				members : [k],
				id 		: k,
				title   : n.title,
				url     : n.url,
				image   : n.image,
				date    : n.date,
				type    : n.type,
				coord 	: coord,
				estCache: n.estCache,
				depth   : n.depth,
				note    : n.note
			};

			this._this.sto.groups[group.id]=group;
			// Application de la correction:
			gparent=this._this.sto.groups[n.parent];

			if(gparent!=undefined&&Array.isArray(gparent.children)&&!gparent.children.includes(k))
				gparent.children.push(k); 
		}

	}
	/* 
	 * But: Changer les informations à un bookmarks donné
	 */
	//0x2
	update_at(target_id,insert,set_flag=true)
	{
		//////////////console.warn("1/2 est sensé être à false ("+target_id+")set_flag=",set_flag);
		let n = this._this.backend.storage.bookmarks[target_id];
		if(n==undefined||insert==undefined)
			return false;

		let keys=["url","image","title","date","type","note"];
		let test=false,k;
		for(k of keys)
		{
			if(insert[k]==undefined) continue;
			if(n[k]!=insert[k]) test = true;
			n[k] = insert[k];
		}
		if(set_flag && test)
		{
			//////////console.warn("(update_at)I set the flag on ",n.state);
			n.state="Changed";
		}
	} // vérifié
	/*
	 * But: Trouver un lien qui a un hote potentiel
	 * on va regarder si l'host donné peut renseigner mon tableau
	 * (navigation classique)
	 * Rq: il ne pourra le renseigner que pour un seul domain (contrainte qu'on a imposé)
	 */ 
	check_host(host)
	{
		let k,noeud;
		for(k of Object.keys(this._this.backend.storage.bookmarks))
		{
			noeud = this._this.backend.storage.bookmarks[k];
			if(noeud.type=="bookmark")
			{
				if(host==(new URL(noeud.url)).host)
					return noeud;
			}
		}
		return null;
	} // vérifié
	//0x1
	new(from,
	   parentId=undefined,forceId=undefined,set_flags=true,from_default={
	   children:[],date:this._this.tools.get_date(),type:"folder",
	   coord:{x: 0,y: 0},estCache: false,depth:1,note:""})
	{
		if(typeof(from)=="string") from=this._this.sto.bookmarks[from];
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Bookmarks.new)	{
			this._this.tools.group("backend.storage.Bookmarks.new");
			this._this.tools.console("from=","d",from);
			this._this.tools.console("parentId=","d",parentId);
			this._this.tools.console("forceId=","d",forceId);}} catch(e) {}

		from = this._this.api.Object.copy(from_default,from);
		let id 		= this._this.tools.gen_bid();
		if(forceId!=undefined) id = forceId;
		let b  		= this._this.sto.bookmarks[id] = {};
		b.id 		= id;
		b.parent 	= (parentId==undefined)?(from.parent==undefined)?(from.parents==undefined)?undefined:from.parents[0]:from.parent:parentId;
		b.url 		= from.url;
		b.image 	= from.image;
		b.title 	= from.title;
		b.date 		= from.date;
		b.type 		= from.type;
		b.coord 	= from.coord;
		b.estCache 	= from.estCache;
		b.depth 	= from.depth;
		b.note 		= from.note;
		b.children  = [];
		if(set_flags)
			b.state     = "Created"; // indiquer au démon qu'il doit procéder
		if(parentId!=undefined)
		{
			if(this._this.sto.bookmarks[parentId].children==undefined)
				this._this.sto.bookmarks[parentId].children=[];
			this._this.sto.bookmarks[parentId].children.push(id);
		}
		let p = this._this.sto.bookmarks[b.parent];
		if(p!=undefined && p.children.includes(b.id)==false)
			p.children.push(b.id);

		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Bookmarks.new)	{
			this._this.tools.console("b=","d",b);
			this._this.tools.groupEnd();}} catch(e) {}
		return b;
	}
	/* But: copier une partie de l'arbre bookmarks a un autre endroit
	 * rq:  cette copie n'entraine pas de changements graphiques
	 */
	//0x20
	copy_branche_at(from,at,forceId=undefined,first=true)
	{
		if(typeof(at)=="string") at=this._this.sto.bookmarks[at];
		if(typeof(from)=="string") from=this._this.sto.groups[from];

		let g;
		if(!this._this.sto.Groups.isGroups(from))
		{
			for(g of this._this.sto.Groups.get_groups_of(from))
			{
				this.copy_branche_at(g,at,forceId,first);
				forceId=undefined;							// éviter de reprendre le même id pour plusieurs bookmarks
			}
			return;
		}
		let b,childs,i,m;

		if(at.children==undefined)
			at.children=[];

		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Bookmarks.copy_branche_at) {
			if(first)
				this._this.tools.group("backend.storage.Bookmarks.copy_branche_at");
			this._this.tools.console("from=","d",from);
			this._this.tools.console("at=","d",at);
			this._this.tools.console("copy:Bookmarks: "+from.id+"("+b.id+")"+" -> "+at.id);
			this._this.tools.console("                "+from.title+" -> "+at.title);
			this._this.tools.console("            adding "+b.id+" to children of "+at.id); }} catch(e) {}

		b = this.new(from,at.id,forceId);
		if(!at.children.includes(b.id))
			at.children.push(b.id); // remonter des ids dans le parent

		// pour tous les ids, on ajoute un membre
		childs=[];
		from.members.push(b.id);
		if(from.children!=undefined) childs = from.children;

		for(i of childs)
			this.copy_branche_at(this._this.sto.groups[i],b,undefined,false);
	
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Bookmarks.copy_branche_at) {
			this._this.tools.console("Bookmarks créer: b=","d",b);
			this._this.tools.console("this._this.sto.bookmarks[b.id]=","d",this._this.sto.bookmarks[b.id]);
			this._this.tools.console("at=","d",at);
			this._this.tools.console("from=","d",from);
			if(first)
				this._this.tools.groupEnd();}} catch(e) {}
	}

	async delete(noeud,recursive=false,set_flag=true,remonterLesEnfants=true,first=true,commit_later=false)
	{
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Bookmarks.delete)
		{
			this._this.tools.group("backend.storage.Bookmarks.delete");
			this._this.tools.console("noeud=","d",noeud);
			this._this.tools.console("recursive=","d",recursive);
			this._this.tools.console("first=","d",first);
			this._this.tools.console("commit_later=","d",commit_later);
		}} catch(e) {}
////////console.warn("a");
		if(typeof(noeud)=="string")
			noeud=this._this.sto.bookmarks[noeud];
		if(noeud==undefined) return false;
		if(noeud.children==undefined) noeud.children=[];
////////console.warn("b");
		let c,child,p,i,g,group,childs;
		// pour tous les enfants, on remplace mon id par celui du parent
		// en plus de cela, il faut mettre un attribut moved sur tous les enfants
		if(remonterLesEnfants)
			for(c of noeud.children)
			{
				child = this._this.sto.bookmarks[c];
				child.parent = noeud.parent;
				child.state = "Moved";
			} 
		if(first)
		{
			p = this._this.sto.bookmarks[noeud.parent];
			if(p!=undefined)
			{
				i = p.children.indexOf(noeud.id);
				if(i != -1) p.children.splice(i,1);
				if(remonterLesEnfants)
					p.children=p.children.concat(noeud.children);
			}
		}
		if(set_flag)
			noeud.state="Removed";
		

		// On enleve ce bookmark de tous les groups auquels il est associé
		for(g of this._this.sto.Groups.get_groups_of(noeud.id))
		{
			group = this._this.sto.groups[g];
			i = group.members.indexOf(noeud.id);
			if(i!=-1) group.members.splice(i,1);
		}

		childs = noeud.children.slice();
		if(recursive)for(c of childs)
			await this.delete(this._this.sto.bookmarks[c],recursive,set_flag,remonterLesEnfants,false,commit_later);

		if(!commit_later && first)
		{
			await this._this.sto.set();
			await this._this.com.envoi("bookmark_change");
		}
			
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Bookmarks.delete){this._this.tools.groupEnd();}}catch(e){}
	}
	/*
	 * But: traduire les héritage multiple au format interne Bookmarks
	 */
	// cette fonction retourne si il n'y a pas plusieurs parents, hors, il y a aussi héritage multiple
	// la véritée est : some des membres des g.parents <= 1
	process_heritage_multiple(g)
	{
		// condition pour qu'un noeud soit soumis à héritage multiple
		if(g.parents==undefined)
			return false;

	
		let members = g.members.slice(),parent,p,test,member;
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Bookmarks.process_heritage_multiple) {
			this._this.tools.group(".process_heritage_multiple");
			this._this.tools.console("g=","d",g);
			this._this.tools.group("for all parents of g for all members of parents");}} catch(e) {}

		// Il faut étendre cela à :
		// pour tous les parents, pour tous les membres
		/* 		p1   p2  p3
		 *        \  |  /
		 *         \ | /
		 *          \|/
		 *           |
		 *           g(m1,m2,m3)
		 *
		 *
		 */
		for(parent of g.parents)
		{
			for(p of this._this.sto.groups[parent].members)
			{
				try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Bookmarks.process_heritage_multiple) {
					this._this.tools.console("g.parents=","d",g.parents);
					this._this.tools.console("p=","d",p);
					this._this.tools.console("members=","d",members);}} catch(e) {}

				test = false;
				// Vérifier que le parent actuel a bien comme enfant un membre de g
				for(member of members)
				{
					if(this._this.sto.bookmarks[p].children.includes(member)) // et que le parent n'a pas d'enfants
					{
						test = true;
						members.splice(members.indexOf(member),1);
						break;
					}
				}
				// si le parent actuel n'a pas d'enfant dans g, alors il faut en créer un

				
				// le groupe ne contient pas le favoris
				// on doit copier dans le parent
				// principe de la récursion:
				// pour tous les enfants
				// si le favoris est un dossier alors il faudra le faire de maniere récursive
				if(!test)
				{
					try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Bookmarks.process_heritage_multiple) {
						this._this.tools.console("Heritage multiple pour groupe g et p=","d",p);
						this._this.tools.console("this._this.sto.bookmarks[g.id]=","d",this._this.sto.bookmarks[g.id]);
						this._this.tools.console("this._this.sto.bookmarks[p]=","d",this._this.sto.bookmarks[p]);}} catch(e) {}

					this.copy_branche_at(g,this._this.sto.bookmarks[p]);

				}
			}
		}
		try{if(this._this.backend.storage.settings.backend.debug.backend.storage.Bookmarks.process_heritage_multiple) {
			this._this.tools.groupEnd();
			this._this.tools.groupEnd();}} catch(e) {}
		return true;
	}
	
};
async function _backend_storage_Bookmarks_regression(controle=0xFFFF,more_tests=false,debug=false,verb=false)
{
	let gsz,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,test,g1,g2,g3,h1,h2,h3,h4,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,tests=[],id,bsz;
	this._this.tools.group("Bookmarks");
		if(controle&0x1){test=true;
		gsz = Object.keys(this._this.sto.bookmarks).length;
		b =  this._this.sto.Bookmarks.new({note: "ok",type:"folder"});
		test&=(Object.keys(this._this.sto.bookmarks).length==gsz+1)&&b.note=="ok"&&b.type=="folder";
		a =  this._this.sto.Bookmarks.new({note: "ok",title: "ok",type:"folder",depth: 10},b.id);
		test&=a.note=="ok"&&a.title=="ok"&&a.type=="folder"&&a.state=="Created"&&a.depth==10&&a.parent==b.id&&b.children.includes(a.id);
		A =  this._this.sto.Bookmarks.new({parent: "toolbar_____",title: "A",note: "ok",type:"folder"});
		B =  this._this.sto.Bookmarks.new({parent: A.id,title: "B",note: "ok",type:"folder"});
		C =  this._this.sto.Bookmarks.new({parent: B.id,title: "C",note: "ok",type:"folder"});
		test&=(B.parent==A.id)&&(A.children.length==1)&&(B.children.length==1)&&(C.children.length==0);
		test&=(C.note=="ok");
		bsz=(Object.keys(this._this.sto.groups)).length;
		g = this._this.sto.Groups.new({title: "ok",url: "http://ok.google/",note:"ok",parents: ["toolbar_____"]});
		test&=(bsz+1==(Object.keys(this._this.sto.groups).length))&&(g.title=="ok");
		bsz=(Object.keys(this._this.sto.bookmarks)).length;
		b =  this._this.sto.Bookmarks.new(g);
		c =  this._this.sto.Bookmarks.new(g);
		test&=(bsz+2==(Object.keys(this._this.sto.bookmarks).length));
		test&=(b.title==g.title)&&(b.url==g.url)&&(c.title==b.title);
		this._this.tools.console("new","a",test);}

		if(controle&0x2){test=true;
		A =  this._this.sto.Bookmarks.new({parent: "toolbar_____",title: "A",note: "Aok",type:"folder"});
		B =  this._this.sto.Bookmarks.new({parent: A.id,title: "B",note: "Bok",type:"folder"});
		C =  this._this.sto.Bookmarks.new({parent: B.id,title: "C",note: "Cok",type:"folder"});
		 this._this.sto.Bookmarks.update_at(A.id,{title:B.title,url:B.url,note:B.note},true);
		test&=(A.title==B.title)&&(A.url==B.url)&&(A.note==B.note)&&(A.state=="Changed");
		this._this.tools.console("update_at","a",test);}

		if(controle&0x4){test=true;
		A =  this._this.sto.Bookmarks.new({parent: "toolbar_____",title: "A",note: "Aok",type:"folder"});
		B =  this._this.sto.Bookmarks.new({parent: A.id,title: "B",note: "Bok",type:"folder"});
		C =  this._this.sto.Bookmarks.new({parent: B.id,title: "C",note: "Cok",type:"folder"});
		bsz = (Object.keys(this._this.sto.bookmarks).length);
		 this._this.sto.Bookmarks.delete_at(A.id);
		test&=(Object.keys(this._this.sto.bookmarks).length)==bsz-3;
		A =  this._this.sto.Bookmarks.new({parent: "toolbar_____",title: "A",note: "Aok",type:"folder"});
		B =  this._this.sto.Bookmarks.new({parent: A.id,title: "B",note: "Bok",type:"folder"});
		C =  this._this.sto.Bookmarks.new({parent: A.id,title: "C",note: "Cok",type:"folder"});
		bsz = (Object.keys(this._this.sto.bookmarks).length);
		 this._this.sto.Bookmarks.delete_at(A.id);
		test&=(Object.keys(this._this.sto.bookmarks).length)==bsz-3;
		this._this.tools.console("delete_at","a",test);}

		if(controle&0x8){test=true;
		/*               A
		 *               |
		 *               B
		 *             / | \
		 *            E  C  D
		 */
		A =  this._this.sto.Bookmarks.new({parent: "toolbar_____",title: "A",note: "Aok",type:"folder"});
		B =  this._this.sto.Bookmarks.new({parent: A.id,title: "B",note: "Bok",type:"folder"});
		C =  this._this.sto.Bookmarks.new({parent: B.id,title: "C",note: "Cok",type:"folder"});
		D =  this._this.sto.Bookmarks.new({parent: B.id,title: "D",note: "Cok",type:"folder"});
		E =  this._this.sto.Bookmarks.new({parent: B.id,title: "E",note: "Cok",type:"folder"});
		 this._this.sto.Bookmarks.delete(E);
		test&=(B.children.length==2)&&(B.parent==A.id);
		this._this.tools.console("delete","a",test);}

		if(controle&0x10){test=true;
		A =  this._this.sto.Bookmarks.new({parent: "toolbar_____",title: "A",note: "Aok",type:"folder"});
		for(d=0;d<10;d++)
			 this._this.sto.Bookmarks.insert_at(A.id,A);
		test&=(A.children.length==10);
		for(d=0;d<10;d++)
			test&=(this._this.sto.bookmarks[A.children[d]].parent==A.id);
		this._this.tools.console("insert_at","a",test);}

		if(controle&0x20){test=true;
		//       A
		//      / \
		//     B   C
		//    /
		//   D
		//  /
		// E
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
		B = this._this.sto.Groups.new({parents: [A.id],title: "B"});
		C = this._this.sto.Groups.new({parents: [A.id],title: "C"});
		D = this._this.sto.Groups.new({parents: [B.id],title: "D"});
		E = this._this.sto.Groups.new({parents: [D.id],title: "E"});
		this._this.sto.Groups.to_bookmarks();
		s=(Object.keys(this._this.sto.bookmarks)).length;
		 this._this.sto.Bookmarks.copy_branche_at(D,C.members.first());
		test&=(s+2==(Object.keys(this._this.sto.bookmarks)).length);
		c=this._this.sto.bookmarks[C.members.first()];
		test&=(c.children.length==1);
		//       A -----
		//      / \     \
		//     B   C     E
		//      \ /
		//       D
			//       |
			//       F
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
		B = this._this.sto.Groups.new({parents: [A.id],title: "B"});
		C = this._this.sto.Groups.new({parents: [A.id],title: "C"});
		D = this._this.sto.Groups.new({parents: [B.id,C.id],title: "D"});
		E = this._this.sto.Groups.new({parents: [A.id],title: "E"});
		F = this._this.sto.Groups.new({parents: [D.id],title: "F"});
		this._this.sto.Groups.to_bookmarks();
		s=(Object.keys(this._this.sto.bookmarks)).length;
		 this._this.sto.Bookmarks.copy_branche_at(D,E.members.first());
		test&=(s+2==(Object.keys(this._this.sto.bookmarks)).length);
		e=this._this.sto.bookmarks[E.members.first()];
		test&=(e.children.length==1);
//			console.warn("test=",test);
//			return;
		// 				A
		//  	       / \
		//            B   C
		//           / \ /  \
		//          F   D    E
		//              |
		//              Z
		//              |
		//              X
		//             / \     X
	    //            R   S
		//             \ /    
		//              T    T  T
		//
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
		B = this._this.sto.Groups.new({parents: [A.id],title: "B"});	
		C = this._this.sto.Groups.new({parents: [A.id],title: "C"});		
		F = this._this.sto.Groups.new({parents: [B.id],title: "F"});
		D = this._this.sto.Groups.new({parents: [B.id,C.id],title: "D"});
		E = this._this.sto.Groups.new({parents: [C.id],title: "E"});	
		Z = this._this.sto.Groups.new({parents: [D.id],title: "Z"});	
		X = this._this.sto.Groups.new({parents: [Z.id],title: "X"});
		R = this._this.sto.Groups.new({parents: [X.id],title: "R"});
		S = this._this.sto.Groups.new({parents: [X.id],title: "S"});
		T = this._this.sto.Groups.new({parents: [R.id,S.id],title: "T"});
		this._this.sto.Groups.to_bookmarks();
		 this._this.sto.Bookmarks.copy_branche_at(D,E.members[0]);
		test&=(this._this.sto.bookmarks[E.members[0]].children.length==1);
		this._this.tools.console("copy_branche_at","a",test);}

		if(controle&0x40){test=true;
		i=this._this.sto.images.push({url: "tamere.fr",image:"data:image/x-icon;base64,AAABAAIAEBAAAAEAIABoBAAAJgAAACAgAAABACAAqBAAAI4EAAAoAAAAEAAAACAAAAABACAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP///zD9/f2W/f392P39/fn9/f35/f391/39/ZT+/v4uAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/v7+Cf39/Zn///////////////////////////////////////////39/ZX///8IAAAAAAAAAAAAAAAA/v7+Cf39/cH/////+v35/7TZp/92ul3/WKs6/1iqOv9yuFn/rNWd//j79v///////f39v////wgAAAAAAAAAAP39/Zn/////7PXp/3G3WP9TqDT/U6g0/1OoNP9TqDT/U6g0/1OoNP+Or1j//vDo///////9/f2VAAAAAP///zD/////+vz5/3G3V/9TqDT/WKo6/6LQkf/U6cz/1urO/6rUm/+Zo0r/8IZB//adZ////v7///////7+/i79/f2Y/////4nWzf9Lqkj/Vqo4/9Xqzv///////////////////////ebY//SHRv/0hUL//NjD///////9/f2U/f392v////8sxPH/Ebzt/43RsP/////////////////////////////////4roL/9IVC//i1jf///////f391/39/fr/////Cr37/wW8+/+16/7/////////////////9IVC//SFQv/0hUL/9IVC//SFQv/3pnX///////39/fn9/f36/////wu++/8FvPv/tuz+//////////////////SFQv/0hUL/9IVC//SFQv/0hUL/96p7///////9/f35/f392/////81yfz/CrL5/2uk9v///////////////////////////////////////////////////////f392P39/Zn/////ks/7/zdS7P84Rur/0NT6///////////////////////9/f////////////////////////39/Zb+/v4y//////n5/v9WYu3/NUPq/ztJ6/+VnPT/z9L6/9HU+v+WnfT/Ul7t/+Hj/P////////////////////8wAAAAAP39/Z3/////6Or9/1hj7v81Q+r/NUPq/zVD6v81Q+r/NUPq/zVD6v9sdvD////////////9/f2YAAAAAAAAAAD///8K/f39w//////5+f7/paz2/11p7v88Suv/Okfq/1pm7v+iqfX/+fn+///////9/f3B/v7+CQAAAAAAAAAAAAAAAP///wr9/f2d///////////////////////////////////////////9/f2Z/v7+CQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP7+/jL9/f2Z/f392/39/fr9/f36/f392v39/Zj///8wAAAAAAAAAAAAAAAAAAAAAPAPAADAAwAAgAEAAIABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIABAACAAQAAwAMAAPAPAAAoAAAAIAAAAEAAAAABACAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP7+/g3+/v5X/f39mf39/cj9/f3q/f39+f39/fn9/f3q/f39yP39/Zn+/v5W////DAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP7+/iT9/f2c/f399f/////////////////////////////////////////////////////9/f31/f39mv7+/iMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP7+/gn9/f2K/f39+////////////////////////////////////////////////////////////////////////////f39+v39/Yf///8IAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD+/v4k/f390v////////////////////////////////////////////////////////////////////////////////////////////////39/dD///8iAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA////MP39/er//////////////////////////+r05v+v16H/gsBs/2WxSf9Wqjj/Vqk3/2OwRv99vWX/pdKV/97u2P////////////////////////////39/ej+/v4vAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP7+/iT9/f3q/////////////////////+v15/+Pxnv/VKk2/1OoNP9TqDT/U6g0/1OoNP9TqDT/U6g0/1OoNP9TqDT/U6g0/36+Z//d7tf///////////////////////39/ej///8iAAAAAAAAAAAAAAAAAAAAAAAAAAD///8K/f390//////////////////////E4bn/XKw+/1OoNP9TqDT/U6g0/1OoNP9TqDT/U6g0/1OoNP9TqDT/U6g0/1OoNP9TqDT/U6g0/1apN/+x0pv///////////////////////39/dD///8IAAAAAAAAAAAAAAAAAAAAAP39/Yv/////////////////////sdij/1OoNP9TqDT/U6g0/1OoNP9TqDT/U6g0/1OoNP9TqDT/U6g0/1OoNP9TqDT/U6g0/1OoNP9TqDT/YKU1/8qOPv/5wZ////////////////////////39/YcAAAAAAAAAAAAAAAD+/v4l/f39+////////////////8Lgt/9TqDT/U6g0/1OoNP9TqDT/U6g0/1OoNP9utlT/n86N/7faqv+426v/pdKV/3u8ZP9UqDX/U6g0/3egN//jiUH/9IVC//SFQv/82MP//////////////////f39+v7+/iMAAAAAAAAAAP39/Z3////////////////q9Ob/W6w+/1OoNP9TqDT/U6g0/1OoNP9nskz/zOXC/////////////////////////////////+Dv2v+osWP/8YVC//SFQv/0hUL/9IVC//WQVP/++fb//////////////////f39mgAAAAD+/v4O/f399v///////////////4LHj/9TqDT/U6g0/1OoNP9TqDT/dblc//L58P/////////////////////////////////////////////8+v/3p3f/9IVC//SFQv/0hUL/9IVC//rIqf/////////////////9/f31////DP7+/ln////////////////f9v7/Cbz2/zOwhv9TqDT/U6g0/2KwRv/v9+z///////////////////////////////////////////////////////738//1kFT/9IVC//SFQv/0hUL/9plg///////////////////////+/v5W/f39nP///////////////4jf/f8FvPv/Bbz7/yG1s/9QqDz/vN2w//////////////////////////////////////////////////////////////////rHqP/0hUL/9IVC//SFQv/0hUL//vDn//////////////////39/Zn9/f3L////////////////R878/wW8+/8FvPv/Bbz7/y7C5P/7/fr//////////////////////////////////////////////////////////////////ere//SFQv/0hUL/9IVC//SFQv/718H//////////////////f39yP39/ez///////////////8cwvv/Bbz7/wW8+/8FvPv/WNL8///////////////////////////////////////0hUL/9IVC//SFQv/0hUL/9IVC//SFQv/0hUL/9IVC//SFQv/0hUL/9IVC//rIqv/////////////////9/f3q/f39+v///////////////we9+/8FvPv/Bbz7/wW8+/993P3///////////////////////////////////////SFQv/0hUL/9IVC//SFQv/0hUL/9IVC//SFQv/0hUL/9IVC//SFQv/0hUL/+cGf//////////////////39/fn9/f36////////////////B737/wW8+/8FvPv/Bbz7/33c/f//////////////////////////////////////9IVC//SFQv/0hUL/9IVC//SFQv/0hUL/9IVC//SFQv/0hUL/9IVC//SFQv/6xaX//////////////////f39+f39/e3///////////////8cwvv/Bbz7/wW8+/8FvPv/WdP8///////////////////////////////////////0hUL/9IVC//SFQv/0hUL/9IVC//SFQv/0hUL/9IVC//SFQv/0hUL/9IVC//vVv//////////////////9/f3q/f39y////////////////0bN/P8FvPv/Bbz7/wW8+/8hrvn/+/v///////////////////////////////////////////////////////////////////////////////////////////////////////////////////39/cj9/f2c////////////////ht/9/wW8+/8FvPv/FZP1/zRJ6/+zuPf//////////////////////////////////////////////////////////////////////////////////////////////////////////////////f39mf7+/lr////////////////d9v7/B7n7/yB38f81Q+r/NUPq/0hV7P/u8P3////////////////////////////////////////////////////////////////////////////////////////////////////////////+/v5X////D/39/ff///////////////9tkPT/NUPq/zVD6v81Q+r/NUPq/2Fs7//y8v7////////////////////////////////////////////09f7//////////////////////////////////////////////////f399f7+/g0AAAAA/f39n////////////////+Tm/P89Suv/NUPq/zVD6v81Q+r/NUPq/1Bc7f/IzPn/////////////////////////////////x8v5/0xY7P+MlPP////////////////////////////////////////////9/f2cAAAAAAAAAAD+/v4n/f39/P///////////////7W69/81Q+r/NUPq/zVD6v81Q+r/NUPq/zVD6v9ZZe7/k5v0/6609/+vtff/lJv0/1pm7v81Q+r/NUPq/zVD6v+GjvL//v7//////////////////////////////f39+/7+/iQAAAAAAAAAAAAAAAD9/f2N/////////////////////6Cn9f81Q+r/NUPq/zVD6v81Q+r/NUPq/zVD6v81Q+r/NUPq/zVD6v81Q+r/NUPq/zVD6v81Q+r/NUPq/zVD6v+BivL////////////////////////////9/f2KAAAAAAAAAAAAAAAAAAAAAP7+/gv9/f3V/////////////////////7W69/8+S+v/NUPq/zVD6v81Q+r/NUPq/zVD6v81Q+r/NUPq/zVD6v81Q+r/NUPq/zVD6v81Q+r/P0zr/7q/+P///////////////////////f390v7+/gkAAAAAAAAAAAAAAAAAAAAAAAAAAP7+/ib9/f3r/////////////////////+Xn/P94gfH/NkTq/zVD6v81Q+r/NUPq/zVD6v81Q+r/NUPq/zVD6v81Q+r/NkTq/3Z/8f/l5/z///////////////////////39/er+/v4kAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP7+/jL9/f3r///////////////////////////k5vz/nqX1/2p08P9IVez/OEbq/zdF6v9GU+z/aHLv/5qh9f/i5Pz////////////////////////////9/f3q////MAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP7+/ib9/f3V/////////////////////////////////////////////////////////////////////////////////////////////////f390v7+/iQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP///wr9/f2N/f39/P///////////////////////////////////////////////////////////////////////////f39+/39/Yv+/v4JAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD+/v4n/f39n/39/ff//////////////////////////////////////////////////////f399v39/Z3+/v4lAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/v7+Dv7+/lr9/f2c/f39y/39/e39/f36/f39+v39/ez9/f3L/f39nP7+/ln+/v4OAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP/AA///AAD//AAAP/gAAB/wAAAP4AAAB8AAAAPAAAADgAAAAYAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAABgAAAAcAAAAPAAAAD4AAAB/AAAA/4AAAf/AAAP/8AAP//wAP/"});
		A= this._this.sto.Bookmarks.new({parents: ["toolbar_____"],title: "TEST-1",url:"http://tamere.fr/"});
		 this._this.sto.Bookmarks.link_icons();
		i--;
		test&=(this._this.sto.images[i].url==this._this.sto.images[A.image].url);
		this._this.tools.console("link_icons","a",test);}

		if(controle&0x80){test=true;
	/*	b=this._this.sto.images.length;
		i=this._this.sto.images.findIndex( o => o.url=="www.google.fr" );
		if(i==-1)b++;
		A= this._this.sto.Bookmarks.new({parents: ["toolbar_____"],title: "TEST-N",url:"http://www.google.fr/"});
		await  this._this.sto.Bookmarks.sync_icons();
		await this._this.tools.sleep(1000);
		s=this._this.sto.images.length;
		test&=(s==b);*/
		this._this.tools.console("sync_icons","a",test);}
		
		if(controle&0x100){test=true;
		A= this._this.sto.Bookmarks.new({parent: "toolbar_____",title:"A"});
		s=(Object.keys(this._this.sto.groups)).length;
		 this._this.sto.Bookmarks.to_groups(A.id);
		test&=(s+1==(Object.keys(this._this.sto.groups)).length);
		A= this._this.sto.Bookmarks.new({parent: "toolbar_____",title:"A"});
		B= this._this.sto.Bookmarks.new({parent: A.id,title:"B"});
		s=(Object.keys(this._this.sto.groups)).length;
		 this._this.sto.Bookmarks.to_groups([A.id,B.id]);
		test&=(s+2==(Object.keys(this._this.sto.groups)).length);
		this._this.tools.console("to_groups","a",test);}

		if(controle&0x200){test=true;
		A= this._this.sto.Bookmarks.new({parent: "toolbar_____",title:"A",url:"http://tame.re/ok.html",type:"bookmark"});
		a= this._this.sto.Bookmarks.check_host((new URL(A.url)).host);
		test&=(a.id==A.id);
		this._this.tools.console("check_host","a",test);}

		if(controle&0x400){test=true;
		/* 		A         F
		 *	   / \        |
		 *    B   C       G
		 *     \          | \
		 *      D         H  I
		 *      |
		 *      E 
		 *
		 */
		A= this._this.sto.Bookmarks.new({parent: "toolbar_____",title:"A"});
		B= this._this.sto.Bookmarks.new({parent:A.id,title:"B"});
		C= this._this.sto.Bookmarks.new({parent:A.id,title:"C"});
		D= this._this.sto.Bookmarks.new({parent:B.id,title:"D"});
		E= this._this.sto.Bookmarks.new({parent:D.id,title:"E"});
		F= this._this.sto.Bookmarks.new({parent:"toolbar_____",title:"F"});
		G= this._this.sto.Bookmarks.new({parent:F.id,title:"G"});
		H= this._this.sto.Bookmarks.new({parent:G.id,title:"H"});
		I= this._this.sto.Bookmarks.new({parent:G.id,title:"I"});
		test&=( this._this.sto.Bookmarks.process_closure(A)).length==5;
		test&=( this._this.sto.Bookmarks.process_closure(G)).length==3;
		test&=( this._this.sto.Bookmarks.process_closure(C)).length==1;
		a= this._this.sto.Bookmarks.process_closure(A,this._this.sto.bookmarks,"Object",0);
		test&=(typeof(a)=="object")&&(a[A.id]!=undefined)&&(a[B.id]!=undefined)&&(a[C.id]!=undefined)&&(a[D.id]!=undefined)&&(a[E.id]!=undefined);
		test&=(a[F.id]==undefined)&&(a[G.id]==undefined)&&(a[H.id]==undefined)&&(a[I.id]==undefined);
		a= this._this.sto.Bookmarks.process_closure(E,this._this.sto.bookmarks,"Object",1);
		test&=(typeof(a)=="object")&&(a[A.id]!=undefined)&&(a[B.id]!=undefined)&&(a[C.id]==undefined)&&(a[D.id]!=undefined)&&(a[E.id]!=undefined);
		test&=(a[F.id]==undefined)&&(a[G.id]==undefined)&&(a[H.id]==undefined)&&(a[I.id]==undefined);
		this._this.tools.console("process_closure","a",test);}

		if(controle&0x800){test=true;
		A = this._this.sto.Groups.new({parents: ["toolbar_____"],title: "A"});
		B = this._this.sto.Groups.new({parents: [A.id],title: "B"});	
		C = this._this.sto.Groups.new({parents: [B.id],title: "C"});
		 this._this.sto.Bookmarks.correct(B.members[0],"test");
		test&=(B.members[0]=="test")&&(this._this.sto.bookmarks["test"]!=undefined);
		test&=(this._this.sto.bookmarks[A.members[0]].children.includes("test"))&&(this._this.sto.bookmarks[C.members[0]].parent=="test");
		this._this.tools.console("correct","a",test);}

		if(controle&0x1000){test=true;
		//this._this.tools.parsePref("backend.debug.backend.storage.Bookmarks.process_heritage_multiple=boolean=true");
		/*      A
		 *     / \     
		 *    B   C
		 *       /
		 *      D
		 *
		 */
		A=this._this.sto.Groups.new({parents:["toolbar_____"],title:"A"});
		B=this._this.sto.Groups.new({parents:[A.id],title:"B"});
		C=this._this.sto.Groups.new({parents:[A.id],title:"C"});
		D=this._this.sto.Groups.new({parents:[C.id],title:"D"});
		D.parents.push(B.id);
		s=(Object.keys(this._this.sto.bookmarks)).length;
		 this._this.sto.Bookmarks.process_heritage_multiple(D);
		test&=(Object.keys(this._this.sto.bookmarks)).length==s+1;
	//	this._this.tools.parsePref("backend.debug.backend.storage.Bookmarks.process_heritage_multiple=boolean=false");
		this._this.tools.console("process_heritage_multiple","a",test);}

		if(controle&0x2000){test=true;
		//      A
		//     / \
		//    B   C
		//    |
		//    D
		//    |
		//    E
		A= this._this.sto.Bookmarks.new({parent: "toolbar_____",title:"A"});
		B= this._this.sto.Bookmarks.new({parent:A.id,title:"B"});
		C= this._this.sto.Bookmarks.new({parent:A.id,title:"C"});
		D= this._this.sto.Bookmarks.new({parent:B.id,title:"D"});
		E= this._this.sto.Bookmarks.new({parent:D.id,title:"E"});
		a= this._this.sto.Bookmarks.process_parcours_prefixe(A.id,this._this.sto.bookmarks);
		test&=(a.length==5)&&(a[0]==[A.id]&&a[1]==B.id&&a[2]==D.id&&a[3]==E.id&&a[4]==C.id);
		this._this.tools.console("process_parcours_prefixe","a",test);}

	this._this.tools.groupEnd();
}
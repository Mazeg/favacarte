/*
    Copyright (C) 2020-present Aïdo WILHELM

    This file is part of favacarte.

    favacarte is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    favacarte is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with favacarte.  If not, see <https://www.gnu.org/licenses/>.
*/
/*
 * Outils d'affichage et de gestion
 */
 class Tools{ constructor(from_str="cs") {
 	this.get_date=this.get_date;
 	this.console=this.console;
 	this.gen_bid=this.gen_bid;
 	this.group=this.group;
 	this.groupEnd=this.groupEnd;
 	this.javascript_patch=this.javascript_patch;
 	this.lock_api=this.lock_api;
 	this.parsePref=this.parsePref;
 	this.sleep=this.sleep;
 	this.host_permission=this.host_permission;
 	this.redirect_events=this.redirect_events;
 	this.from_str=from_str;
 	this.regression=_backend_tools_regression;
}
	/*
	 * But: rediriger les events destinés à la surcouche html vers le canvas
	 *
	 */
	redirect_events(div,dest)
	{
		div.addEventListener("mousemove",function(e) {
			e.preventDefault();
			let n = new MouseEvent("mousemove",e);
			dest.dispatchEvent(n);
		});
		div.addEventListener("contextmenu",function(e) {
			e.preventDefault();
			let n = new MouseEvent("contextmenu",e);
			dest.dispatchEvent(n);
		});
		div.addEventListener("pointerdown",function(e) {
			e.preventDefault();
			let n = new PointerEvent("pointerdown",e);
			dest.dispatchEvent(n);
		});
		div.addEventListener("wheel",function(e) {
			e.preventDefault();
			let n = new WheelEvent("wheel",e);
			dest.dispatchEvent(n);
		});
	}
	get_date(date=undefined)
	{
		let n = (new Date).toLocaleString();
		if(date!=undefined)
			n = (new Date(date)).toLocaleString();
		return n;
	}
	console(message,level="info",obj="",from=this.from_str)
	{
	 	if (level == "info" || level == "i" || level =="in" || level == "inf")
	 		console.log("[%cINFO %c:%s] %s%o","color: blue;","color: default;",from,message,obj);
	  	if (level == "warn" || level == "war" || level =="wa" || level == "w")
	 		console.log("[%cWARN %c:%s] %s%o","color: yellow;","color: default;",from,message,obj);
	 	if(level == "error" || level == "erro"|| level == "err"|| level == "er"|| level == "e")
	 		console.log("[%cERR  %c:%s] %s%o","color: red;","color: default;",from,message,obj);
	 	if (level == "good" || level == "goo" || level =="go" || level == "g")
	 		console.log("[%cGOOD %c:%s] %s%o","color: green;","color: default;",from,message,obj);
	 	if (level == "debug" || level == "d" || level =="deb" || level == "de"||level=="debu")
	  		console.log(String((new Date()).getTime())+":"+message,obj," ");
	  	// Assertion
	  	if(level == "a")
	  		if(obj)
	  		{
		 		console.log("[%cGOOD %c:%s] %s%c%s","color: green;","color: default;",from,message,"float: right;","OK");
	  		}
	  		else
	  		{
		 		console.log("[%cERR  %c:%s] %s%c%s","color: red;","color: default;",from,message,"float: right;","NOK");
		 		console.warn(new Error().stack);
	  		}
	}
	/* But: Faire du simple pattern matching, les about: sont désactivés, à voir pour d'autres types */
	host_permission(url)
	{
		let er = /^about:/;
		if( url.match(er) != null)
			return false;
		return true;
	}
	/* But: générer un id de bookmark */
	gen_bid()
	{
		return String(Math.round(Math.random()*100000));
	}
	group(message,from=this.from_str)
	{
		try{if(this._this.backend.storage.settings.backend.debug.backend.tools.groupCollapsed)
				window.console.groupCollapsed("[GROUP:%s] %s",from,message);
			else
				window.console.group("[GROUP:%s] %s",from,message);
		}
		catch(e) { window.console.group("[GROUP:%s] %s",from,message); }
	}
	groupEnd()
	{
		window.console.groupEnd();
	}
	javascript_patch(obj=this,root=obj,exclude_from_patch=["bookmarks","settings","images","groups","config"])
	{
		let b,test,e;
		obj._this = root;
		for(b of Object.keys(obj))
		{
			test = false;
			for(e of exclude_from_patch)
			{
				if(e == b)
				{
					test = true;
					break;
				}
			}
			if(!test && (typeof(obj[b]) == 'object'||typeof(obj[b])=='function' ) && obj[b] != null && b != '_this' )
				this.javascript_patch(obj[b],root);
		}
	}
	/*
	 * Pallions au manque de ce Fichu Javascript
	 *  lock_api         : permet d'éviter les fautes de frappes
	 *  javascript_patch : permet d'éviter les fautes liées à la scope courante (permet de faire des alias)
	 */
	// pour cette fonction on voudrait bien appeler freeze, mais elle a le défaut de geler tout l'ojet (sans faire de distinction entre les champs)
	lock_api(a=this)
	{
		// NOK: TypeError: can't prevent extensions on this proxy object
		//Object.preventExtensions(a);
		let p;
		for(p of Object.keys(a))
		{
			if (p != '_this' && typeof(a[p]) == 'object' && a[p] != null )
				this.lock_api(a[p]);
		}
	}
	/* But: fournir une interface minimale permettant de controller le debug */
	parsePref(str,obj=this._this.backend.storage.settings,first=true)
	{
		let subs,t,kv,o,jsvalue,sub,rv,k,v,type,arr,m;
		if(str==undefined || str=="")
			return true;
		if(first)
		{
			try{if(this._this.backend.storage.settings.backend.debug.backend.tools.parsePref)
				{
					if(first)
						this.group("backend.tools.parsePref");
					this.console("str=","d",str);
				}
			}
			catch(e) { }
			subs = str.split(";;");
			if(subs.length > 1)
			{
				t=true;
				for(sub of subs)
				{
					t&=this.parsePref(sub,obj,first);
				}
				return t;
			}
		}
		// Séparation de la clé des valeurs
		kv = str.split("=");
		k=kv.shift();					// clé
		type=kv.shift(); 				// type de la valeur de la clé
		if(k==undefined || type==undefined) return false;
		v=kv.join("=");					// valeur
		arr = k.split("."); 			// split de la clé (création des objets découlants)
		if(first && !Object.keys(obj).includes(arr[0]))
			arr.unshift("backend","debug");
		o = arr.shift();
		if( arr.length == 0)
		{
			switch(type)
			{
				case "boolean":
				case "number":
				case "array":
					jsvalue=JSON.parse(v);
					break;
				case "CSS":
					try{
						m=JSON.parse(v);
						if(Array.isArray(m))
							obj[o]=m;
						else
							obj[o].unshift(v);
					} catch(e) {obj[o].unshift(v);}
					return true;
				default:
				case "string":
					jsvalue=String(v);
					break;
			}
			obj[o]=jsvalue;
			return true;
		}

		if(obj[o]==undefined)
			obj[o]={};
		rv=this.parsePref(arr.join(".")+"="+type+"="+v,obj[o],false);
		try{if(this._this.backend.storage.settings.backend.debug.backend.tools.parsePref)
			if(first)
				this.groupEnd();
		}
		catch(e) { }
		return rv;
	}
	sleep(ms)
	{
		return new Promise(resolve => setTimeout(resolve, ms));
	}

};
class vAPI{constructor(){
	this.Object={};
	this.Object.copy=this.Object_copy;
	Array.prototype.last=function(){if(this.length==0)return undefined;return this[this.length-1];}
	Array.prototype.first=function(){if(this.length==0)return undefined;return this[0];}
}
	Object_copy(target,t2=undefined)
	{
		if(t2!=undefined)
		{
			target=Object.assign(JSON.parse(JSON.stringify(target)),JSON.parse(JSON.stringify(t2)));
			return target;
		}
		return JSON.parse(JSON.stringify(target));
	}
};
function _backend_tools_regression()
{
	let waithandlers=200,gsz,a,b,c,d,e,f,g,h,i,ii,iii,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,test,g1,g2,g3,g4,h1,h2,h3,h4,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,tests=[];
	this._this.tools.group("tools");
		test=true;
		this._this.tools.parsePref("backend.debug.backend.storage.BookmarkTreeNode.handlers.onCreated=boolean=true;;");
		this._this.tools.parsePref("backend.debug.backend.storage.BookmarkTreeNode.handlers.onRemoved=boolean=true;;");
		this._this.tools.parsePref("backend.debug.backend.storage.BookmarkTreeNode.handlers.onChanged=boolean=true;;");
		this._this.tools.parsePref("backend.debug.browser.bookmarks.onChanged=boolean=true;;");
		this._this.tools.parsePref("backend.debug.browser.bookmarks.onRemoved=boolean=true;;");
		this._this.tools.parsePref("backend.debug.browser.bookmarks.onCreated=boolean=true;;");
		try{
			test&=this._this.sto.settings.backend.debug.backend.storage.BookmarkTreeNode.handlers.onCreated;
			test&=this._this.sto.settings.backend.debug.backend.storage.BookmarkTreeNode.handlers.onRemoved;
			test&=this._this.sto.settings.backend.debug.backend.storage.BookmarkTreeNode.handlers.onChanged;
			test&=this._this.sto.settings.backend.debug.browser.bookmarks.onChanged;
			test&=this._this.sto.settings.backend.debug.browser.bookmarks.onRemoved;
			test&=this._this.sto.settings.backend.debug.browser.bookmarks.onCreated;
		}catch(e){
			test=false;
		}
		this._this.tools.parsePref("backend.debug.backend.storage.BookmarkTreeNode.handlers.onCreated=boolean=false;;");
		this._this.tools.parsePref("backend.debug.backend.storage.BookmarkTreeNode.handlers.onRemoved=boolean=false;;");
		this._this.tools.parsePref("backend.debug.backend.storage.BookmarkTreeNode.handlers.onChanged=boolean=false;;");
		this._this.tools.parsePref("backend.debug.browser.bookmarks.onChanged=boolean=false;;");
		this._this.tools.parsePref("backend.debug.browser.bookmarks.onRemoved=boolean=false;;");
		this._this.tools.parsePref("backend.debug.browser.bookmarks.onCreated=boolean=false;;");
		this._this.tools.console("parsePref","a",test);

	this._this.tools.groupEnd();
}

# Configuration variables
#
webext:=$(WE_ROOT)web-ext
webext_r:=$(webext) run --pref=devtools.toolbox.selectedTool=webconsole --pref=devtools.toolbox.host=window --pref=devtools.toolbox.splitconsoleEnabled=true --bc
webext_r_urls:=-u ../src/tests/main.html -u about:addons -u 'about:debugging\#/runtime/this-firefox'



# Main rule
all: build



# Build & testing environment
build:
	if ! [ -d "bin/" ] ; then mkdir bin/ ; fi
	./gen.sh -p
	$(webext) -o -a bin/ -s src-prod/ build ;
	bash -c '\
		L=(`ls --sort time bin/`) ; \
		l=$${L[0]}; \
		mv bin/$$l bin/$${l/%\.zip/.xpi} ; ';
	chmod -R 777 bin/
	chmod -R 777 src-prod/

run: r_default

r_prod:
	cd src-prod/ && $(webext_r) $(webext_r_urls);

r_default:
	cd src/ && $(webext_r) $(webext_r_urls);
	
# Test firefox specific versions
r_dev72:
	cd src/ && $(webext_r) $(webext_r_urls) -f ../../../firefox-72.0b2/firefox/firefox-bin;

r_dev75:
	cd src/ && $(webext_r) $(webext_r_urls) -f ../../../firefox-75.0a1.en-US.linux-i686/firefox/firefox-bin


# Additionnal rules
clean:
	@-rm -fr bin/
	@-rm -fr src-prod/
	@-rm -fr favacarte-src-prod/
compute-checksums: build
	if ! [ -d "favacarte-src-prod" ] ; then mkdir favacarte-src-prod/ ; fi
	cd favacarte-src-prod/ && 7z x /favacarte.xpi
	find src-prod/ -type f -exec bash -c 'a={};sha256sum $${a/#src-prod\//favacarte-src-prod/} ; sha256sum {}' \; > /checksums.txt
	cat /checksums.txt
	chmod -R 777 favacarte-src-prod/




#!/bin/bash -x
# Manager le code

function dhelp() {
	echo "Usage: $0 [options]";
	echo " -a : update assets (data/images/proxy/ -> src/data/)";
	echo " -p : dev -> prod (src/ -> src-prod/)";
	echo " -h : display this help";
}
for a in "$@" ; do
	case $a in
	 "-h")
		dhelp;
	;;
	"-a")
	        cp -rL data/images/proxy/* src/data/;
	        echo "Assets updated";
	;;	
	"-p")
	    p=src-prod;
	    rm -rf $p;
	    mkdir $p $p/css;
	    cp src/css/fav_ui.css $p/css/;
	    cp src/css/popup.css $p/css/;
	    cp src/css/favastyles.css $p/css/;
            cp -Lr src/data/ $p/;
	    cp -r src/_locales $p/;
	    mkdir $p/popup;
	    cp src/popup/options.html $p/popup/;
	    cp src/popup/popup.html $p/popup/;
	    mkdir $p/scripts $p/scripts/content/;
	    cp -r src/scripts/class $p/scripts;
	    cp src/scripts/content/fav_ui.js $p/scripts/content/;
	    sed 's/_html_attach_events();/_html_attach_events(true);/g' $p/scripts/content/fav_ui.js > .temp && mv .temp $p/scripts/content/fav_ui.js;
	    cp src/scripts/content/vis.patch.js $p/scripts/content/vis.patch.js;
	    cp -r src/scripts/options $p/scripts;
	    cp -r src/scripts/privilegied/ $p/scripts/;
	    grep -v -E "fava_[^7]" src/manifest.json > $p/manifest.json;
	    find $p -type f -name "*.js" -exec bash -c 'terser --comments " " --compress pure_funcs=["sss"] {} >> .temp ; mv .temp {}' \;
	    echo "prod created as '$p'";
    ;;
	esac
done
if [ "$#" = "0" ] ; then dhelp ; fi

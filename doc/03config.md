***
	
	Options de configuration
	Configuration options

***

frontend.interface.background=string=VALUE;;
	Set up background color of main interface
	Changer la couleur du background de l'interface principale.
	VALUE could be : color hex code, string, rgb(), rgba()

frontend.interface.opacity=number=VALUE;;
	Change opacity of the main interface (you will see webpage)
	Changer l'opacité de l'interface principale (vous verrez la page web en arrière plan)
	VALUE entre 0 et 1

frontend.interface.font.color=string=VALUE;;
	Change font color of main interface
	Changer la couleur de la police de l'interface principale
	VALUE could be : color hex code, string, rgb(), rgba()

Il existe d'autres options *** A DETAILLER ICI ***

***

	How to process
	Comment procéder

***
Avec votre chaine d'options (ex: frontend.interface.background=string=red;;frontend.interface.font.color=string=green;;), rendez-vous à l'url about:addons. Dans "Préférences", utiliser "Importer" avec la chaine d'options.
Votre interface s'actualise.

With your options (eg: frontend.interface.background=string=red;;frontend.interface.font.color=string=green;;), goto "about:addons". Under "Préférences", use "Import" with your options.
Your interface updates.
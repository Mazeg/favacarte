Firefox API | BookmarkTreeNode | Bookmarks | Groups | Frontend(visjs)


Each part is an interface that translate from/to firefox to/from Frontend


*
    BookmarkTreeNode permit to translate BookmarkTreeNode of firefox to Bookmarks (we just add some attributes like notes en bookmarks, cf 00backend.storage.bookmark.md). Or in the other direction from Bookmarks to Firefox API.

*
    Bookmarks permit to translate Groups of favacarte to Bookmarks (a Group represent 1 or more Bookmarks).
    This layer permit a to represent to the user bookmarks (with large sens) with one or more parents.

*
    visjs nodes are just representation of Groups.

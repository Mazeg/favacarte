Firefox API | BookmarkTreeNode | Bookmarks | Groups | Frontend(visjs)
                                                *

this._this.backend.storage.groups[] = [ {} , {} , {} ]

{
   id       	        // identifiant du bookmark, exemple: "toolbar____"
   members              // Array
   children 	        // Array des ids des successeurs
   parents              // Array des parents
   url	                // url du bookmark
   image 	            // un index dans Array d'images
   title         	    // titre du bookmark
   date 	            // date d'ajout du bookmark
   type          	    // type : bookmark, folder
   coord:{x: 0,y: 0}    // coordonnées sur l'interface utilisateur (UI) cad les coordonnees de visjs
   estCache 	        // le noeud est-il masqué sur l'interface utilisateur
   depth                // stocke la profondeur actuelle du noeud
   note                 // Note sur le favoris
}

Firefox API | BookmarkTreeNode | Bookmarks | Groups | Frontend(visjs)
                                    *

this._this.backend.storage.bookmarks[] = [ {} , {} , {} ]

{
   id       	     // identifiant du bookmark, exemple: "toolbar____"     | an id (could be the one of firefox internal)
   children 	     // liste des ids des successeurs		                | list of bookmark childrens
   parent            // parentId                                            | parent id of bookmark
   url	             // url du bookmark                                     | url pointed by bookmark_change
   image 	         // un index dans Array d'images                        | an index in an Array of bookmarks
   title         	 // titre du bookmark                                   | name of the bookmark
   date 	         // date d'ajout du bookmark                            | date of creation
   type          	 // type : bookmark, folder                             | type : folder, bookmark
   coord:{x: 0,y: 0} // coordonnées sur l'interface utilisateur (UI)        | coord on the interface (visjs coord system)
   estCache 	     // le noeud est-il masqué sur l'interface utilisateur  | hidden node or not
   depth             // stocke la profondeur actuelle du noeud              | actuel depth (from "toolbar____")
   note              // Note sur le favoris                                 | small note on this bookmark
}

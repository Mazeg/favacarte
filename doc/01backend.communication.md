An overview of communication inside
Echange de message en interne | Exchange of message inside

A: daemon.js
B: fav_ui.js
C: options.js

ordre 			          sens	DISPO ACTUELLE 			description


bookmark_change 	B  ----->	A 		OK 	démon doit faire tous les changement de Bookmarks               |  client has perfom some changes (set some flags .state on bookmarks), so the daemon need to process with BookmarkTreeNode.push()

											                                             

bookmark_open  		B ------>   A 		OK 	démon doit ouvrir une ou plusieurs url pour le client           | daemon need to open 1/n urls for client


ui_render_show 		B <-------  A       OK 	faire afficher la vue avec le raccourcis clavier lié à ext.     | toggling the view


sto_read 			B <-------  A 		OK 	démon ordonne à cs de lire le stockage                          | daemon request content for a storage 
read


sto_read_exceptUI	B <-------	A 		OK  démon ordonne à cs de lire le stockage mais en gardant les coord| daemon request content for a storage read but keeping coord that are currently in the view.


sto_write 			B <------- A		OK 	démon ordonne à cs d'écrire ses valuers sur stock               | daemon force content script to write the actual state

options_settings    C -------> A        OK  les prefs sont changées par l'utilisateur, le daemon doit faire | prefs have been changed so daemon need to perform some actions (like reload all tabs)
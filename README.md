# favacarte : a bookmarks map
 - You're under Firefox.
 - You're boring of small folder, difficult to access.
 - You need a powerful interface.
 - You need freedom in your configuration.<br />
This addon is for you !<br />
<sub>
<img src="https://framagit.org/Mazeg/favacarte/raw/master/doc/04overview/Screenshot_20200222_184043.png">
</sub>
 

## Building
### Using docker
```bash
docker image pull favacartedocker/favacarte
docker run -ti --mount type=bind,source=$PWD,target=/favacarte/ favacartedocker/favacarte
cd /favacarte/ && make build
```
### Without docker
```bash
make build
```
### Verify favacarte versions from .xpi (addons.mozilla.org)
You want to verify the fact that git version match addons.mozilla.org version:
```bash
git checkout <VERSION>
docker run -ti --mount type=bind,source=<MY PATH>/favacarte-<VERSION>.xpi,target=/favacarte.xpi --mount type=bind,source=$PWD,target=/favacarte/ favacartedocker/favacarte
cd favacarte && make compute-checksums
```
checksums.txt contains paired files, both got same hash. If not please report (download url, favacarte version).<br />
Example:
```bash
git checkout 0.2.1
docker run -ti --mount type=bind,source=$HOME/Downloads/favacarte-0.2.1.xpi,target=/favacarte.xpi --mount type=bind,source=$PWD,target=/favacarte/ favacartedocker/favacarte
cd favacarte && make compute-checksums
```
78bc6e0f1f8fd4fb10b3c7805a9936da46996bcef15ecb364f6601952796a8bf  favacarte-src-prod/manifest.json<br />
78bc6e0f1f8fd4fb10b3c7805a9936da46996bcef15ecb364f6601952796a8bf  src-prod/manifest.json<br />
..<br />
hash n°x                                                          from addons.mozilla.org/something<br />
hash n°x                                                          from sources/something<br />
..<br />
fa73f99a1a23eb6ac4173f180fe499a74504b716722e475c0fa4628fed4e219c  favacarte-src-prod/popup/options.html<br />
fa73f99a1a23eb6ac4173f180fe499a74504b716722e475c0fa4628fed4e219c  src-prod/popup/options.html<br />



### Testing
There is two directories at this step:<br />
src/ which contains developpment version<br  />
src-prod/ which contains production version (minified)<br />
Run src/ : <br />
```bash
make r_default
```
Run src-prod/ : <br />
```bash
make r_prod
```
Browser console and main window starts<br />
Command input  : main window<br />
Command output : browser console<br />
Use Ctrl+Shift+1 to get help<br />
Use Ctrl+Shift+6 to launch tests<br />
Check browser console to see tests results<br />



## Start with configuration
favacarte configuration's start in about:addons, in doc/03themes.md you can find some example themes<br />
in doc/03config.md, you will find doc on differents configuration options.<br />



## Report a bug
Open an issue with a maximum-detailled description (firefox version, plugin version, conditions under which bug append).<br />
Or send an electronique message to mazeg under netcourrier.
